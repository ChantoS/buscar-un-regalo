import { createSlice } from '@reduxjs/toolkit';

export const authSlice = createSlice({
    name: 'auth',
    initialState: {
        status: 'not-authenticated', // checking 'not-authenticated', 'aunthenticated'
        username: '',
        displayName: '',
        email: '',
        id: 0,
        img: '',
        sub: 0,
        communities: [],
        errorMessage: undefined
    },
    reducers: {
        onChecking: ( state ) => {
            state.status = 'checking', // checking 'not-authenticated', 'aunthenticated'
            state.username = '',
            state.errorMessage = undefined  
        },
        onLogin: (state, { payload }) => {
            state.status = 'authenticated'; // checking 'not-authenticated', 'aunthenticated'
            state.username = payload.username;
            state.displayName = payload.display_name;
            state.email = payload.email;
            state.id = payload.id;
            state.img = payload.img;
            state.sub = payload.sub;
            state.errorMessage = undefined;
        },
        editUser: (state, { payload } ) => {
            state.status = 'authenticated'; // checking 'not-authenticated', 'aunthenticated'
            state.displayName = payload.name;
            state.email = payload.email;
            state.img = payload.img;
            state.errorMessage = undefined;
        },
        onLogout: (state, { payload }) => {
            state.status = 'not-authenticated', // checking 'not-authenticated', 'aunthenticated'
            state.username = '';
            state.displayName = '';
            state.email = '';
            state.id = 0;
            state.img = '';
            state.sub = 0;
            state.communities = [];
            state.errorMessage = payload;
        },
        setCommunities: (state, { payload }) => {
            let communities = payload.communities.map((item) => item.name);
            state.communities = payload.communities
        },
        clearErrorMessage: (state) => {
            state.errorMessage = undefined;
        },
        checkingCredentials: (state) => {
            state.status= 'checking'
        },
    }
});

export const { onChecking, onLogin, editUser, onLogout, clearErrorMessage, checkingCredentials, setCommunities } = authSlice.actions;