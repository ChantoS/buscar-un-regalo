import { createSlice } from '@reduxjs/toolkit';

export const giftsSlice = createSlice({
    name: 'gifts',
 
    initialState: {
        id: 0,
        name: '',
        description: '',
        img: '',
        reserved: 0,
        userId: 0,
        communityID: 0,
        owner: '',
        errorMessage: null,
    },

    reducers: {
        
        getGiftData: ( state, { payload } ) => {
            const { id, name, description, img, reserved, owner, communityID } = payload;
            state.id = id,
            state.name = name,
            state.description = description,
            state.img = img,
            state.reserved = reserved,
            state.owner = owner,
            state.communityID = communityID,
            state.errorMessage = null
        },
        onAddGifts: ( state, { payload }) => {
            state.id = payload.id,
            state.name = payload.name,
            state.description = payload.description,
            state.img = payload.img,
            state.userId = payload.userId,
            state.communityID = payload.communityID,
            state.errorMessage = null
        },
        onDeleteGifts: ( state, { payload }) => {
    
        },
        onUpdateGifts: ( state, { payload }) => {
            const { name, description, img, communityID } = payload;
            state.name = name,
            state.description = description,
            state.img = img,
            state.communityID = communityID,
            state.errorMessage = null    
        },
        resetGiftData: (state) => {
            state.id = -1,
            state.name = '',
            state.description = '',
            state.img = '',
            state.reserved = -1,
            state.owner = '',
            state.communityID = 0,
            state.errorMessage = null
        },
        reserveGiftReducer: (state, { payload }) => {
            state.reserved = payload.userID
        },
        unreserveGiftReducer: (state) => {
            state.reserved = 0
        }
    }
});

export const { onAddGifts, onDeleteGifts, getGiftData, onUpdateGifts, resetGiftData, reserveGiftReducer, unreserveGiftReducer } = giftsSlice.actions;