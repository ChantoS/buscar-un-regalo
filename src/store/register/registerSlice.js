import { createSlice } from '@reduxjs/toolkit';

export const registerSlice = createSlice({
    name: 'register',
    initialState: {
        status: 'not-registered', // registering, 'not-registered', 
        displayName: null,
        username: null,
        email: null,
        password: null,
        password_confirm: null,
        picture: null,
        errorMessage: '',
    },
    reducers: {
        onRegistering: (state) => {
            state.status = 'registering', // checking 'not-authenticated', 'aunthenticated'
            state.errorMessage = ''
        },
        onGetCodeResponse: (state, { payload: { codeRegister }}) => {
            state.errorMessage = codeRegister != 200 ? 'Código incorrecto' : ''; 
        },
        onResetErrorMessage: (state) => {
            state.errorMessage = ''; 
        }
    }
});

export const { onRegistering, onGetCodeResponse, onResetErrorMessage } = registerSlice.actions;