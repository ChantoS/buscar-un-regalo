import { configureStore } from "@reduxjs/toolkit";
import { giftsApi } from "./api/giftsApi";
import { authSlice } from "./auth/authSlice";
import { giftsSlice } from "./gifts/giftsSlice";
import { registerSlice } from "./register/registerSlice";


export const store = configureStore ({
    reducer: {
       auth: authSlice.reducer,
       register: registerSlice.reducer,
       gifts: giftsSlice.reducer,

       [giftsApi.reducerPath]: giftsApi.reducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware()
        .concat( giftsApi.middleware )
})