
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { apiURL } from "../../helpers";


export const giftsApi = createApi({

    reducerPath: 'regalos',

    baseQuery: fetchBaseQuery({
        baseUrl: `${apiURL}api`
    }),

    tagTypes: ['Groups', 'Gifts'],
    
    endpoints: (builder) => ({

        getToken: builder.query({
            query: () => ({
                url: '/user/myinfo',
                method: 'GET',
                headers: {
                    'x-token': localStorage.getItem('token')
                }})
        }),

        postLogin: builder.mutation({
            query: ( payload ) => ({
                url: '/auth/login',
                method: 'POST',
                body: payload,
                //headers: {'x-token': localStorage.getItem('token')}
            }),
        }),

        postRegister: builder.mutation({
            query: ( payload ) => ({
                url: '/auth/register',
                method: 'POST',
                body: payload,
                //headers: { 'x-token': localStorage.getItem('token') }
            }),
        }),

        postRecoverCode: builder.mutation({
            query: ( payload ) => ({
                url: '/user/password/reset',
                method: 'POST',
                body: payload,
            }),
        }),

        getRecoverCodeCheck: builder.query({
            query: ( code ) => ({ 
                url: `/user/password/check/${code}`,
                method:'GET',
                headers:{
                    'x-token': localStorage.getItem('token')
                }}), 
        }),

        postChangePassord: builder.mutation({
            query: ( payload ) => ({
                url: '/user/password/changePassword',
                method: 'POST',
                body: payload,
            }),
        }),

        postRegisterCode: builder.mutation({
            query: ( payload ) => ({
                url: '/auth/register/code',
                method: 'POST',
                body: payload,
            }),
        }),

        getUser: builder.query({
            query: ( id ) => ({ 
                url: `/user/id/${id}`,
                method:'GET',
                headers:{
                    'x-token': localStorage.getItem('token')
                }}), 
        }),

        putEditUser: builder.mutation({
            query: ( payload ) => ({
                url: `/user`,
                method: 'PUT',
                body: payload,
                headers: {
                    'x-token': localStorage.getItem('token')
                }
            }),
        }),

        //De un solo regalo
        getGift: builder.query({
            query: ( id ) => ({ 
                url: `/gifts/id/${id}`,
                method:'GET',
                headers:{
                    'x-token': localStorage.getItem('token')
                }}), 
        }),

        //Para una persona o my gifts
        getUserGifts: builder.query({
            query: ( {id, community} ) => ({ 
                url: `/gifts/user/${id}/${community}`,
                method:'GET',
                headers:{
                    'x-token': localStorage.getItem('token')
                }}), 
            providesTags: ['Gifts']
        }),

        getUserReservedGifts: builder.query({
            query: () => ({ 
                url: `/gifts/reserved`,
                method:'GET',
                headers:{
                    'x-token': localStorage.getItem('token')
                }}), 
            providesTags: ['Gifts']
        }),

        postAddGift: builder.mutation({
            query: ( payload ) => ({
                url: '/gifts',
                method: 'POST',
                body: payload,
                headers: {
                    'x-token': localStorage.getItem('token')
                }
            }),
            invalidatesTags: ['Gifts']
        }),

        postReserveGift: builder.mutation({
            query: ( payload ) => ({
                url: '/gifts/reserve',
                method: 'POST',
                body: payload,
                headers: {
                    'x-token': localStorage.getItem('token')
                }
            }),
            invalidatesTags: ['Gifts']
        }),

        postUnreserveGift: builder.mutation({
            query: ( payload ) => ({
                url: '/gifts/unreserve',
                method: 'POST',
                body: payload,
                headers: {
                    'x-token': localStorage.getItem('token')
                }
            }),
            invalidatesTags: ['Gifts']
        }),

        putEditGift: builder.mutation({
            query: ( payload ) => ({
                url: '/gifts',
                method: 'PUT',
                body: payload,
                headers: {
                    'x-token': localStorage.getItem('token')
                }
            }),
            invalidatesTags: ['Gifts']
        }),

        deleteGift: builder.mutation({
            query: ( payload ) => ({
                url: '/gifts',
                method: 'DELETE',
                body: payload,
                headers: {
                    'x-token': localStorage.getItem('token')
                }
            }),
            invalidatesTags: ['Gifts']
        }),

        postSearch: builder.mutation({
            query: ( payload ) => ({
                url: '/gifts/search',
                method: 'POST',
                body: payload,
                headers: {
                    'x-token': localStorage.getItem('token')
                }
            }),
        }),
        
        getCommunities: builder.query({
            query: () => ({
                url: `/community`,
                method: 'GET',
                headers: {
                    'x-token': localStorage.getItem('token')
                }}),
            providesTags: ['Groups']
        }),
        
        getFiveMemebers: builder.query({
            query: () => ({
                url: `/user/random`,
                method: 'GET',
                }),
            providesTags: ['Groups']
        }),
        
        getCommonCommunities: builder.query({
            query: (id) => ({
                url: `/community/user/${id}`,
                method: 'GET',
                headers: {
                    'x-token': localStorage.getItem('token')
                }}),
            providesTags: ['Groups']
        }),
        
        getCommunityByID: builder.query({
            query: (id) => ({
                url: `/community/${id}`,
                method: 'GET',
                headers: {
                    'x-token': localStorage.getItem('token')
                }}),
            providesTags: ['Groups']
        }),
        
        getCommunityMembers: builder.query({
            query: (id) => ({
                url: `/community/members/${id}`,
                method: 'GET',
                headers: {
                    'x-token': localStorage.getItem('token')
                }}),
            providesTags: ['Groups']
        }),
        
        postCommunities: builder.mutation({
            query: (payload) => ({
                url: '/community',
                method: 'POST',
                body: payload,
                headers: {
                    'x-token': localStorage.getItem('token')
                }}),
            invalidatesTags: ['Groups']
        }),
        
        putCommunities: builder.mutation({
            query: (payload) => ({
                url: '/community',
                method: 'PUT',
                body: payload,
                headers: {
                    'x-token': localStorage.getItem('token')
                }}),
            invalidatesTags: ['Groups']
        }),
        
        postJoinCommunity: builder.mutation({
            query: (payload) => ({
                url: '/community/join',
                method: 'POST',
                body: payload,
                headers: {
                    'x-token': localStorage.getItem('token')
                }}),
            invalidatesTags: ['Groups']
        }),
        
        postLeaveCommunity: builder.mutation({
            query: (payload) => ({
                url: '/community/leave',
                method: 'POST',
                body: payload,
                headers: {
                    'x-token': localStorage.getItem('token')
                }}),
            invalidatesTags: ['Groups']
        }),

    })

});

export const { 
    useGetTokenQuery, 
    usePostLoginMutation, 
    usePostRegisterMutation,
    usePostRecoverCodeMutation,
    usePostRegisterCodeMutation,
    useGetRecoverCodeCheckQuery,
    usePostChangePassordMutation,
    useGetUserQuery, 
    usePutEditUserMutation, 
    useGetGiftQuery, 
    useGetUserGiftsQuery, 
    useGetUserReservedGiftsQuery,
    usePostAddGiftMutation, 
    usePostReserveGiftMutation,
    usePostUnreserveGiftMutation,
    usePutEditGiftMutation, 
    useDeleteGiftMutation,
    usePostSearchMutation,
    useGetCommunitiesQuery,
    useGetCommonCommunitiesQuery,
    useGetCommunityByIDQuery,
    useGetCommunityMembersQuery,
    usePostCommunitiesMutation,
    usePutCommunitiesMutation,
    usePostJoinCommunityMutation,
    usePostLeaveCommunityMutation,
    useGetFiveMemebersQuery
} = giftsApi;