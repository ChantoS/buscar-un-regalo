import { Grid } from '@mui/material'

export const AuthLayout = ({ children, title = '' }) => {
  return (
    <Grid
      className='bg-gradient'
      container
      spacing={ 0 }
      direction="column"
      alignItems="center"
      justifyContent="center"
      sx={{ 
        minHeight: '100vh', 
      }}
    >
        
      { children }
        
    </Grid>
  )
}
