import { Button, CircularProgress, Grid, Typography } from '@mui/material'
import { useState } from 'react'

export const EditInfoComponent = ({ closeDialogAlert, editAction }) => {

    const [loading, setloading] = useState(false);

    const startAction = () => {
        setloading(true);
        editAction();
    }

    return (
        <Grid
            direction = {{ xs: "column-reverse", sm: "row" }}
            container
            justifyContent = "center"
            sx = {{
                pt: 3,
            }}
            >

            <Grid
                container 
                justifyContent = "center"
                sx = {{
                width: { xs: "100%", sm: "30%" },
                mt: { xs: 2, sm: 0 },
                mr: { xs: 0, sm: 3 },
                }}
            >

                <Button
                    onClick = { () => closeDialogAlert()}
                    sx = {{
                        backgroundColor: "red",
                        pt: 1.5,
                        pb: 1.5,
                        ":hover": { backgroundColor: "#cf2525" },
                        borderRadius: 5,
                        width: "100%",
                    }}
                >

                <Typography
                    sx = {{
                    color: "white",
                    fontSize: 24,
                    }}
                >

                    Cancelar

                </Typography>

                </Button>

            </Grid>

            <Grid
                container 
                justifyContent = "center"
                sx = {{
                width: { xs: "100%", sm: "30%" },
                }}
            >

                <Button
                    onClick = { () => startAction()}
                    disabled={loading}
                    sx = {{
                        backgroundColor: "#5494b3",
                        pt: 1.5,
                        pb: 1.5,
                        ":hover": { backgroundColor: "#01668d" },
                        borderRadius: 5,
                        width: "100%"
                    }}
                >

                <Typography
                    sx = {{
                    color: "white",
                    fontSize: 24,
                    }}
                >

                    { loading ? <CircularProgress /> : "Aceptar" }

                </Typography>   

                </Button>

            </Grid>

        </Grid>
    )
}
