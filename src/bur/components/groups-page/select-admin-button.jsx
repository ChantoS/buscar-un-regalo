import { Button, Grid, Typography } from '@mui/material';
import React from 'react'
import { useState } from 'react';
import { useCustomAlerts } from '../../../hooks/useCustomAlerts';
import { usePostLeaveCommunityMutation } from '../../../store/api/giftsApi';

export const SelectAdminButton = ({ userID, myID, membersCount, closeDialogAlert, communityID }) => {
  
    let buttonText = "Seleccionar";
    let buttonType = 0; // 0 = Select (other user) // 1 = Select (myself) // 2 = Close group
    const [ leaveGroup ] = usePostLeaveCommunityMutation();
    const { openToast, newToast } = useCustomAlerts();
    const [leavingGroup, setLeavingGroup] = useState(false);

    if(userID == myID) {
        if(membersCount <= 1) {
            buttonType = 2;
            buttonText = "Cerrar grupo";
        }
        else 
            buttonType = 1
    }

    const selectLeaving = (id) => {
        if(leavingGroup) return;

        setLeavingGroup(true);
        selectAdmingAction(id);
    }

    const selectAdmingAction = async (anotherAdmin = 0) => {
        if(buttonType != 1) {
            if(buttonType == 2) {
                const { data: leaveData, error: leaveError } = await leaveGroup({ communityID });
                if( leaveData && leaveData.code == 200 ) 
                    closeDialogAlert();
                else if (leaveError) {
                    openToast("Error al intentar cerrar el grupo", 'error');
                }
            }
            else {
                const { data: leaveData, error: leaveError } = await leaveGroup({ communityID, anotherAdmin});
                if( leaveData && leaveData.code == 200 ) 
                    closeDialogAlert();
                else if (leaveError) {
                    openToast("Error al intentar asignar otro admin", 'error');
                }
            }
        }
    }
    

    return (
        <Grid
            sx = {{
                mt: 2
            }}
        >

            <Button
                onClick={ () => selectLeaving(userID) }
                sx = {{
                    backgroundColor: buttonType == 2 ? "#ba3232" : "#6b98ae",
                    py: 1.7,
                    px: 4,
                    ":hover": { backgroundColor: buttonType == 2 ? "#cf2525" : "#197cae" },
                    opacity: (buttonType == 1) ? 0.3 : 1,
                    cursor: buttonType == 1 ? 'default' : 'pointer'
                }}
            >

                <Typography
                    sx = {{
                        color: "white",
                        fontSize: 15,
                    }}
                >

                    {buttonText}

                </Typography>

            </Button>

        </Grid>
    )
}
