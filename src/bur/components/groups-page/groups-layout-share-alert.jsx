import { Button, Grid, Typography } from "@mui/material"
import { siteURL } from "../../../helpers"

export const GroupsLayoutShareAlert = ({ closeDialogAlert, id }) => {
  return (
    <Grid
        container
        direction = {{ xs: "column", sm: "row" }}
        justifyContent = "center"
        sx = {{
            pt: 3,
        }}
    >

        <Grid
            container
            justifyContent = "center"
            sx = {{
                mb: 3,
            }}
        >

            <Grid
                sx = {{
                    width: { xs: "100%", sm: "80%", md: "80%" },
                }}
            >
                <input 
                    className = "shareInput"
                    value={`${siteURL}/groups/join/${id}`}
                    readOnly={true}
                />

            </Grid>

        </Grid>
        
        <Grid
            sx = {{
                width: { xs: "100%", sm: "35%" },
            }}
        >

            <Button
                onClick = { () => closeDialogAlert()}
                sx = {{
                    backgroundColor: "#5494b3",
                    mr: 3,
                    ":hover": { backgroundColor: "#01668d" },
                    borderRadius: 3,
                    width: "100%",
                    py: 2,
                }}
            >

                <Typography
                    sx = {{
                        color: "white",
                        fontSize: 24,
                        width: "100%",
                    }}
                >

                    Cerrar

                </Typography>

            </Button>

        </Grid>

    </Grid>
  )
}
