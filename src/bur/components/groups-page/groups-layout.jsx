import { Avatar, Button, Grid, Typography } from "@mui/material";
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import { useState, useRef, useEffect } from "react";
import { apiURL, siteURL } from "../../../helpers";
import { useForm } from "../../../hooks/useForm";
import { GroupsLayoutEditAlert } from "./groups-layout-edit-alert";
import { GroupsLayoutLeaveAlert } from "./groups-layout-leave-alert";
import { usePutCommunitiesMutation } from "../../../store/api/giftsApi";
import { GroupMemberList } from "./members-list";
import { useCustomAlerts } from "../../../hooks/useCustomAlerts";
import { GroupsLayoutShareAlert } from "./groups-layout-share-alert";
import { GroupMemberListVisualizer } from "./members-list-visualizer";

export const GroupLayout = ({ id, name: groupName, description: groupDesc, img, leaveAction, owner }) => {

    const { 
        name, 
        description, 
        picture, 
        password,
        onInputChange, 
        onFileChange,
    } = useForm ( { name: groupName, description: groupDesc, password: ''} );

    const {
        openAlertDialog,
        openToast,
        closeDialogAlert,
        newAlert,
        newToast
    } = useCustomAlerts();

    const [isMobile, setIsMobile] = useState(false);

    const [ editState, setEditState ] = useState(false);
    const [changedInput, setChangedInput] = useState(false);

    const [ buttonText, setButtonText ] = useState('Editar');
    const fileInputRef = useRef();
    const [avatarSRC, setAvatarSRC] = useState(img.length > 1 ? `${apiURL}uploads/communities/${img}` : "");

    const [ edit ] = usePutCommunitiesMutation();

    const changeButtonText = () => {
        
        if (editState) {
            if(changedInput) {
                openAlertDialog("¿Confirmar los nuevos datos del grupo?", <GroupsLayoutEditAlert {...{closeDialogAlert, confirmEdit, id} }  />);
            }
            else {
                confirmEdit();
            }
        }

        else {
            setEditState(true);
            setButtonText('Confirmar');
        }
    }

    const confirmEdit = () => {
        editGroup();
        setEditState(false);
        setButtonText('Editar');
    }

    const editGroup = async() => {

        if(!changedInput) {
            return;
        }

        const formData = new FormData();
        formData.append('communityID', id);
        formData.append('name', name);
        formData.append('description', description);
        formData.append('picture', picture);

        if(password.length > 0) {
            formData.append('isProtected', 1);
            formData.append('password', password);
        }
        else
            formData.append('isProtected', 0);
    
        try {
    
          const { data, error } = await edit( formData );
          if(data && data.code == 200) {
            openToast("El grupo ha sido editado correctamente!", 'success');
            closeDialogAlert();
          }
          else if (error) {
            openToast("Error al editar el grupo, por favor intente de nuevo", 'error');
          }
    
        } catch (error) {
          console.log(error);
        }

        setChangedInput(false);
    }

    const shareGroup = () => {
        if(navigator.share && isMobile) {
            navigator.share({
                title: "Invita personas a tu grupo",
                text: `Unete al grupo ${groupName} para compartir regalos en busca un regalo!`,
                url: `${siteURL}/groups/join/${id}`
            })
            .then(() => {
                openToast("Grupo compartido!", 'success');
            })
            .catch((error) => {
                console.log("Sharing error: ", error);
                openToast("Error al compartir el grupo", 'error');
            });
        }
        else {
            openAlertDialog("Comparte tu grupo a los demás!", <GroupsLayoutShareAlert {...{ closeDialogAlert, id}} />)
        }
    }

    const changeInput = (e) => {
        setChangedInput(true);
    }

    useEffect( () => {
        if(picture) {
          changeInput();
          const url = URL.createObjectURL(picture);
          setAvatarSRC(url);
        }
    }, [picture]);

    useEffect(() => {
        let check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        setIsMobile(check);
    }, []);

    return (
        
        <>
        
            <Grid
                container
                justifyContent = "center"
                sx = {{
                    width: "100%",
                }}
            >

                <Grid
                    container
                    sx = {{
                        width: { xs: "92%", sm: "92%", md: "80%", lg: "60%", xl: "60%" },
                        pt: 4,
                        pb: 4,
                        backgroundColor: "#01668d",
                        borderRadius: 15,
                    }}
                >

                    <Grid
                        justifyContent = "center"
                        alignItems = "center"
                        container
                        sx = {{
                            mt: 2,
                            width: { xs: "100%", sm: "0%", md: "10%", lg: "10%", xl: "10%" },
                            ml: { xs: 0, sm: 10 },
                        }}
                    >

                        <input
                            style = {{ 
                                display: 'none',
                            }}
                        />

                        
                        { img.length > 2
                            ? (
                                <a href={avatarSRC}>
                                    <Avatar
                                        src = { avatarSRC }
                                        sx = {{
                                            backgroundColor: "orange",
                                            height: { xs: 160, sm: 110, md: 140, lg: 140, xl: 160 },
                                            width: { xs: 160, sm: 110, md: 140, lg: 140, xl: 160 }, 
                                        }}
                                    />
                                </a>
                            )
                            : (
                                <Avatar
                                    src = { avatarSRC }
                                    sx = {{
                                        backgroundColor: "orange",
                                        height: { xs: 160, sm: 110, md: 140, lg: 140, xl: 160 },
                                        width: { xs: 160, sm: 110, md: 140, lg: 140, xl: 160 }, 
                                    }}
                                />
                            )
                        }

                        <input 
                            type = "file"
                            multiple
                            ref = { fileInputRef }
                            name = "picture"
                            onChange = { onFileChange }
                            style = {{ display: 'none' }}
                        />

                        <Grid
                            visibility = { !editState ? "hidden" : "visible" }
                            container
                            justifyContent = "end"
                            sx = {{
                                height: "10px",
                                position: "relative",
                                top: { xs: "-60px", sm: "-140px", md: "-150px" },
                                left: { xs: "-95px", sm: "50px", md: "20px" }
                            }}
                        >

                            <Grid
                                container
                                alignItems = "center"
                                justifyContent = "center"
                                sx = {{
                                    backgroundColor: "#6b98ae",
                                    height: "70px",
                                    width: "70px",
                                    borderRadius: "50px"
                                }}
                            >

                                <AddPhotoAlternateIcon 
                                    onClick = { () => fileInputRef.current.click() }
                                    sx = {{
                                        fontSize: "40px",
                                        color: "#003e65"
                                    }}
                                />

                            </Grid>

                        </Grid>

                    </Grid>

                    <Grid
                        container
                        direction = "column"
                        justifyContent = "center"
                        sx = {{
                            width: { xs: "100%", sm: "70%", md: "70%", lg: "70%", xl: "70%" },
                            ml: { xs: 0, sm: 10, md: 5},
                            px: 3,
                        }}
                    >

                        <Grid
                            container
                            alignItems = "center"
                            justifyContent = {{ xs: "center", sm: "start" }}
                            sx = {{
                                mb: 1.2,
                                mt: { xs: 2, sm: 0 },
                            }}
                        >

                            <Grid
                                container
                                justifyContent = {{ xs: "center", sm: "start" }}
                                sx = {{
                                    width: "100%"
                                }}
                            >

                                <Grid>

                                    <input 
                                        className = { !editState ? "hideEditInput" : "showEditNameInput" } 
                                        placeholder = "Nuevo nombre..."
                                        name="name"
                                        value={name}
                                        onChange={ (e) => { onInputChange(e); changeInput() }}
                                    />

                                </Grid>

                            </Grid>

                            <Typography
                                visibility = { editState ? "hidden" : "visible" }
                                component = { 'span' } 
                                variant={ 'body2' }
                                sx = {{
                                    color: "white",
                                    fontSize: editState ? 0 : 30,
                                    textAlign: "center"
                                }}
                            >

                                { `${name}` }

                            </Typography>

                            <Grid
                                container
                                justifyContent = {{ xs: "center", sm: "start" }}
                                sx = {{
                                    width: "100%",
                                    mt: { xs: 1.5, sm: 3 }
                                }}
                            >

                                <Grid 
                                    className = "roundedRectangle"
                                    sx = {{
                                        backgroundColor: "#5494b3",
                                        height: "4px",
                                        width: { xs: "80%", sm: "70%", md: "100%"},
                                        borderTopRightRadius: 100,
                                        borderTopLeftRadius: 100,
                                        borderBottomLeftRadius: 100,
                                        borderBottomRightRadius: 100,
                                    }}
                                />

                            </Grid>

                        </Grid>


                    <Grid
                        container
                        direction = "column"
                        justifyContent = {{ xs: "center", sm: "start" }}
                        sx = {{
                            mt: 2,
                            mb: 2,
                        }}
                    >

                        <Grid
                            container
                            justifyContent = "center"
                            sx = {{
                                width: "100%",
                            }}
                        >

                            <Grid
                                sx = {{
                                    width: { xs: "80%", sm: "100%" },
                                }}
                            >

                                <input 
                                    className = { !editState ? "hideEditInput" : "showEditDescInput" } 
                                    placeholder = "Nueva descripción..."
                                    name="description"
                                    value={description}
                                    onChange={(e) => { onInputChange(e); changeInput() }}
                                />

                            </Grid>

                        </Grid>

                        <Grid
                            container
                            justifyContent = "center"
                            sx = {{
                                width: "100%",
                            }}    
                        >

                            <Grid
                                sx = {{
                                    width: { xs: "80%", sm: "100%" },
                                }}
                            >

                                <input 
                                    type='password'
                                    className = { !editState ? "hideEditInput" : "showEditDescInput" } 
                                    placeholder = "Nueva contraseña..."
                                    name="password"
                                    value={password}
                                    onChange={(e) => { onInputChange(e); changeInput() }}
                                />

                            </Grid>

                        </Grid>

                        <Typography
                            visibility = { editState ? "hidden" : "visible" }
                            component = { 'span' } 
                            variant={ 'body2' }
                            sx = {{
                                color: "white",
                                fontSize: 20,
                                textAlign: { xs: "center", sm: "start" }
                            }}
                        >

                            { description }

                        </Typography>

                    </Grid>

                    <Grid
                        container
                        alignItems = "center"
                        justifyContent = {{ xs: "center", sm: "start"}}
                        direction = {{ xs: "column", sm: "row" }}
                        sx = {{
                            mt: 2,
                        }}
                    >
                        { 
                            owner === "yes"  && (

                                <Grid>

                                    <Button
                                        onClick = { changeButtonText }
                                        variant = "contained" 
                                        sx = {{
                                            backgroundColor: "white",
                                            pt: 1.5,
                                            pb: 1.5,
                                            pl: 3,
                                            pr: 3,
                                            mr: { xs: 0, sm: 2, md: 3, lg: 3, xl: 3},
                                            mb: { xs: 2, sm: 0, },
                                            ":hover": { backgroundColor: "#95bbcf" },
                                            width: { xs: "250px", sm: "110px", md: "130px" },
                                        }}
                                    >

                                        <Typography
                                            component = {'span'} 
                                            variant = {'body2'}
                                            textAlign = "start"
                                            sx = {{ 
                                                color: "#01668d",
                                                fontWeight: "bold", 
                                                fontSize: { xs: 20, sm: 15, md: 17 },
                                            }}
                                        >

                                            { buttonText }

                                        </Typography>

                                    </Button>

                                </Grid>
                            )
                        }

                        { 
                            owner === "no"  && (

                                <Grid>

                                    <Button
                                        onClick = { () => openAlertDialog("Miembros del grupo", <GroupMemberListVisualizer {... {communityID: id, closeDialogAlert}} />) }
                                        variant = "contained" 
                                        sx = {{
                                            backgroundColor: "white",
                                            pt: 1.5,
                                            pb: 1.5,
                                            pl: 3,
                                            pr: 3,
                                            mr: { xs: 0, sm: 2, md: 3, lg: 3, xl: 3},
                                            mb: { xs: 2, sm: 0, },
                                            ":hover": { backgroundColor: "#95bbcf" },
                                            width: { xs: "250px", sm: "110px", md: "130px" },
                                        }}
                                    >

                                        <Typography
                                            component = {'span'} 
                                            variant = {'body2'}
                                            textAlign = "start"
                                            sx = {{ 
                                                color: "#01668d",
                                                fontWeight: "bold", 
                                                fontSize: { xs: 20, sm: 15, md: 17 },
                                            }}
                                        >

                                            Miembros

                                        </Typography>

                                    </Button>

                                </Grid>
                            )
                        }

                        <Grid display={ editState ? "none" : "flex" }>

                            <Button
                                variant = "contained" 
                                onClick = { () => { 
                                    if(owner == 'yes') {
                                        openAlertDialog("Asigne un nuevo administrador para el grupo", <GroupMemberList {...{closeDialogAlert, leaveAction, communityID: id} }  />);
                                    }
                                    else {
                                        openAlertDialog("¿Seguro que quieres salir de este grupo?", <GroupsLayoutLeaveAlert {...{closeDialogAlert, leaveAction, id} }  />);
                                    }
                                }}
                                sx = {{
                                    backgroundColor: "white",
                                    pt: 1.5,
                                    pb: 1.5,
                                    pl: 3,
                                    pr: 3,
                                    mr: { xs: 0, sm: 2, md: 3, lg: 3, xl: 3},
                                    mb: { xs: 2, sm: 0, },
                                    ":hover": { backgroundColor: "#95bbcf" },
                                    width: { xs: "250px", sm: "110px", md: "130px" },
                                }}
                            >

                                <Typography
                                    component = {'span'} 
                                    variant = {'body2'}
                                    textAlign = "start"
                                    sx = {{ 
                                        color: "#01668d",
                                        fontWeight: "bold", 
                                        fontSize: { xs: 20, sm: 15, md: 17 },
                                    }}
                                >

                                    Salir

                                </Typography>

                            </Button>

                        </Grid>     

                        <Grid display={ editState ? "none" : "flex" }>

                            <Button
                                variant = "contained" 
                                onClick = { shareGroup }
                                sx = {{
                                    backgroundColor: "white",
                                    pt: 1.5,
                                    pb: 1.5,
                                    pl: 3,
                                    pr: 3,
                                    ":hover": { backgroundColor: "#95bbcf" },
                                    width: { xs: "250px", sm: "130px", md: "150px" },
                                }}
                            >

                                <Typography
                                    component = {'span'} 
                                    variant = {'body2'}
                                    textAlign = "start"
                                    sx = {{ 
                                        color: "#01668d",
                                        fontWeight: "bold", 
                                        fontSize: { xs: 20, sm: 15, md: 17 },
                                    }}
                                >

                                    Compartir

                                </Typography>

                            </Button>

                        </Grid>

                    </Grid>

                </Grid>

            </Grid>

            </Grid>

            <Grid 
                sx = {{
                    pb: 5,
                }}
            />

            { newAlert() }
            { newToast() }

        </>

    );
}