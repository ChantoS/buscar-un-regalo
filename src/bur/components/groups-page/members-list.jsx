import { Avatar, Button, Grid, List, ListItem, Typography } from "@mui/material";
import { apiURL } from "../../../helpers";
import { useUserData } from "../../../hooks/useUserData";
import { useGetCommunityMembersQuery } from "../../../store/api/giftsApi"
import { SelectAdminButton } from "./select-admin-button";

export const GroupMemberList = ({ communityID, closeDialogAlert }) => {

    const { myID } = useUserData();
    const { data } = useGetCommunityMembersQuery(communityID);

    if(data && data.code == 200)
    return (
        <>
            <List
                sx = {{
                    height: "100vh"
                }}
            >

                {
                    data.rows.map(row => (
                        <ListItem key={row.userID} >

                            <Grid
                                container
                                sx = {{
                                    backgroundColor: "#E6E6E6",
                                    mb: 3,
                                    borderRadius: 10,
                                    width: "100%",
                                }}
                            >

                                <Grid
                                    container
                                    justifyContent = "center"
                                    sx = {{
                                        mx: 5,
                                        py: 3,
                                        width: { xs: "100%", sm: "12.5%"}
                                    }}
                                >

                                    <Avatar 
                                        src={ row.userIMG.length > 0 ? `${apiURL}uploads/users/${row.userIMG}` : "" }
                                        sx = {{
                                            backgroundColor: "orange",
                                            width: "100px",
                                            height: "100px",
                                        }}
                                    />

                                </Grid>

                                <Grid
                                    container
                                    direction = "column"
                                    justifyContent = "center"
                                    sx = {{
                                        width: { xs: "100%", sm: "60%" },
                                    }}
                                >

                                    <Grid
                                        container
                                        justifyContent = {{ xs: "center", sm: "start" }}
                                        sx = {{
                                            width: "100%",
                                        }}
                                    >

                                        <Typography
                                            sx = {{
                                                color: "#00417d",
                                                fontSize: 30
                                            }}
                                        >

                                            {row.display_name}

                                        </Typography>

                                    </Grid>


                                    <Grid
                                        container
                                        justifyContent = {{ xs: "center", sm: "start" }}
                                        sx = {{
                                            pb: { xs: 3, sm: 0 }
                                        }}
                                    >

                                        <SelectAdminButton {...{myID, userID: row.userID, membersCount: data.rows.length, closeDialogAlert, communityID}} />

                                    </Grid>

                                </Grid>

                            </Grid>

                        </ListItem>
                    ))
                }

            </List>

        </>
    )
}
