import { Button, Grid, Typography } from "@mui/material"
import { useState } from "react"

export const GroupsLayoutLeaveAlert = ({ closeDialogAlert, leaveAction, id }) => {
    
    const [leavingGroup, setLeavingGroup] = useState(false);

    const leavingAction = (id) => {
        if(leavingGroup) return;
        setLeavingGroup(true);
        leaveAction(id, closeDialogAlert);
    }
    
    return (
        <Grid
            container
            direction = {{ xs: "column", sm: "row" }}
            justifyContent = "center"
            sx = {{
                pt: 3,
            }}
        >

            <Grid
                sx = {{
                    width: { xs: "100%", sm: "25%" },
                    mr: { xs: 0, sm: 5 },
                }}
            >

                <Button
                    onClick = { () => closeDialogAlert()}
                    sx = {{
                        backgroundColor: "#5494b3",
                        mr: 3,
                        ":hover": { backgroundColor: "#01668d" },
                        borderRadius: 3,
                        width: "100%",
                        py: 2,
                    }}
                >

                    <Typography
                        sx = {{
                            color: "white",
                            fontSize: 24,
                            width: "100%",
                        }}
                    >

                        Cancelar

                    </Typography>

                </Button>

            </Grid>

            <Grid
                sx = {{
                    width: { xs: "100%", sm: "25%" },
                    mt: { xs: 2, sm: 0 },
                }}
            >

                <Button
                    onClick = { () => leavingAction(id)}
                    sx = {{
                        backgroundColor: "#ba3232",
                        ":hover": { backgroundColor: "#cf2525" },
                        borderRadius: 3,
                        width: "100%",
                        py: 2,
                    }}
                >

                    <Typography
                        sx = {{
                            color: "white",
                            fontSize: 24,
                        }}
                    >

                        Salir

                    </Typography>   

                </Button>

            </Grid>

        </Grid>
    )
}
