
import { NavLink } from "react-router-dom";
import { Avatar, Button, Grid, Typography } from "@mui/material";


export const UserGiftLayout = () => {

    return (

        <Grid
            sx = {{
                mr: 5,
                ml: 5,
                mt: 5,
                mb: 5,
                }}
        >

            <Grid
                container
                justifyContent = "center"
                alignItems = "center"
                sx = {{
                    backgroundColor: "#6b92a9",
                    width: "300px",
                    pt: 5,
                    pb: 5,
                    borderRadius: "50px",
                }}
            >

                <Avatar 
                    src = { "/imgs/BUR-gift-logo.png"}
                    sx = {{
                        height: "170px",
                        width: "170px",
                    }}
                />

                <Grid
                    sx = {{
                        mt: 2,
                        pl: 3,
                        pr: 3,
                    }}
                >

                    <Typography
                        component = { 'span' } 
                        variant={ 'body2' }
                        textAlign = "center"
                        sx = {{
                            color: "white",
                            fontSize: 35
                        }}
                    >

                        Chocolate
                    
                    </Typography>

                </Grid>

                <Grid
                    sx = {{
                        mt: 2,
                    }}
                >

                    <Button
                        sx = {{
                            backgroundColor: "#00417d",
                            borderRadius: 4,
                            color: "white",
                            fontSize: 23,
                            pr: 3,
                            pl: 3,
                            ":hover": { backgroundColor: "#01557c" }
                        }}
                    >
                        
                        Ver regalo

                    </Button>

                </Grid>

            </Grid>

        </Grid>
    );
}
