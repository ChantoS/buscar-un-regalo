import { Button, Grid, Typography } from "@mui/material"
import { useState } from "react";

export const UserGiftAddAlert = ({ closeDialogAlert, addGift }) => {

    const [addingGift, setAddingGift] = useState(false);

    const addGiftAction = () => {
        if(addingGift) return;

        setAddingGift(true);
        addGift();
    };

    return (
        
        <Grid
            container
            direction = {{ xs: "column-reverse", sm: "row" }}
            justifyContent = "center"
            sx = {{
                pt: 3,
            }}
        >

            <Grid
                sx = {{
                    width: { xs: "100%", sm: "25%" },
                    mr: { xs: 0, sm: 5 },
                    mt: { xs: 2, sm: 0 }
                }}
            >

                <Button
                    onClick = { () => closeDialogAlert() }
                    sx = {{
                        backgroundColor: "#ba3232",
                        mr: 3,
                        ":hover": { backgroundColor: "#cf2525" },
                        borderRadius: 3,
                        width: "100%",
                        py: 2,
                    }}
                >

                    <Typography
                        sx = {{
                            color: "white",
                            fontSize: { xs: 18, sm: 24 },
                        }}
                    >

                        Cancelar

                    </Typography>

                </Button>

            </Grid>

            <Grid
                sx = {{
                    width: { xs: "100%", sm: "25%" },
                }}
            >

                <Button
                    onClick = { addGiftAction }
                    sx = {{
                        backgroundColor: "#5494b3",
                        ":hover": { backgroundColor: "#01668d" },
                        borderRadius: 3,
                        width: "100%",
                        py: 2,
                    }}
                >

                    <Typography
                        sx = {{
                            color: "white",
                            fontSize: { xs: 18, sm: 24 },
                        }}
                    >

                        Aceptar

                    </Typography>   

                </Button>

            </Grid>

        </Grid>

    );

}
