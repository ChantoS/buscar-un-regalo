import { Button, Grid, Typography } from "@mui/material";
import { useCustomAlerts } from "../../../hooks/useCustomAlerts";
import { useForm } from "../../../hooks/useForm";
import { usePostRecoverCodeMutation } from "../../../store/api/giftsApi";

export const RecoverPasswordComponent = ({ closeDialogAlert }) => {

    const { email, onInputChange } = useForm({ email: '' }); 
    const [ sendCode ] = usePostRecoverCodeMutation();

    const { openToast, newToast } = useCustomAlerts();

    const sendRecoverCode = async (email) => {

        if(email.length <= 0) {
            openToast("El correo no puede estár vacío", 'warning');
            return;
        }

        const { data, error } = await sendCode({ email });

        if(data && data.code == 200) {
            closeDialogAlert();
            openToast("Correo de recuperación enviado", 'success');
        }
        else if (error) {
            openToast("Problemas al enviar el correo de recuperación", 'error');
        }
    }

    return (

        <Grid>

            { newToast() }

            <Grid
                container
                justifyContent = "center"
                sx = {{
                    px: 5,
                }}
            >

                <Typography
                    sx = {{
                        color: "#00417d",
                        fontSize: { xs: 15, sm: 17, md: 20 },
                        textAlign: "left"
                    }}
                >

                    En este momento tenemos problemas para entregar el código, por favor escribanos a buscaunregalo@patozgaming.com para solicitando el reinicio de su contraseña.
                    {/* Por favor escriba su correo, si existe en nuestro servicio, le mandaremos un correo para que pueda cambiar su contraseña, este código solo durará 15 minutos después de enviado el correo. */}

                </Typography>

            </Grid>

            <Grid
                container
                justifyContent = "center"
                sx = {{
                    mt: 3
                }}
            >

                {/* <input 
                    className = "recoverPasswordInput"
                    name="email"
                    value={email}
                    onChange={onInputChange}
                /> */}

            </Grid>

            <Grid
                container
                direction = {{ xs: "column-reverse", sm: "row" }}
                justifyContent = "center"
                sx = {{
                    pt: 3,
                }}
            >

                <Grid
                    sx = {{
                        width: { xs: "100%", sm: "25%" },
                        mr: { xs: 0, sm: 5 },
                        mt: { xs: 2, sm: 0 }
                    }}
                >

                    <Button
                        onClick={closeDialogAlert}
                        sx = {{
                            backgroundColor: "#B03636",
                            py: 2.5,
                            px: 5,
                            mr: 10,
                            ":hover": { backgroundColor: "#C44F4F" },
                            width: "100%",
                        }}
                    >

                        <Typography
                            sx = {{
                                color: "white",
                                fontSize: { xs: 18, sm: 24 },
                            }}
                        >
                            Cerrar
                            {/* Cancelar */}

                        </Typography>

                    </Button>

                </Grid>

                {/* <Grid
                    sx = {{
                        width: { xs: "100%", sm: "25%" },
                    }}
                >

                    <Button
                        onClick={() => sendRecoverCode(email)}
                        sx = {{
                            backgroundColor: "#00417d",
                            py: 2.5,
                            px: 5,
                            ":hover": { backgroundColor: "#95bbcf" },
                            width: "100%",
                        }}
                    >

                        <Typography
                            sx = {{
                                color: "white",
                                fontSize: { xs: 18, sm: 24 },
                            }}
                        >

                            Enviar

                        </Typography>

                    </Button>

                </Grid> */}

            </Grid>

        </Grid>

    );

}
