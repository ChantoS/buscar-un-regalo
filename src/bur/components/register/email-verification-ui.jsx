import { Button, Grid, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "../../../hooks/useForm";
import { onResetErrorMessage } from "../../../store/register/registerSlice";
import { ErrorIndicator } from "../global/error-indicator";

export const EmailVerificationUI = ({ user, submitCode, closeDialogAlert = () => {} }) => {

    const { code, onInputChange } = useForm({ code: '' }); 
    const [loading, setloading] = useState(false);
    const { errorMessage } = useSelector(state => state.register);
    const dispatch = useDispatch();

    const startAction = () => {
        if(code.length > 0) {
            setloading(true);
            submitCode(code, user);
        }
    }

    useEffect(() => {
        dispatch(onResetErrorMessage());
    }, []);

    useEffect(() => {
        if(errorMessage.length > 0) {
            setloading(false);
        } 
    });

    useEffect(() => {
        dispatch(onResetErrorMessage());
    }, [code])
    

    const closeWindow = () => {
        setloading(false);
        closeDialogAlert();
    }

    return (

        <Grid>

            <Grid
                container
                justifyContent = "center"
                sx = {{
                    px: 3,
                }}
            >

                <Typography
                    sx = {{
                        color: "#00417d",
                        fontSize: { xs: 20, sm: 20, md: 25 },
                        textAlign: "center"
                    }}
                >

                    Se ha enviado un código de verificación a este correo, para continuar verifique su código aquí

                </Typography>

            </Grid>

            <Grid
                container
                justifyContent = "center"
                sx = {{
                    mt: 3
                }}
            >

                <Grid
                    sx = {{
                        width: { xs: "80%", sm: "30%", md: "30%" }
                    }}
                >
                    <ErrorIndicator message={ errorMessage } />

                    <input 
                        className = "verificationEmailInput"
                        name = "code"
                        value = { code }
                        onChange = { onInputChange }
                        maxLength = { 6 }
                    />

                </Grid>

            </Grid>

            <Grid
                direction = {{ xs: "column-reverse", sm: "row" }}
                container
                justifyContent = "center"
                sx = {{
                  pt: 5,
                }}
            >

                <Grid
                    container 
                    justifyContent = "center"
                    sx = {{
                      width: { xs: "100%", sm: "30%" },
                      mt: { xs: 2, sm: 0 },
                      mr: { xs: 0, sm: 5 },
                    }}
                >

                    <Button
                        onClick = { closeWindow }
                        sx = {{
                            backgroundColor: "#B03636",
                            py: 2.5,
                            px: 5,
                            ":hover": { backgroundColor: "#C44F4F" },
                            width: "100%",
                        }}
                    >

                        <Typography
                            sx = {{
                                color: "white",
                                fontSize: { xs: 15, sm: 20, md: 20 },
                            }}
                        >

                            Cancelar

                        </Typography>

                    </Button>

                </Grid>

                <Grid
                    container 
                    justifyContent = "center"
                    sx = {{
                      width: { xs: "100%", sm: "30%" },
                      mt: { xs: 2, sm: 0 },
                    }}
                >

                    <Button
                        onClick={() => startAction()}
                        disabled={loading}
                        sx = {{
                            backgroundColor: "#00417d",
                            py: 2.5,
                            px: 5,
                            ":hover": { backgroundColor: "#95bbcf" },
                            width: "100%",
                        }}
                    >

                        <Typography
                            sx = {{
                                color: "white",
                                fontSize: { xs: 15, sm: 20, md: 20 },
                            }}
                        >

                            Verificar
                            {/* { loading ? <CircularProgress /> : "Verificar" } */}

                        </Typography>

                    </Button>

                </Grid>

            </Grid>

        </Grid>

    );

}
