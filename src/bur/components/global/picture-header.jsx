import { Avatar, Grid, Typography } from "@mui/material"
import { Link } from "react-router-dom";

export const PictureHeader = ({ title, img='' }) => {
  return (

    <>
        <Grid
            container
            justifyContent = "center"
            sx = {{
                mt: { xs: 10, sm: 25, md: 3 }
            }}
        >

            <Grid
                container
                justifyContent = "center"
                alignItems = "center"
                sx = {{
                    width: { xs: "20%", sm: "25%" }
                }}
            >

                <Grid 
                    className = "roundedRectangle"
                    sx = {{
                        backgroundColor: "#5494b3",
                        height: { xs: "8px", sm: "15px" },
                        width: "100%",
                        borderTopLeftRadius: 100,
                        borderTopRightRadius: 25,
                        borderBottomRightRadius: 25,
                    }}
                />

            </Grid>

            <Grid
                sx = {{
                    mr: { xs: 2, sm: 5 },
                    ml: { xs: 2, sm: 5 },
                }}
            >

                { img.length > 2
                    ? (
                        <a href={img}>
                            <Avatar
                                src={img}
                                sx = {{
                                    backgroundColor: "orange",
                                    height: 150,
                                    width: 150, 
                                }}
                            />
                        </a>
                    )
                    : (
                        <Avatar
                            src={img}
                            sx = {{
                                backgroundColor: "orange",
                                height: 150,
                                width: 150, 
                            }}
                        />
                    )
                }
                

            </Grid>

            <Grid
                container
                justifyContent = "center"
                alignItems = "center"
                sx = {{
                    width: { xs: "20%", sm: "25%" }
                }}
            >

                <Grid 
                    className = "roundedRectangle"
                    sx = {{
                        backgroundColor: "#5494b3",
                        height: { xs: "8px", sm: "15px" },
                        width: "100%",
                        borderTopRightRadius: 100,
                        borderTopLeftRadius: 25,
                        borderBottomLeftRadius: 25,
                    }}
                />

            </Grid>

        </Grid>

        <Grid
            container
            justifyContent = "center"
            sx = {{
                pt: 2,
            }}
        >

            <Typography
                sx = {{
                    color: "white",
                    fontSize: { xs: 30, sm: 45},
                }}
            >

            { title }

            </Typography>

        </Grid>

    </>

  );

}
