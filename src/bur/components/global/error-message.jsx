import { Typography } from '@mui/material'

export const ErrorMessage = ({ message, color = 'red'}) => {

  return (

    <Typography
        sx={{
            color: color
        }}
    >

      {message}

    </Typography>

  );

}
