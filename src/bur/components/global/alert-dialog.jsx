import { Dialog, DialogContent, DialogTitle } from "@mui/material";

export const AlertDialog = ({ title, content, open, closeAction}) => {

    return (

        <Dialog
            className = "dialog"
            open = { open }
            fullWidth = { true }
            maxWidth = "md"
            onClose = { closeAction } 
        >

            <DialogTitle
                sx = {{
                    textAlign: "center",
                    pt: 5,
                    fontSize: { xs: 23, md: 30 },
                    color: "#00417d",
                    pl: 5,
                    pr: 5,
                }}
            >

                { title }

            </DialogTitle>

            <DialogContent
                sx = {{
                    pb: 5,
                }}
            >

                { content }

            </DialogContent>

        </Dialog>

    );

}
