import { Alert, Snackbar } from '@mui/material'
import React from 'react'

export const ToastAlert = ({ open, timeToHide = 6000, closeToast, toastType, message }) => {
  
  let sxprops = {
    width: '100%',
    backgroundColor: 'black',
    color: 'white'
  }

  switch(toastType) {
    case 'success': sxprops.backgroundColor = "#08830F";
      break;
    case 'warning': sxprops.backgroundColor = "#CF7801";
      break;
    case 'info': sxprops.backgroundColor = "#0579AF";
      break;
    case 'error': sxprops.backgroundColor = "#C00303";
      break;
  }

  return (
    <Snackbar open={open} autoHideDuration={timeToHide} onClose={closeToast}>
        <Alert onClose={closeToast} severity={toastType} sx={sxprops}>
            { message }
        </Alert>
    </Snackbar>
  )
}
