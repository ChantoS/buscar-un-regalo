import { Grid, Typography } from '@mui/material'
import React from 'react'

export const ErrorIndicator = ({ message, color = 'red', size = 14 }) => {
  return (
    <Grid
      display = "flex"
      sx = {{
          width: "100%",
          mt: 1,
      }}
      >

      <Typography
          sx = {{
            color: color,
            fontSize: size
          }}
      >

          { message }

      </Typography>

    </Grid>
  )
}
