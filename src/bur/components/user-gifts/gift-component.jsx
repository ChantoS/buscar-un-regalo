import { Avatar, Button, Grid, Typography } from '@mui/material'
import React from 'react'
import { apiURL } from '../../../helpers'

export const GiftComponent = ({img, name, description, id, reserved, owner, viewGift}) => {
  return (

    <Grid
      sx = {{
        mr: 5,
        ml: 5,
        mb: 5,
      }}
    >

      <Grid
        className =  { reserved == 1 && owner != "yes" ? "reservedGift" : "openGift" }
        container
        justifyContent = "center"
        alignItems = "center"
        sx = {{
            // width: "300px",
            width: { xs: "250px", sm: "300px" },
            pt: 5,
            pb: 5,
            borderRadius: "50px",
        }}
      >

        <Grid
          container
          justifyContent = "center"
          sx = {{
            width: "100%"
          }}
        >

          <Avatar
              src = { img.length > 1 ? `${apiURL}uploads/gifts/${img}` : '/imgs/BUR-gift-logo.png' }
              sx = {{
                  height: { xs: "115px", sm: "170px" },
                  width: { xs: "115px", sm: "170px" },
              }}
          />

        </Grid>

          <Grid
            direction = "column"
            container
            justifyContent = "center"
            alignItems = "center"
            sx = {{
              width: "100%",
              mt: 3,
            }}
          >

            <Grid
              sx = {{
                px: 2,
              }}
            >

              <Typography
                textAlign = "center"
                sx = {{
                    color: "white",
                    fontSize: { xs: 20, sm: 25 }
                }}
              >

                {name}
                
              </Typography>
              
            </Grid>

            <Grid
              container
              justifyContent = "center"
              alignContent = "center"
              sx = {{
                width: "100%",
                mt: 3,
              }}
            >

              <Button
                onClick={() => viewGift({id, name, description, img, reserved, owner})}
                sx = {{
                    backgroundColor: "#00417d",
                    borderRadius: 4,
                    color: "white",
                    fontSize: { xs: 18, sm: 23 },
                    pr: 3,
                    pl: 3,
                    ":hover": { backgroundColor: "#01557c" },
                }}
              >

                Ver regalo

              </Button>
              
            </Grid>

          </Grid>

      </Grid>

    </Grid>

  );

}
