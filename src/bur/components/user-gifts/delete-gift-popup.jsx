import { Button, Grid, Typography } from '@mui/material'
import React from 'react'
import { useState } from 'react'

export const DeleteGiftPopup = ({ closeDialogAlert, deleteGiftByID }) => {

    const [deletingGift, setDeletingGift] = useState(false);

    const deleteGift = () => {
        if(deletingGift) return;

        setDeletingGift(true);
        deleteGiftByID();
    };

  return (
    <Grid
        container
        direction = {{ xs: "column-reverse", sm: "row" }}
        justifyContent = "center"
        sx = {{
            pt: 3,
        }}
    >

        <Grid
            sx = {{
                width: { xs: "100%", sm: "25%" },
                mr: { xs: 0, sm: 5 },
                mt: { xs: 2, sm: 0 }
            }}
        >

            <Button
                onClick = { () => closeDialogAlert()}
                sx = {{
                    backgroundColor: "#01668d",
                    pt: 1.5,
                    pb: 1.5,
                    ":hover": { backgroundColor: "#5494b3" },
                    borderRadius: 5,
                    width: "100%",
                }}
            >

                <Typography
                    sx = {{
                        color: "white",
                        fontSize: { xs: 18, sm: 24 },
                    }}
                >

                    Cancelar

                </Typography>

            </Button>

        </Grid>

        <Grid
            sx = {{
                width: { xs: "100%", sm: "25%" },
            }}
        >

            <Button
                onClick = { () => deleteGift() }
                sx = {{
                    backgroundColor: `#cf2525`,
                    pt: 1.5,
                    pb: 1.5,
                    ":hover": { backgroundColor: `#ba3232` },
                    borderRadius: 5,
                    width: "100%",
                }}
            >

                <Typography
                    sx = {{
                        color: "white",
                        fontSize: { xs: 18, sm: 24 },
                    }}
                >

                    Eliminar

                </Typography>   

            </Button>

        </Grid>

    </Grid>
  )
}
