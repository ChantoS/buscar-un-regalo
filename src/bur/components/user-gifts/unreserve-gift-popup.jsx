import { Button, Grid, Typography } from '@mui/material'

export const UnreserveGiftPopup = ({ closeDialogAlert, unreserveGiftByID }) => {

  return (

    <Grid
        container
        direction = {{ xs: "column-reverse", sm: "row" }}
        justifyContent = "center"
        sx = {{
            pt: 3,
        }}
    >

        <Grid
            sx = {{
                width: { xs: "100%", sm: "40%", md: "30%" },
                mr: { xs: 0, sm: 5 },
                mt: { xs: 2, sm: 0 }
            }}
        >

            <Button
                onClick = { () => closeDialogAlert()}
                sx = {{
                    backgroundColor: "#01668d",
                    mr: 3,
                    pt: 1.5,
                    pb: 1.5,
                    ":hover": { backgroundColor: "#5494b3" },
                    borderRadius: 5,
                    width: "100%",
                }}
            >

                <Typography
                    sx = {{
                        color: "white",
                        fontSize: { xs: 18, sm: 18, md: 20 },
                    }}
                >

                    Cancelar

                </Typography>

            </Button>

        </Grid>

        <Grid 
            sx = {{
                width: { xs: "100%", sm: "40%", md: "30%" },
            }}
        >

            <Button
                onClick = { () => unreserveGiftByID()}
                sx = {{
                    backgroundColor: `#cf2525`,
                    pt: 1.5,
                    pb: 1.5,
                    ":hover": { backgroundColor: `#ba3232` },
                    borderRadius: 5,
                    width: "100%",
                }}
            >

                <Typography
                    sx = {{
                        color: "white",
                        fontSize: { xs: 18, sm: 18, md: 20 },
                    }}
                >

                    Quitar aparatado

                </Typography>   

            </Button>

        </Grid>

    </Grid>

  );

}
