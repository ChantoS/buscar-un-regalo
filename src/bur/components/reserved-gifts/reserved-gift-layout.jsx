import { NavLink, useNavigate } from "react-router-dom";
import { Avatar, Button, Grid, Typography } from "@mui/material";
import { useDispatch } from "react-redux";
import { getGiftData } from "../../../store/gifts/giftsSlice";
import { apiURL } from "../../../helpers";


export const ReservedGiftsLayout = ({ 
    id, 
    name, 
    description, 
    img = '', 
    reserved, 
    owner,
    communityID, 
    display_name, 
    userID
}) => {

    const navigate = useNavigate();
    const dispatch = useDispatch();

    const viewGift = ( gift ) => {
        dispatch( getGiftData({...gift, communityID}) );
        navigate('/gifts/view');
    }

    return (

        <Grid
            sx = {{
                mb: 5,
                ml: 5,
                mr: 5,
            }}
        >

            <Grid
                container
                justifyContent = "center"
                alignItems = "center"
                sx = {{
                    backgroundColor: "#6b92a9",
                    width: "300px",
                    pt: 5,
                    pb: 5,
                    borderRadius: "50px",
                }}
            >

                <Avatar 
                    src = { img.length > 1 ? `${apiURL}uploads/gifts/${img}` : "/imgs/BUR-gift-logo.png"}
                    sx = {{
                        height: "170px",
                        width: "170px",
                    }}
                />

                <Grid 
                    sx = {{
                        mt: 2,
                        pl: 3,
                        pr: 3,
                    }}
                >

                    <Grid
                        sx = {{
                            textAlign: "center"
                        }}
                    >

                        <Typography
                            component = { 'span' } 
                            variant={ 'body2' }
                            textAlign = "center"
                            sx = {{
                                color: "white",
                                fontSize: 35
                            }}
                        >

                            { name }
                        
                        </Typography>

                    </Grid>

                    <Grid
                        sx = {{
                            textAlign: "center"
                        }}
                    >

                        <Typography
                            component = { 'span' } 
                            variant={ 'body2' }
                            textAlign = "center"
                            sx = {{
                                color: "white",
                                fontSize: 20,
                                opacity: 0.8
                            }}
                        >

                            { `(de ${display_name})` }
                        
                        </Typography>

                    </Grid>

                </Grid>

                <Grid
                    sx = {{
                        mt: 2,
                    }}
                >

                    <Button
                        onClick={() => viewGift({id, name, description, img, reserved, owner})}
                        sx = {{
                            backgroundColor: "#00417d",
                            borderRadius: 4,
                            color: "white",
                            fontSize: 23,
                            pr: 3,
                            pl: 3,
                            ":hover": { backgroundColor: "#01557c" }
                        }}
                    >
                        
                        Ver regalo

                    </Button>

                </Grid>

            </Grid>

        </Grid>
    );
}
