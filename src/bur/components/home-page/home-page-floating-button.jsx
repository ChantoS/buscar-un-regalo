import { Grid, IconButton } from "@mui/material"
import { NavLink } from "react-router-dom"
import { Add } from "@mui/icons-material";
import { useAuthStore } from "../../../hooks/useAuthStore";


export const HomePageFloatingButton = () => {

    const {status} = useAuthStore();
  
    if (status === 'authenticated') 

    return (
        
        <IconButton
            size = "large"
            sx = {{
                color: "white",
                backgroundColor: "white",
                ':hover': { backgroundColor: "#015c84" },
                position: "fixed",
                right: { xs: 35, sm: 35, md: 80 },
                bottom: { xs: 35, sm: 35, md: 80 },
                zIndex: 1400,
            }}
        >

            <Grid 
                width = {50}
                height = {50}
                alignItems = "center"
                justifyContent = "center"
            >

                <NavLink to='/gifts/add'>
                    <Add 
                        sx = {{
                            color: "#00417d",
                            fontSize: 50,
                        }}
                    />
                </NavLink>

            </Grid>

        </IconButton>

    );
}