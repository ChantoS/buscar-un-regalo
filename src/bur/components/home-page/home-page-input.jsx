import { Button, FormControl, Grid, InputAdornment, MenuItem, Select, TextField, Typography } from "@mui/material";
import ManageSearchIcon from '@mui/icons-material/ManageSearch';

export const HomePageInput = ({ onSubmitInput, communities, searchInputChange, searchValue, topicValue }) => {

  return (
    <form onSubmit={ onSubmitInput }>
      <Grid
        sx = {{
          width: "100vw",
          mt: { xs: 25, sm: 30},
          mb: 3,
        }}
      >

        <Grid
          justifyContent = "center"
          container
          sx = {{
            pr: 4,
            pl: 4,
          }}
        >

          <Typography
            component = {'span'} 
            variant = {'body2'}
            textAlign = "center"
            sx = {{ 
              color: "white",
              fontWeight: "bold", 
              fontSize: { 
                xs: 30, 
                sm: 35 
              },
            }}
          >

            Busca un usuario o busca un regalo

          </Typography>

        </Grid>

        <Grid
          container
          justifyContent = "center"
        >

          <Grid
            container
            justifyContent = "center"
            sx = {{
              pt: { 
                xs: 5, 
                sm: 5,
                md: 8

              },
              width: { 
                xs: "85%", 
                sm: "78%", 
                md: "65%"
              },
            }}
          >

            <TextField 
              placeholder = "¿Que buscas...?"
              variant = "standard"
              name = "searchbox"
              value = { searchValue }
              onChange = { searchInputChange }
              InputProps = {{
                disableUnderline: true,
                sx: {
                  boxSizing: "initial",
                  padding: 2.5,
                  fontWeight: "bold",
                  fontSize: 20
                },
                endAdornment: (

                  <InputAdornment position = "start">

                    <Button
                      type="submit"
                    >
                      <ManageSearchIcon 
                        sx = {{
                          color: "#004f77",
                          fontSize: 35
                        }}
                      />
                    </Button>

                  </InputAdornment>

                ),
                startAdornment: (

                  <Grid
                    sx = {{
                      mr: { 
                        xs: 2, 
                        sm: 5,
                      }
                    }}
                  >

                    <FormControl
                      sx = {{
                        border: "none"
                      }}
                      onSubmit={onSubmitInput}
                    >

                      <Select
                        defaultValue = { 0 }
                        name = "topic"
                        value = {topicValue}
                        onChange = { searchInputChange }
                        sx = {{
                          width: { xs: "90px", sm: "200px" },
                          textAlign: { xs: "start", sm: "end" },
                          '.MuiOutlinedInput-notchedOutline': { border: 0 }
                        }}
                      >

                        <MenuItem value = { 0 }>

                          <Typography
                              component = {'span'} 
                              variant = {'body2'}
                              sx = {{ 
                                  color: "#003e65",
                                  fontWeight: "bold", 
                                  fontSize: { 
                                    xs: 15, 
                                    sm: 25,
                                  },
                                  mr: 5,
                              }}
                          >

                            Usuarios

                          </Typography>

                        </MenuItem>

                        <MenuItem value = { 1 }>

                          <Typography
                              component = {'span'} 
                              variant = {'body2'}
                              sx = {{ 
                                  color: "#003e65",
                                  fontWeight: "bold", 
                                  fontSize: { 
                                    xs: 15, 
                                    sm: 25,
                                  },
                                  mr: 5,
                              }}
                          >

                            Regalos / Públicos

                          </Typography>

                        </MenuItem>

                        {
                          communities.map(community => (
                            <MenuItem key={community.id} value = { community.id + 2 }>

                              <Typography
                                  component = {'span'} 
                                  variant = {'body2'}
                                  sx = {{ 
                                      color: "#003e65",
                                      fontWeight: "bold", 
                                      fontSize: { 
                                        xs: 15, 
                                        sm: 25, 
                                      },
                                      mr: 5,
                                  }}
                              >

                                Regalos / { community.name }

                              </Typography>

                            </MenuItem>
                          ))
                        }

                      </Select>

                    </FormControl>

                  </Grid>

                ),
              }}
              style = {{
                underline: "none",
                borderRadius: 30,
                backgroundColor: "white",
                width: "100%",
              }}
            />

          </Grid>

        </Grid>

      </Grid>

    </form>

  );
  
}
