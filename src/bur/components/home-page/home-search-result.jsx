import { Avatar, Grid, Typography } from '@mui/material'
import { apiURL } from '../../../helpers';

export const HomeSearchResult = ({ 
  id,
  name, 
  description = '',
  img, 
  display_name = '',
  reserved,
  owner, 
  topic, 
  clickable, 
  communityID
}) => {

  return (

    <Grid
      onClick={() => clickable({id, name, description, img, display_name, reserved, owner, communityID})}
      container
      alignContent = "center"
      justifyContent = "center"
      sx = {{
        px: 5,
        py: 3,
        backgroundColor: "#4786a4",
        mb: 3,
        borderRadius: 15,
        border: 5,
        borderColor: "white",
        cursor: 'pointer',
        width: "100%"
      }}
    >

      <Grid
        display = {{ sm: "flex" }}
        sx = {{
          width: "100%",
        }}
      >

        <Grid
          container
          justifyContent = {{ xs: "center", sm: "start" }}
          sx = {{
            width: { xs: "100%", sm: "40%", md: "15%"},
          }}
        >

          <Avatar 
            alt = "avatar"
            src = { img.length > 1 ? `${apiURL}uploads/${topic === 0 ? 'users' : 'gifts'}/${img}` : '' }
            sx = {{ 
              width: 120, 
              height: 120 
            }}
          />

        </Grid>

        <Grid
          container
          alignItems = "center"
          justifyContent = {{ xs: "center", sm: "start" }}
          sx = {{
            textAlign: "center",
            width: { xs: "100%", sm: "80%", md: "80%", lg: "70%" },
            pt: { xs: 2, sm: 0 },
            ml: { xs: 0, sm: 0, md: 10, lg: 7, xl: 5}
          }}
        >

          <Typography
            sx = {{
              fontSize: { xs: 20, sm: 25, md: 35 },
              color: "white"
            }}
          >

            { display_name.length > 0 ? `${name} (de ${display_name})` : name }

          </Typography>

        </Grid>

      </Grid>

    </Grid> 

  );

}
