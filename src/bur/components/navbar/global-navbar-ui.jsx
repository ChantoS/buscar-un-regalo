import { AppBar, Grid, Box, Typography } from "@mui/material";
import { useDispatch } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { useAuthStore } from "../../../hooks/useAuthStore";
import { onLogout } from "../../../store/auth/authSlice";
import MenuIcon from '@mui/icons-material/Menu';
import { useState } from "react";
import CancelIcon from '@mui/icons-material/Cancel';

export const GlobalNavbar = () => {

    const { status } = useAuthStore();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [ burgerMenu, setBurgerMenu] = useState('hiddenNavbarMenu');

    const handleLogout = () => {
        dispatch(onLogout());
        localStorage.clear();
        navigate('/', {
            replace: true
        });
    }

    const showHamburgerButton = () => {
        
        if (burgerMenu == 'hiddenNavbarMenu') {
            setBurgerMenu('showingNavbarMenu');
        } 

        else {
            setBurgerMenu('hiddenNavbarMenu');
        }
    }

    const hideHamburguerButton = () => {
        setBurgerMenu('hiddenNavbarMenu');
    }

    return (

        <>
        
            <AppBar 
                sx = {{
                    top: "15px",
                    boxShadow: "20",
                    position: "absolute"
                }}
            >

                <Grid 
                    display = "flex"
                    alignItems = "center"
                    sx = {{ 
                        backgroundColor: "#6b98ae",
                        borderBottom: 5,
                        borderColor: "white",
                        height: "130px",
                        maxHeight: "160px",
                    }}
                >

                    <Box 
                        component = "img"
                        src = "/imgs/BUR-logo-dark.png"
                        sx = {{
                            height: { xs: 80, sm: 90 },
                            width: { xs: 160, sm: 210 },
                            cursor: "pointer",
                            ml: { xs: 3.5 },
                            position: "absolute",
                        }}
                        onClick = { () => navigate('/') }
                    />

                    <Grid 
                        container
                        justifyContent = "end"
                        visibility = {{ 
                            xs: "hidden", 
                            xl: "visible",
                        }}
                    >

                        <Typography
                            textAlign = "center"
                            component = { 'span' } 
                            variant={ 'body2' }
                            sx = {{ 
                                fontWeight: "bold", 
                                fontSize: 20,
                                backgroundColor: "#4786a4",
                                borderRadius: 4,
                                width: "180px",
                                mr: 5,
                                pt: 2,
                                pb: 2,
                                borderBottom: 4,
                                borderColor: "#00417d",
                            }}
                        >
                            
                            <Link 
                                to = '/'
                                style = {{ 
                                    textDecoration: "none",
                                    color: "#00417d",
                                }}
                            > 

                                PERSONAS

                            </Link>

                        </Typography>

                        <Typography
                            textAlign = "center"
                            component = { 'span' } 
                            variant={ 'body2' }
                            sx = {{ 
                                display: status === "authenticated" ? "" : "none",
                                fontWeight: "bold", 
                                fontSize: 20,
                                backgroundColor: "#4786a4",
                                borderRadius: 4,
                                width: "180px",
                                mr: 5,
                                pt: 2,
                                pb: 2,
                                borderBottom: 4,
                                borderColor: "#00417d",
                            }}
                        >
                            
                            <Link 
                                to = '/groups/mygroup'
                                style = {{ 
                                    textDecoration: "none",
                                    color: "#00417d",
                                }}
                            > 

                                MIS GRUPOS

                            </Link>

                        </Typography>

                        <Typography
                            textAlign = "center"
                            component = { 'span' } 
                            variant={ 'body2' }
                            sx = {{ 
                                display: status === "authenticated" ? "" : "none",
                                fontWeight: "bold", 
                                fontSize: 20,
                                backgroundColor: "#4786a4",
                                borderRadius: 4,
                                width: "180px",
                                mr: 5,
                                pt: 2,
                                pb: 2,
                                borderBottom: 4,
                                borderColor: "#00417d",
                            }}
                        >
                            
                            <Link 
                                to = '/gifts/mygifts'
                                style = {{ 
                                    textDecoration: "none",
                                    color: "#00417d",
                                }}
                            > 

                                MIS REGALOS

                            </Link>

                        </Typography>
                        
                        <Typography
                            textAlign = "center"
                            component = { 'span' } 
                            variant={ 'body2' }
                            sx = {{ 
                                display: status === "authenticated" ? "" : "none",
                                fontWeight: "bold", 
                                fontSize: 20,
                                backgroundColor: "#4786a4",
                                borderRadius: 4,
                                width: "180px",
                                mr: 5,
                                pt: 2,
                                pb: 2,
                                borderBottom: 4,
                                borderColor: "#00417d",
                            }}
                        >
                            
                            <Link 
                                to = '/groups/reservegift'
                                style = {{ 
                                    textDecoration: "none",
                                    color: "#00417d",
                                }}
                            > 

                                APARTADOS

                            </Link>

                        </Typography>

                        <Typography
                            textAlign = "center"
                            component = { 'span' } 
                            variant={ 'body2' }
                            sx = {{ 
                                display: status === "authenticated" ? "" : "none",
                                fontWeight: "bold", 
                                fontSize: 20,
                                backgroundColor: "#4786a4",
                                borderRadius: 4,
                                width: "180px",
                                mr: 5,
                                pt: 2,
                                pb: 2,
                                borderBottom: 4,
                                borderColor: "#00417d",
                            }}
                        >
                            
                            <Link 
                                to = '/profile'
                                style = {{ 
                                    textDecoration: "none",
                                    color: "#00417d",
                                }}
                            > 

                                PERFIL

                            </Link>

                        </Typography>

                        <Typography
                            textAlign = "center"
                            component = { 'span' } 
                            variant={ 'body2' }
                            sx = {{ 
                                display: status !== "authenticated" ? "" : "none",
                                fontWeight: "bold", 
                                fontSize: 20,
                                backgroundColor: "#4786a4",
                                borderRadius: 4,
                                width: "180px",
                                mr: 5,
                                pt: 2,
                                pb: 2,
                                borderBottom: 4,
                                borderColor: "#00417d",
                            }}
                        >
                            
                            <Link 
                                to = '/auth/login'
                                style = {{ 
                                    textDecoration: "none",
                                    color: "#00417d",
                                }}
                            > 

                                LOGIN

                            </Link>

                        </Typography>

                        <Typography
                            onClick = { () => handleLogout }
                            textAlign = "center"
                            component = { 'span' } 
                            variant={ 'body2' }
                            sx = {{ 
                                display: status === "authenticated" ? "" : "none",
                                fontWeight: "bold", 
                                fontSize: 20,
                                backgroundColor: "#4786a4",
                                borderRadius: 4,
                                width: "200px",
                                mr: 5,
                                pt: 2,
                                pb: 2,
                                borderBottom: 4,
                                borderColor: "#00417d",
                                color: "#00417d",
                                cursor: "pointer",
                            }}
                        >
                            
                            <Box
                                sx={{
                                    cursor: "pointer",
                                    color: "#00417d"
                                }}
                                onClick={handleLogout}
                            >

                                CERRAR SESIÓN

                            </Box>


                        </Typography>


                    </Grid>
                    
                    <MenuIcon   
                        onClick = { () => showHamburgerButton() }
                        sx = {{
                            position: "absolute",
                            right: 0,
                            fontSize: { 
                                xs: 50, 
                                md: 60, 
                            },
                            visibility: { 
                                xs: "visible", 
                                xl: "hidden", 
                            },
                            mr: {
                                xs: 6, 
                            },
                            cursor: "pointer"
                        }}
                    />
                    
                </Grid>
            
            </AppBar>

            <Grid
                display = "flex"
                className = { burgerMenu }
                sx = {{
                    width: { xs: "65%", sm: "40%" },
                    height: { xs: "100vh", sm: "700px" },
                    position: "absolute",
                    top: 0,
                    right: 0,
                    zIndex: 1500,
                    backgroundColor: "#01668d",
                    borderTopLeftRadius: 50,
                    borderBottomLeftRadius: 30,
                    borderLeft: 5,
                    borderColor: "white",
                }}
            >

                <Grid
                    container
                    alignItems = "center"
                    sx = {{
                        width: "15%",
                        height: "14vh",
                    }}
                >
                    
                    <Grid
                        sx = {{
                            position: "absolute",
                            left: { xs: 5, sm: 20 },
                            top: 30,
                            padding: 1,
                        }}
                        onClick = { () => showHamburgerButton() }
                    >

                        <CancelIcon
                            sx = {{
                                fontSize: 50,
                                color: "white",
                                cursor: "pointer",
                            }}
                        />

                    </Grid>


                </Grid>

                <Grid
                    sx = {{
                        width: "100%",
                        height: "98vh",
                    }}
                >

                    <Grid
                        container
                        alignItems = "center"
                        justifyContent = "center"
                        sx = {{
                            pt: 8,
                            mt: 4,
                        }}
                    >

                        <Typography
                            onClick = { () => hideHamburguerButton() }
                            component = { 'span' } 
                            variant={ 'body2' }
                            sx = {{ 
                                fontWeight: "bold", 
                                fontSize: 15,
                                backgroundColor: "#4786a4",
                                borderRadius: 4,
                                width: "170px",
                                pt: 2,
                                pb: 2,
                                textAlign: "center",
                                borderBottom: 3.5,
                                borderColor: "white",
                            }}
                        >
                            
                            <Link 
                                to = '/'
                                style = {{ 
                                    textDecoration: "none",
                                    color: "white",
                                }}
                            > 

                                PERSONAS

                            </Link>

                        </Typography>

                    </Grid>

                    <Grid
                        container
                        alignItems = "center"
                        justifyContent = "center"
                        sx = {{
                            pt: status !== "authenticated" ? 0 : 5,
                        }}
                    >

                        <Typography
                            onClick = { () => hideHamburguerButton() }
                            component = { 'span' } 
                            variant={ 'body2' }
                            sx = {{ 
                                display: status === "authenticated" ? "" : "none",
                                fontWeight: "bold", 
                                fontSize: 15,
                                backgroundColor: "#4786a4",
                                borderRadius: 4,
                                width: "170px",
                                pt: 2,
                                pb: 2,
                                textAlign: "center",
                                borderBottom: 3.5,
                                borderColor: "white",
                            }}
                        >
                            
                            <Link 
                                to = '/groups/mygroup'
                                style = {{ 
                                    textDecoration: "none",
                                    color: "white",
                                }}
                            > 

                                MIS GRUPOS

                            </Link>

                        </Typography>

                    </Grid>

                    <Grid
                        container
                        alignItems = "center"
                        justifyContent = "center"
                        sx = {{
                            pt: status !== "authenticated" ? 0 : 5,
                        }}
                    >

                        <Typography
                            onClick = { () => hideHamburguerButton() }
                            component = { 'span' } 
                            variant = { 'body2' }
                            sx = {{ 
                                display: status === "authenticated" ? "" : "none",
                                fontWeight: "bold", 
                                fontSize: 15,
                                backgroundColor: "#4786a4",
                                borderRadius: 4,
                                width: "170px",
                                pt: 2,
                                pb: 2,
                                textAlign: "center",
                                borderBottom: 3.5,
                                borderColor: "white",
                            }}
                        >
                            
                            <Link 
                                to = '/gifts/mygifts'
                                style = {{ 
                                    textDecoration: "none",
                                    color: "white",
                                }}
                            > 

                                MIS REGALOS

                            </Link>

                        </Typography>   

                    </Grid>

                    <Grid
                        container
                        alignItems = "center"
                        justifyContent = "center"
                        sx = {{
                            pt: status !== "authenticated" ? 0 : 5,
                        }}
                    >

                        <Typography
                            onClick = { () => hideHamburguerButton() }
                            component = { 'span' } 
                            variant = { 'body2' }
                            sx = {{ 
                                display: status === "authenticated" ? "" : "none",
                                fontWeight: "bold", 
                                fontSize: 15,
                                backgroundColor: "#4786a4",
                                borderRadius: 4,
                                width: "170px",
                                pt: 2,
                                pb: 2,
                                textAlign: "center",
                                borderBottom: 3.5,
                                borderColor: "white",
                            }}
                        >
                            
                            <Link 
                                to = '/groups/reservegift'
                                style = {{ 
                                    textDecoration: "none",
                                    color: "white",
                                }}
                            > 

                                APARTADOS

                            </Link>

                        </Typography>  

                    </Grid>

                    <Grid
                        container
                        alignItems = "center"
                        justifyContent = "center"
                        sx = {{
                            pt: status !== "authenticated" ? 0 : 5,
                        }}
                    >

                        <Typography
                            onClick = { () => hideHamburguerButton() }
                            component = { 'span' } 
                            variant = { 'body2' }
                            sx = {{ 
                                display: status === "authenticated" ? "" : "none",
                                fontWeight: "bold", 
                                fontSize: 15,
                                backgroundColor: "#4786a4",
                                borderRadius: 4,
                                width: "170px",
                                pt: 2,
                                pb: 2,
                                textAlign: "center",
                                borderBottom: 3.5,
                                borderColor: "white",
                            }}
                        >
                            
                            <Link 
                                to = '/profile'
                                style = {{ 
                                    textDecoration: "none",
                                    color: "white",
                                }}
                            > 

                                PERFIL

                            </Link>

                        </Typography>  

                    </Grid>

                    <Grid
                        container
                        alignItems = "center"
                        justifyContent = "center"
                        sx = {{
                            pt: status !== "authenticated" ? 5 : 5,
                        }}
                    >

                        <Typography
                            onClick = { () => hideHamburguerButton() }
                            component = { 'span' } 
                            variant = { 'body2' }
                            sx = {{ 
                                display: status !== "authenticated" ? "" : "none",
                                fontWeight: "bold", 
                                fontSize: 15,
                                backgroundColor: "#4786a4",
                                borderRadius: 4,
                                width: "170px",
                                pt: 2,
                                pb: 2,
                                textAlign: "center",
                                borderBottom: 3.5,
                                borderColor: "white",
                            }}
                        >
                            
                            <Link 
                                to = '/auth/login'
                                style = {{ 
                                    textDecoration: "none",
                                    color: "white",
                                }}
                            > 

                                LOGIN

                            </Link>

                        </Typography>  

                    </Grid>

                    <Grid
                        container
                        alignItems = "center"
                        justifyContent = "center"
                        sx = {{
                            pt: status !== "authenticated" ? 0 : 0,
                            pb: 5,
                        }}
                    >

                        <Typography
                            onClick = { () => handleLogout }
                            component = { 'span' } 
                            variant = { 'body2' }
                            sx = {{ 
                                display: status === "authenticated" ? "" : "none",
                                fontWeight: "bold", 
                                fontSize: 15,
                                backgroundColor: "#4786a4",
                                borderRadius: 4,
                                cursor: "pointer",
                                color: "white",
                                width: "170px",
                                pt: 2,
                                pb: 2,
                                textAlign: "center",
                                borderBottom: 3.5,
                                borderColor: "white",
                            }}
                        >
                            
                            <Box
                                sx={{
                                    cursor: "pointer",
                                    color: "white"
                                }}
                                onClick={handleLogout}
                            >

                                CERRAR SESIÓN

                            </Box>


                        </Typography>  

                    </Grid>

                </Grid>

            </Grid>

        </>


    );
}
