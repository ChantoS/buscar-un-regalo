import { Button, Grid, TextField, Typography } from "@mui/material";
import { useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useForm } from "../../hooks/useForm";
import { useGetRecoverCodeCheckQuery, usePostChangePassordMutation } from "../../store/api/giftsApi";

export const ResetPasswordPage = () => {

  const navigate = useNavigate();
  const { password, confirmPassword, onInputChange } = useForm({ password: '', confirmPassword: '' });

  const { resetCode = 0 } = useParams();
  const { data: checkCodeData, error: checkCodeError } = useGetRecoverCodeCheckQuery(resetCode);
  const [ changeNewPassword ] = usePostChangePassordMutation();

  const submitNewPassword = async (e) => {

    e.preventDefault();

    if(password == confirmPassword) {

      const { data, error } = await changeNewPassword({resetCode, newPassword: password });

      if(data && data.code == 200) {
        navigate('/auth/login', {
          replace: true
        });
      }

    }

  };

  useEffect(() => {
      if(checkCodeData && checkCodeData.code != 200) {
        console.log(checkCodeData);
        navigate('/auth/login', {
          replace: true
        });
      }

      console.log(checkCodeData)
  }, [checkCodeData]);

  if(checkCodeData && checkCodeData.code == 200)

  return (

    <form onSubmit={submitNewPassword}>
    
      <Grid
        display = {{ xs: "none", sm: "none", md: "flex" }}
        container
        alignItems = "center"
        direction = "column"
        sx = {{
          width: "100vw",
          height: "70vh",
          mt: 15,
        }}
      >

        <Grid
          sx = {{
            mt: 3,
            px: 3,
          }}
        >

          <Typography
            sx = {{
              color: "white",
              fontSize: 35,
              textAlign: "center"
            }}
          >

            Introduce tu nueva contraseña con un mínimo de 6 caracteres

          </Typography>

        </Grid>

        <Grid
          container
          justifyContent = "center"
          sx = {{
            width: "100%",
            mt: 2,
          }}
        >

          <Grid 
            className = "roundedRectangle"
            sx = {{
              backgroundColor: "#5494b3",
              width: "80%",
              height: "5px",
              borderTopLeftRadius: 100,
              borderTopRightRadius: 100,
            }}
          /> 

        </Grid>

        <Grid
          sx = {{
            width: { md: "70%", lg: "68%", xl: "53.5%" },
            mt: 5,
          }}
        >

          <Typography
            sx = {{
              color: "white",
              fontSize: 25,
            }}
          >

            Nueva contraseña

          </Typography>

        </Grid>

        <Grid
          sx = {{
            width: { md: "70%", lg: "68%", xl: "53.5%" },
            mt: 3,
          }}
        >

          <Grid 
            className = "roundedRectangle"
            sx = {{
              backgroundColor: "#5494b3",
              width: "30%",
              height: "5px",
              borderTopLeftRadius: 100,
              borderTopRightRadius: 100,
            }}
          /> 

        </Grid>

        <Grid
          sx = {{
            width: { md: "70%", lg: "68%", xl: "53.5%" },
            mt: 3,
          }}
        >

          <Grid
            sx = {{
              width: "50%"
            }}
          >

            <TextField
              className = "inputs"
              placeholder = "Aa..."
              variant = "standard"
              name="password"
              value={password}
              type="password"
              onChange={onInputChange}
              InputProps = {{
                disableUnderline: true,
                sx: {
                  color: "white",
                  fontWeight: "bold",
                  fontSize: 15,
                  pt: 1,
                  pb: 1,
                  pr: 3,
                  pl: 3.5,
                },
              }}
              style = {{
                underline: "none",
                borderRadius: 10,
                backgroundColor: "#6b99b0",
              }}
            />

          </Grid>

        </Grid>

        <Grid
          sx = {{
            width: { md: "70%", lg: "68%", xl: "53.5%" },
            mt: 5
          }}
        >

          <Typography
            sx = {{
              color: "white",
              fontSize: 25,
            }}
          >

            Confirmar contraseña

          </Typography>

        </Grid>

        <Grid
          sx = {{
            width: { md: "70%", lg: "68%", xl: "53.5%" },
            mt: 3,
          }}
        >

          <Grid 
            className = "roundedRectangle"
            sx = {{
              backgroundColor: "#5494b3",
              width: "30%",
              height: "5px",
              borderTopLeftRadius: 100,
              borderTopRightRadius: 100,
            }}
          /> 

        </Grid>

        <Grid
          sx = {{
            width: { md: "70%", lg: "68%", xl: "53.5%" },
            mt: 3,
          }}
        >

          <Grid
            sx = {{
              width: "50%"
            }}
          >

            <TextField
              className = "inputs"
              placeholder = "Aa..."
              variant = "standard"
              name="confirmPassword"
              value={confirmPassword}
              type="password"
              onChange={onInputChange}
              InputProps = {{
                disableUnderline: true,
                sx: {
                  color: "white",
                  fontWeight: "bold",
                  fontSize: 15,
                  pt: 1,
                  pb: 1,
                  pr: 3,
                  pl: 3.5,
                },
              }}
              style = {{
                underline: "none",
                borderRadius: 10,
                backgroundColor: "#6b99b0",
              }}
            />

          </Grid>

        </Grid>

        <Grid
          sx = {{
            width: { md: "70%", lg: "68%", xl: "53.5%" },
            mt: 8,
          }}
        >

          <Button
            type="submit"
            sx = {{
              backgroundColor: "#6b96ad",
              pt: 2,
              pb: 2,
              pl: 4,
              pr: 4,
              ":hover": { backgroundColor: "#0f69a2" }
            }}
          >

            <Typography
              component = {'span'} 
              variant = {'body2'}
              sx = {{ 
                  color: "#004f77",
                  fontWeight: "bold", 
                  fontSize: 18,
              }}
            >

              Cambiar contraseña

            </Typography>

          </Button>

        </Grid>

      </Grid>

      {/* Portrait mobile UI */}

      <Grid
        container
        alignItems = "center"
        direction = "column"
        display = {{ xs: "flex", sm: "none" }}
        sx = {{
          width: "100%",
          height: "80vh",
          mt: 25,
          mb: 10,
        }}
      >

        <Grid
          sx = {{
            mt: 3,
            px: 2.5,
          }}
        >

          <Typography
            sx = {{
              color: "white",
              fontSize: 25,
              textAlign: "center"
            }}
          >

            Introduce tu nueva contraseña con un mínimo de 6 caracteres

          </Typography>

        </Grid>

        <Grid
          container
          justifyContent = "center"
          sx = {{
            width: "80%",
            mt: 2,
          }}
        >

          <Grid 
            className = "roundedRectangle"
            sx = {{
              backgroundColor: "#5494b3",
              width: "100%",
              height: "5px",
              borderTopLeftRadius: 100,
              borderTopRightRadius: 100,
            }}
          /> 

        </Grid>

        <Grid
          sx = {{
            width: "75%",
            mt: 5,
          }}
        >

          <Typography
            sx = {{
              color: "white",
              fontSize: 20,
            }}
          >

            Nueva contraseña

          </Typography>

        </Grid>

        <Grid
          sx = {{
            width: "80%",
            mt: 2,
          }}
        >

          <Grid 
            className = "roundedRectangle"
            sx = {{
              backgroundColor: "#5494b3",
              width: "80%",
              height: "5px",
              borderTopLeftRadius: 100,
              borderTopRightRadius: 100,
            }}
          /> 

        </Grid>

        <Grid
          sx = {{
            width: "80%",
            mt: 3,
          }}
        >

          <Grid
            sx = {{
              width: "100%"
            }}
          >

            <TextField
              className = "inputs"
              placeholder = "Aa..."
              variant = "standard"
              InputProps = {{
                disableUnderline: true,
                sx: {
                  color: "white",
                  fontWeight: "bold",
                  fontSize: 15,
                  pt: 1,
                  pb: 1,
                  pr: 3,
                  pl: 3.5,
                },
              }}
              style = {{
                underline: "none",
                borderRadius: 10,
                backgroundColor: "#6b99b0",
              }}
            />

          </Grid>

        </Grid>

        <Grid
          sx = {{
            width: "75%",
            mt: 5
          }}
        >

          <Typography
            sx = {{
              color: "white",
              fontSize: 20,
            }}
          >

            Confirmar contraseña

          </Typography>

        </Grid>

        <Grid
          sx = {{
            width: "80%",
            mt: 2,
          }}
        >

          <Grid 
            className = "roundedRectangle"
            sx = {{
              backgroundColor: "#5494b3",
              width: "80%",
              height: "5px",
              borderTopLeftRadius: 100,
              borderTopRightRadius: 100,
            }}
          /> 

        </Grid>

        <Grid
          sx = {{
            width: "80%",
            mt: 3,
          }}
        >

          <Grid
            sx = {{
              width: "100%"
            }}
          >

            <TextField
              className = "inputs"
              placeholder = "Aa..."
              variant = "standard"
              InputProps = {{
                disableUnderline: true,
                sx: {
                  color: "white",
                  fontWeight: "bold",
                  fontSize: 15,
                  pt: 1,
                  pb: 1,
                  pr: 3,
                  pl: 3.5,
                },
              }}
              style = {{
                underline: "none",
                borderRadius: 10,
                backgroundColor: "#6b99b0",
              }}
            />

          </Grid>

        </Grid>

        <Grid
          sx = {{
            width: "80%",
            mt: 8,
          }}
        >

          <Button
            sx = {{
              backgroundColor: "#6b96ad",
              ":hover": { backgroundColor: "#0f69a2" },
              width: "100%",
              py: 3,
            }}
          >

            <Typography
              component = {'span'} 
              variant = {'body2'}
              sx = {{ 
                  color: "#004f77",
                  fontWeight: "bold", 
                  fontSize: 18,
              }}
            >

              Cambiar contraseña

            </Typography>

          </Button>

        </Grid>

      </Grid>

      {/* Landscape mobile UI */}

      <Grid
        display = {{ xs: "none", sm: "flex", md: "none" }}
        container
        alignItems = "center"
        direction = "column"
        sx = {{
          width: "100%",
          height: "580px",
          mt: 25,
          mb: 10,
        }}
      >

        <Grid
          sx = {{
            mt: 3,
            px: 4,
          }}
        >

          <Typography
            sx = {{
              color: "white",
              fontSize: 25,
              textAlign: "center"
            }}
          >

            Introduce tu nueva contraseña con un mínimo de 6 caracteres

          </Typography>

        </Grid>

        <Grid
          container
          justifyContent = "center"
          sx = {{
            width: "80%",
            mt: 2,
          }}
        >

          <Grid 
            className = "roundedRectangle"
            sx = {{
              backgroundColor: "#5494b3",
              width: "100%",
              height: "5px",
              borderTopLeftRadius: 100,
              borderTopRightRadius: 100,
            }}
          /> 

        </Grid>

        <Grid
          sx = {{
            width: "75%",
            mt: 3,
          }}
        >

          <Typography
            sx = {{
              color: "white",
              fontSize: 18,
            }}
          >

            Nueva contraseña

          </Typography>

        </Grid>

        <Grid
          sx = {{
            width: "80%",
            mt: 2,
          }}
        >

          <Grid 
            className = "roundedRectangle"
            sx = {{
              backgroundColor: "#5494b3",
              width: "45%",
              height: "5px",
              borderTopLeftRadius: 100,
              borderTopRightRadius: 100,
            }}
          /> 

        </Grid>

        <Grid
          sx = {{
            width: "80%",
            mt: 3,
          }}
        >

          <Grid
            sx = {{
              width: "100%"
            }}
          >

            <TextField
              className = "inputs"
              placeholder = "Aa..."
              variant = "standard"
              InputProps = {{
                disableUnderline: true,
                sx: {
                  color: "white",
                  fontWeight: "bold",
                  fontSize: 15,
                  pt: 1,
                  pb: 1,
                  pr: 3,
                  pl: 3.5,
                },
              }}
              style = {{
                underline: "none",
                borderRadius: 10,
                backgroundColor: "#6b99b0",
              }}
            />

          </Grid>

        </Grid>

        <Grid
          sx = {{
            width: "75%",
            mt: 5
          }}
        >

          <Typography
            sx = {{
              color: "white",
              fontSize: 20,
            }}
          >

            Confirmar contraseña

          </Typography>

        </Grid>

        <Grid
          sx = {{
            width: "80%",
            mt: 2,
          }}
        >

          <Grid 
            className = "roundedRectangle"
            sx = {{
              backgroundColor: "#5494b3",
              width: "45%",
              height: "5px",
              borderTopLeftRadius: 100,
              borderTopRightRadius: 100,
            }}
          /> 

        </Grid>

        <Grid
          sx = {{
            width: "80%",
            mt: 3,
          }}
        >

          <Grid
            sx = {{
              width: "100%"
            }}
          >

            <TextField
              className = "inputs"
              placeholder = "Aa..."
              variant = "standard"
              InputProps = {{
                disableUnderline: true,
                sx: {
                  color: "white",
                  fontWeight: "bold",
                  fontSize: 15,
                  pt: 1,
                  pb: 1,
                  pr: 3,
                  pl: 3.5,
                },
              }}
              style = {{
                underline: "none",
                borderRadius: 10,
                backgroundColor: "#6b99b0",
              }}
            />

          </Grid>

        </Grid>

        <Grid
          sx = {{
            width: "80%",
            mt: 8,
          }}
        >

          <Button
            sx = {{
              backgroundColor: "#6b96ad",
              ":hover": { backgroundColor: "#0f69a2" },
              width: "100%",
              py: 3,
            }}
          >

            <Typography
              component = {'span'} 
              variant = {'body2'}
              sx = {{ 
                  color: "#004f77",
                  fontWeight: "bold", 
                  fontSize: 18,
              }}
            >

              Cambiar contraseña

            </Typography>

          </Button>

        </Grid>

      </Grid>

    </form>

  );
  
}
