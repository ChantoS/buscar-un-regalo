import { Avatar, Button, Grid, MenuItem, Select, TextField, Typography } from "@mui/material";
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import { usePutEditGiftMutation } from "../../store/api/giftsApi";
import CardGiftcardIcon from '@mui/icons-material/CardGiftcard';
import ArticleIcon from '@mui/icons-material/Article';
import Groups2Icon from '@mui/icons-material/Groups2';
import { useForm } from "../../hooks/useForm";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useRef, useState } from "react";
import { onUpdateGifts } from "../../store/gifts/giftsSlice";
import { useUserData } from "../../hooks/useUserData";
import { apiURL } from "../../helpers";
import { useNavigate } from "react-router-dom";
import { useCustomAlerts } from "../../hooks/useCustomAlerts";
import { UserGiftEditAlert } from "../components/user-gift/user-gift-edit-alert";

export const EditGift = () => {

  const { id, name: giftName, description: giftDescription, img, communityID: giftCommunityId } = useSelector( state => state.gifts );
  
  const { communities } = useUserData();
  const [charactersCount, setCharactersCount] = useState(0);
  const [editingGift, setEditingGift] = useState(false);

  const [avatarSRC, setAvatarSRC] = useState(img.length > 1 ? `${apiURL}uploads/gifts/${img}` : "/imgs/BUR-gift-logo.png");

  const navigate = useNavigate();
  const dispatch = useDispatch();


  const { 
    name, 
    description, 
    picture, 
    communityID, 
    onInputChange, 
    onFileChange,
  } = useForm( { name: giftName, description: giftDescription, communityID: giftCommunityId} );

  const [ edit ] = usePutEditGiftMutation();

  const {
    openAlertDialog,
    newAlert,
    closeDialogAlert,
  } = useCustomAlerts()

  const fileInputRef = useRef();

  const editGift = async() => {

    if(editingGift) return;

    setEditingGift(true);

    const formData = new FormData();
    formData.append('giftID', id);
    formData.append('name', name);
    formData.append('description', description);
    formData.append('picture', picture);
    formData.append('communityID', communityID);

    try {

      const { data } = await edit( formData );
    
      setEditingGift(false);

      if(data.code == 200) {
        dispatch( onUpdateGifts({ name, description, img: data.img.length > 1 ? data.img : img, communityID }) );
        navigate('/gifts/view', {
          replace: true
        });
      }

    } catch (error) {
      setEditingGift(false);
      console.log(error);
    }
  }

  const sendEdit = (e) => {
    e.preventDefault();

    openAlertDialog("¿Seguro que quieres editar este regalo?", <UserGiftEditAlert {...{closeDialogAlert, editGift }} />);
    // if(changedInput) {
    // }
    // else {
    //   navigate('/gifts/view');
    // }

  }

  const changeInput = () => {
    // setChangedInput(true);
  }

  useEffect( () => {
    setCharactersCount(description.length);
  }, [description]);
  
  useEffect( () => {
    if(picture) {
      changeInput();
      const url = URL.createObjectURL(picture);
      setAvatarSRC(url);
    }
  }, [picture]);

  return (

    <form
      onSubmit={ sendEdit }
    >

      <Grid
        display = {{ xs: "none", sm: "none", md: "inline-grid" }}
        sx = {{
          mt: 20
        }}
      >

        <Grid 
          className = "roundedRectangle"
          sx = {{
            backgroundColor: "#5494b3",
            width: "100%",
            height: "20px",
            borderTopLeftRadius: 100,
            borderTopRightRadius: 100,
          }}
        />

        <Grid
          display = "flex"
          sx = {{
            width: { md: "90vw", lg: "70vw", xl: "60vw" },
            height: "600px",
            mt: 1.5,
            mb: 1.5,
          }}
        >

          <Grid
            container
            alignItems = "center"
            justifyContent = "center"
            direction = "column"
            sx = {{
              width: "50%",
              height: "600px",
            }}
          >

            <Avatar
              src = { avatarSRC }
              sx = {{
                height: "350px",
                width: "350px",
                borderRadius: "200px",
              }}
            />

              <input 
                type = "file"
                multiple
                ref = { fileInputRef }
                name = "picture"
                onChange = { onFileChange }
                style = {{ display: 'none' }}
              />

              <Grid
                container
                justifyContent = "end"
                sx = {{
                  width: "300px",
                  height: "10px",
                  position: "relative",
                  top: "-90px",
                  left: "-10px"
                }}
              >

              <Grid
                container
                alignItems = "center"
                justifyContent = "center"
                sx = {{
                  backgroundColor: "orange",
                  height: "100px",
                  width: "100px",
                  borderRadius: "70px"
                }}
              >

                <AddPhotoAlternateIcon 
                  onClick = { () => fileInputRef.current.click() }
                  sx = {{
                    fontSize: "55px",
                    color: "white"
                  }}
                />

              </Grid>

            </Grid>

          </Grid>

          <Grid
            sx = {{
              width: "50%",
              height: "600px",
              pt: 3.5,
            }}
          >

            <Grid
              display = "flex"
              sx = {{
                ml: 4,
              }}
            >

              <Grid
                sx = {{
                  mr: 1.2,
                  mt: 0.2,
                  mb: 1,
                }}
              >

                <CardGiftcardIcon 
                  sx = {{
                    color: "white",
                    fontSize: 30,
                    mr: 1
                  }}
                />

              </Grid>

              <Grid
                sx = {{
                  mb: 2,
                }}
              >


                <Typography
                  component = {'span'} 
                  variant = {'body2'}
                  sx = {{ 
                      color: "white",
                      fontWeight: "bold", 
                      fontSize: 20,
                      mr: 5,
                  }}
                >

                  Nombre del regalo
                  
                </Typography>

              </Grid>

            </Grid>

            <Grid
              container
              justifyContent = "start"
              sx = {{
                width: "100%",
              }}
            >

              <Grid
                container
                sx = {{
                  width: { xs: "75%", sm: "85%", md: "60%" },
                  ml: 3
                }}
              >

                <TextField 
                  className = "inputs"
                  type = "text"
                  name = "name"
                  placeholder = { giftName }
                  value = { name }
                  onChange = { (e) => { onInputChange(e); changeInput();} }
                  variant = "standard"
                  InputProps = {{
                    disableUnderline: true,
                    sx: {
                      color: "white",
                      fontWeight: "bold",
                      fontSize: 15,
                      pt: 1.5,
                      pb: 1.5,
                      pr: 3,
                      pl: 3.5,
                    },
                  }}
                  style = {{
                    underline: "none",
                    borderRadius: 20,
                    backgroundColor: "#6b99b0",
                  }}
                />

              </Grid>

            </Grid>

            <Grid
              display = "flex"
              sx = {{
                ml: 4,
                mb: 1,
                mt: 3,
              }}
            >

              <Grid
                sx = {{
                  mr: 1.5,
                }}
              >

                <ArticleIcon 
                  sx = {{
                    color: "white",
                    fontSize: 35,
                  }}
                />

              </Grid>

              <Grid>

                <Typography
                  component = {'span'} 
                  variant = {'body2'}
                  sx = {{ 
                      color: "white",
                      fontWeight: "bold", 
                      fontSize: 20,
                      mr: 5,
                  }}
                >
                  
                  Descripción
                  
                </Typography>

              </Grid>

            </Grid>

            <Grid
              container
              justifyContent = "start"
              sx = {{
                pt: 1,
              }}
            >

              <Grid
                container
                sx = {{
                  width: { xs: "75%", sm: "85%", md: "75%" },
                  ml: 3,
                }}
              >

                <textarea 
                  className = "textAreaDesc"
                  type = "text"
                  name="description"
                  placeholder = {giftDescription}
                  value= {description}
                  onChange = { (e) => { onInputChange(e); changeInput();} }
                  maxLength={150}
                />

              </Grid>
              
              <Grid 
                sx = {{
                  width: "85%",
                  height: "15px",
                  position: "relative",
                  top: "-30px",
                  right: "-200px"
                }}
              >

                <Typography
                  component = {'span'} 
                  variant = {'body2'}
                  textAlign = "end"
                  sx = {{ 
                      color: charactersCount >= 150 ? "red" : charactersCount >= 100 ? "orange" : "white",
                      fontWeight: "bold", 
                      fontSize: 15,
                      mr: 5,
                  }}
                >

                  {charactersCount} / 150 caracteres
                  
                </Typography>

              </Grid>
                    
            </Grid>

            <Grid
              display = "flex"
              sx = {{
                ml: 2,
                mb: 2,
              }}
            >

              <Grid>

                <Groups2Icon 
                  sx = {{
                    color: "white",
                    fontSize: 35,
                    mr: 2,
                    ml: 2
                  }}
                />

              </Grid>

              <Grid
                sx = {{
                  width: "550px",
                }}
              >

                <Typography
                  component = {'span'} 
                  variant = {'body2'}
                  sx = {{ 
                      color: "white",
                      fontWeight: "bold", 
                      fontSize: 20,
                      mr: 5,
                  }}
                >
                  
                  En que grupo crearás este regalo?
                  
                </Typography>

              </Grid>

            </Grid>

            <Grid
              sx = {{
                ml: 3.8,
                mb: 2,
              }}
            >

              <Select 
                defaultValue={giftCommunityId}
                name='communityID'
                value={communityID}
                onChange = { (e) => { onInputChange(e); changeInput();} }
                sx = {{
                  width: "60%",
                  fontWeight: "bold",
                  backgroundColor: "#6b99b0",
                  borderRadius: 5,
                }}
              >

                <MenuItem value = { 0 }>

                  <Typography
                    component = {'span'} 
                    variant = {'body2'}
                    sx = {{ 
                        color: "#003e65",
                        fontWeight: "bold", 
                        fontSize: 25,
                        mr: 5,
                    }}
                  >
                    
                    Público
                    
                  </Typography>

                </MenuItem>

                {communities.map( row => (
                  <MenuItem key={row.id} value = { row.id }>

                    <Typography
                      component = {'span'} 
                      variant = {'body2'}
                      sx = {{ 
                          color: "#003e65",
                          fontWeight: "bold", 
                          fontSize: 25,
                          mr: 5,
                      }}
                    >
                      
                      {row.name}
                      
                    </Typography>

                  </MenuItem>
                ))}
                
              </Select>

            </Grid>

            <Grid
              sx = {{
                pt: 2,
                ml: 4
              }}
            >

              <Button
                type = "submit" 
                variant = "contained" 
                value = "Register"
                sx = {{
                  backgroundColor: "#6b96ad",
                  pt: 1,
                  pb: 1,
                  pl: 3,
                  pr: 3,
                  ":hover": { backgroundColor: "#0f69a2" }
                }}
              >
                
                <Typography
                  component = {'span'} 
                  variant = {'body2'}
                  sx = {{ 
                      color: "#004f77",
                      fontWeight: "bold", 
                      fontSize: 25,
                  }}
                >

                  Editar Regalo

                </Typography>

              </Button>

            </Grid>

          </Grid>

        </Grid>

        <Grid 
          className = "roundedRectangle"
          sx = {{
            backgroundColor: "#5494b3",
            width: "100%",
            height: "20px",
            borderBottomLeftRadius: 100,
            borderBottomRightRadius: 100,
          }}
        />

      </Grid>

      {/* Portrait mobile UI */}

      <Grid
        display = {{ xs: "inline-grid", sm: "none", md: "none" }}
        sx = {{
          width: "100vw",
          height: "130vh",
          mt: 28,
          mb: 10,
        }}
      >

        <Grid>

          <Grid
            container
            justifyContent = "center"
          >

            <Grid 
              className = "roundedRectangle"
              sx = {{
                backgroundColor: "#5494b3",
                width: "80%",
                height: "10px",
                borderTopLeftRadius: 100,
                borderTopRightRadius: 100,
              }}
            />

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              mt: 7,
            }}
          >

            <input 
              type = "file"
              multiple
              ref = { fileInputRef }
              name = "picture"
              onChange = { onFileChange }
              style = {{ display: 'none' }}
            />

            <Avatar 
              src = { avatarSRC }
              sx = {{
                height: "200px",
                width: "200px",
              }}
            />

            <Grid
              container
              justifyContent = "end"
              sx = {{
                width: "300px",
                height: "10px",
                position: "relative",
                top: "-60px",
                left: "-40px"
              }}
            >

              <Grid
                container
                alignItems = "center"
                justifyContent = "center"
                sx = {{
                  backgroundColor: "orange",
                  height: "80px",
                  width: "80px",
                  borderRadius: "70px"
                }}
              >

                <AddPhotoAlternateIcon 
                  onClick = { () => fileInputRef.current.click() }
                  sx = {{
                    fontSize: "55px",
                    color: "white"
                  }}
                />

              </Grid>

            </Grid>

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              mt: 5,
            }}
          >

            <Typography
              sx = {{
                color: "white",
                fontSize: 25,
              }}
            >

              Nombre del regalo

            </Typography>
            
          </Grid>


          <Grid
            container
            justifyContent = "center"
            sx = {{
              width: "100%",
              mt: 3,
            }}
          >

            <Grid
              container
              sx = {{
                width: { xs: "75%", sm: "85%", md: "60%" },
              }}
            >

              <TextField 
                className = "inputs"
                type = "text"
                name = "name"
                placeholder = { giftName }
                value = { name }
                onChange = { onInputChange }
                variant = "standard"
                InputProps = {{
                  disableUnderline: true,
                  sx: {
                    color: "white",
                    fontWeight: "bold",
                    fontSize: 15,
                    pt: 1.5,
                    pb: 1.5,
                    pr: 3,
                    pl: 3.5,
                  },
                }}
                style = {{
                  underline: "none",
                  borderRadius: 20,
                  backgroundColor: "#6b99b0",
                }}
              />

            </Grid>

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              mt: 5,
            }}
          >

            <Typography
              sx = {{
                color: "white",
                fontSize: 25,
              }}
            >

              Descripción

            </Typography>
            
          </Grid>


          <Grid
            container
            justifyContent = "center"
            sx = {{
              pt: 1,
            }}
          >

            <Grid
              container
              sx = {{
                width: { xs: "75%", sm: "85%", md: "75%" },
              }}
            >

              <textarea 
                className = "textAreaDesc"
                type = "text"
                name="description"
                placeholder = {giftDescription}
                value= {description}
                onChange = { onInputChange }
                maxLength={150}
              />

            </Grid>
              
            <Grid 
              container
              justifyContent = "end"
              sx = {{
                width: "90%",
                height: "15px",
                position: "relative",
              }}
            >

              <Typography
                component = {'span'} 
                variant = {'body2'}
                textAlign = "end"
                sx = {{ 
                    color: charactersCount >= 150 ? "red" : charactersCount >= 100 ? "orange" : "white",
                    fontWeight: "bold", 
                    fontSize: 15,
                    mr: 5,
                }}
              >

                {charactersCount} / 150 caracteres
              
              </Typography>

            </Grid>
                    
          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              mt: 5,
            }}
          >

            <Typography
              sx = {{
                color: "white",
                fontSize: 25,
                textAlign: "center"
              }}
            >

              ¿En que grupo crearás este regalo?

            </Typography>
            
          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              mt: 3
            }}
          >

            <Select 
              defaultValue={giftCommunityId}
              name='communityID'
              value={communityID}
              onChange={onInputChange}
              sx = {{
                width: "80%",
                fontWeight: "bold",
                backgroundColor: "#6b99b0",
                borderRadius: 5,
              }}
            >

              <MenuItem value = { 0 }>

                <Typography
                  component = {'span'} 
                  variant = {'body2'}
                  sx = {{ 
                      color: "#003e65",
                      fontWeight: "bold", 
                      fontSize: 25,
                      mr: 5,
                  }}
                >
                  
                  Público
                  
                </Typography>

              </MenuItem>

              {communities.map( row => (
                <MenuItem key={row.id} value = { row.id }>

                  <Typography
                    component = {'span'} 
                    variant = {'body2'}
                    sx = {{ 
                        color: "#003e65",
                        fontWeight: "bold", 
                        fontSize: 25,
                        mr: 5,
                    }}
                  >
                    
                    {row.name}
                    
                  </Typography>

                </MenuItem>
              ))}
              
            </Select>

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              mt: 5
            }}
          >

            <Button
              type = "submit" 
              variant = "contained" 
              value = "Register"
              sx = {{
                backgroundColor: "#6b96ad",
                pt: 2,
                pb: 2,
                pl: 3,
                pr: 3,
                ":hover": { backgroundColor: "#0f69a2" }
              }}
            >

              <Typography
                component = {'span'} 
                variant = {'body2'}
                sx = {{ 
                    color: "#004f77",
                    fontWeight: "bold", 
                    fontSize: 25,
                }}
              >

                Editar Regalo

              </Typography>

            </Button>

          </Grid>

          <Grid 
            container
            justifyContent = "center"
            sx = {{
              mt: 5,
            }}
          >

            <Grid 
              className = "roundedRectangle"
              sx = {{
                backgroundColor: "#5494b3",
                width: "80%",
                height: "10px",
                borderBottomLeftRadius: 100,
                borderBottomRightRadius: 100,
              }}
            />

          </Grid>

        </Grid>

      </Grid>

      {/* Landscape mobile UI */}

      <Grid
        display = {{ xs: "none", sm: "inline-grid", md: "none" }}
        sx = {{
          width: "90vw",
          mt: 27,
          mb: 15,
        }}
      >

        <Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              mb: 3,
            }}
          >

            <Grid 
              className = "roundedRectangle"
              sx = {{
                backgroundColor: "#5494b3",
                width: "100%",
                height: "10px",
                borderTopLeftRadius: 100,
                borderTopRightRadius: 100,
              }}
            />  

          </Grid>

          <Grid
            container
            sx = {{
              height: "80%",
              width: "100%"
            }}
          >

            <Grid
              container
              justifyContent = "center"
              alignItems = "center"
              sx = {{
                width: "50%"
              }}
            >

              <input 
                type = "file"
                multiple
                ref = { fileInputRef }
                name = "picture"
                onChange = { onFileChange }
                style = {{ display: 'none' }}
              />

              <Avatar 
                src = { avatarSRC }
                sx = {{
                  height: "250px",
                  width: "250px",
                }}
              />

              <Grid
                container
                justifyContent = "end"
                sx = {{
                  width: "300px",
                  height: "10px",
                  position: "absolute",
                  bottom: "-230px",
                  left: "20px"
                }}
              >

                <Grid
                  container
                  alignItems = "center"
                  justifyContent = "center"
                  sx = {{
                    backgroundColor: "orange",
                    height: "80px",
                    width: "80px",
                    borderRadius: "70px",
                  }}
                >

                  <AddPhotoAlternateIcon 
                    onClick = { () => fileInputRef.current.click() }
                    sx = {{
                      fontSize: "55px",
                      color: "white",
                    }}
                  />

                </Grid>

              </Grid>

            </Grid>

            <Grid
              sx = {{
                width: "50%",
              }}
            >

              <Grid
                sx = {{
                  ml: 1.5,
                }}
              >

                <Typography
                  sx = {{
                    color: "white",
                    fontSize: 18,
                  }}
                >

                  Nombre del regalo

                </Typography>

              </Grid>
              
              <Grid
                container
                justifyContent = "start"
                sx = {{
                  width: "100%",
                  mt: 2,
                }}
              >

                <Grid
                  container
                  sx = {{
                    width: { xs: "75%", sm: "75%", md: "60%" },
                  }}
                >

                  <TextField 
                    className = "inputs"
                    type = "text"
                    name = "name"
                    placeholder = { giftName }
                    value = { name }
                    onChange = { onInputChange }
                    variant = "standard"
                    InputProps = {{
                      disableUnderline: true,
                      sx: {
                        color: "white",
                        fontWeight: "bold",
                        fontSize: 15,
                        pt: 1.5,
                        pb: 1.5,
                        pr: 3,
                        pl: 3.5,
                      },
                    }}
                    style = {{
                      underline: "none",
                      borderRadius: 20,
                      backgroundColor: "#6b99b0",
                    }}
                  />

                </Grid>

              </Grid>

              <Grid
                sx = {{
                  mt: 2,
                  ml: 1.5,
                }}
              >

                <Typography
                  sx = {{
                    color: "white",
                    fontSize: 18,
                  }}
                >

                  Descripción

                </Typography>

              </Grid>


              <Grid
                container
                justifyContent = "start"
                sx = {{
                  pt: 1,
                }}
              >

                <Grid
                  container
                  sx = {{
                    width: { xs: "75%", sm: "85%", md: "75%" },
                  }}
                >

                  <textarea 
                    className = "textAreaDescResponsiveLandscape"
                    type = "text"
                    name = "description"
                    placeholder = {giftDescription}
                    value = {description}
                    onChange = { onInputChange }
                    maxLength = {150}
                  />

                </Grid>
                  
                <Grid 
                  container
                  justifyContent = "end"
                  sx = {{
                    width: "90%",
                    height: "15px",
                    position: "relative",
                  }}
                >

                  <Typography
                    component = {'span'} 
                    variant = {'body2'}
                    textAlign = "end"
                    sx = {{ 
                        color: charactersCount >= 150 ? "red" : charactersCount >= 100 ? "orange" : "white",
                        fontWeight: "bold", 
                        fontSize: 15,
                        mr: 5,
                    }}
                  >

                    {charactersCount} / 150 caracteres
                    
                  </Typography>

                </Grid>
                        
              </Grid>

              <Grid
                sx = {{
                  mt: 2,
                  ml: 1.5,
                }}
              >

                <Typography
                  sx = {{
                    color: "white",
                    fontSize: 18,
                    textAlign: "start"
                  }}
                >

                  ¿En que grupo crearás este regalo?

                </Typography>

              </Grid>

              <Grid
                sx = {{
                  mt: 2
                }}
              >

                <Select 
                  defaultValue={giftCommunityId}
                  name='communityID'
                  value={communityID}
                  onChange={onInputChange}
                  sx = {{
                    width: "80%",
                    fontWeight: "bold",
                    backgroundColor: "#6b99b0",
                    borderRadius: 5,
                  }}
                >

                  <MenuItem value = { 0 }>

                    <Typography
                      component = {'span'} 
                      variant = {'body2'}
                      sx = {{ 
                          color: "#003e65",
                          fontWeight: "bold", 
                          fontSize: 20,
                          mr: 5,
                      }}
                    >
                      
                      Público
                      
                    </Typography>

                  </MenuItem>

                  {communities.map( row => (
                    <MenuItem key={row.id} value = { row.id }>

                      <Typography
                        component = {'span'} 
                        variant = {'body2'}
                        sx = {{ 
                            color: "#003e65",
                            fontWeight: "bold", 
                            fontSize: 20,
                            mr: 5,
                        }}
                      >
                        
                        {row.name}
                        
                      </Typography>

                    </MenuItem>
                  ))}
                  
                </Select>

              </Grid>

              <Grid
                sx = {{
                  mt: 5,
                }}
              >

                <Button
                  type = "submit" 
                  variant = "contained" 
                  value = "Register"
                  sx = {{
                    backgroundColor: "#6b96ad",
                    pt: 1,
                    pb: 1,
                    pl: 3,
                    pr: 3,
                    ":hover": { backgroundColor: "#0f69a2" },
                    ml: 1,
                  }}
                >

                  <Typography
                    component = {'span'} 
                    variant = {'body2'}
                    sx = {{ 
                        color: "#004f77",
                        fontWeight: "bold", 
                        fontSize: 25,
                    }}
                  >

                    Editar Regalo

                  </Typography>

                </Button>

              </Grid>

            </Grid>

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              mt: 6,
            }}
          >

            <Grid 
              className = "roundedRectangle"
              sx = {{
                backgroundColor: "#5494b3",
                width: "100%",
                height: "10px",
                borderBottomLeftRadius: 100,
                borderBottomRightRadius: 100,
              }}
            />

          </Grid>

        </Grid>

      </Grid>

      { newAlert() }

    </form>
    
  );

} 

