import { Button, Grid, TextField, Typography, IconButton, Avatar, CircularProgress } from "@mui/material";
import { usePostRegisterCodeMutation, usePostRegisterMutation } from "../../store/api/giftsApi";
import { useForm } from "../../hooks/useForm";
import { Link, useNavigate } from 'react-router-dom';
import { useEffect, useRef, useState } from "react"
import { EmailVerificationUI } from "../components/register/email-verification-ui";
import { useCustomAlerts } from "../../hooks/useCustomAlerts";
import { ErrorIndicator } from "../components/global/error-indicator";
import { useDispatch } from "react-redux";
import { onGetCodeResponse } from "../../store/register/registerSlice";

const registerForm = {
  displayName: '',
  username: '',
  email: '',
  password: '',
  password_confirm: '',
  picture: '',
}

export const Register = () => {

  const fileInputRef = useRef();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [registeringLoad, setRegisteringLoad] = useState(false);
  const [errorMessages, setErrorMessages] = useState({
    usernameError: '',
    emailError: '',
    passwordError: '',
    confirmPasswordError: '',
  });

  const { 
    displayName, 
    username, 
    email, 
    password, 
    password_confirm, 
    picture, 
    onInputChange, 
    onFileChange 
  } = useForm( registerForm );

  const {
    enableLoadAction,
    openAlertDialog,
    openToast,
    closeDialogAlert,
    newAlert,
    newToast
  } = useCustomAlerts();

  const [ register ] = usePostRegisterMutation();
  const [ sendCode ] = usePostRegisterCodeMutation();

  const [avatarSRC, setAvatarSRC] = useState('');

  const resetLoading = () => {
    setRegisteringLoad(false);
  }

  const validateEmail = (email) => {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  }

  const Submit = async(event) => {
    event.preventDefault();

    checkElementErrors(null, true);

    if(!canRegister()) {
      return;
    }

    setRegisteringLoad(true);

    const formData = new FormData();
    formData.append('displayName', displayName);
    formData.append('username', username);
    formData.append('email', email);
    formData.append('password', password);
    formData.append('password_confirm', password_confirm);
    formData.append('picture', picture);

    try {  

      const  { data, error }  = await register( formData );

      resetLoading();

      // User created
      if(data && data.returnCode == 3)
        navigate('/auth/login');
        // openAlertDialog("Confirme su correo", <EmailVerificationUI {...{user: email, submitCode, closeDialogAlert }} />);
      // Need to be confirmed
      else if (error) {
        if(error.data && error.data.returnCode == 1)
          navigate('/auth/login');
        // openAlertDialog("Confirme su correo", <EmailVerificationUI {...{user: email, submitCode, closeDialogAlert }} />);
      }

    } catch (error) {
      resetLoading();
    }
  }

  
  const submitCode = async(code, user) => {
    const formData = new FormData();
    formData.append('code', code);
    formData.append('user', user);

    try {  

      const  { data, error }  = await sendCode( formData );

      // User created
      if(data && data.code == 200) {
        closeDialogAlert();
        navigate('/auth/login', {
          replace: true
        });
      }
      else if( error || (data && data.code != 200) ) {
        openToast("Código incorrecto", "error");
        dispatch( onGetCodeResponse({ codeRegister: 'Código incorrecto.' }) );
      }

    } catch (error) {

    }
  }

  const checkElementErrors = (event, checkOutside = false) => {
    var input = event && event.target ? event.target.name : '';
    
    let usernameError = '', emailError = '', passwordError = '', confirmPasswordError = ''; 

    // Check username 
    if(checkOutside || input == "username") {
      if(username.length <= 0 ) {
        usernameError = 'Usuario obligatorio.';
      }
      else {
        usernameError = '';
      }
    }

    // Check email
    if(checkOutside || input == "email") {
      if(email.length <= 0) {
        emailError = 'Correo obligatorio.';
      }
      else if(!validateEmail(email)) {
        emailError = 'Correo inválido.';
      }
      else {
        emailError = '';
      }
    }

    // Check password
    if(checkOutside || input == "password") {
      if(password.length < 6) {
        passwordError = 'La contraseña necesita mínimo 6 carácteres.';
      }
      else {
        passwordError = '';
      }
    }

    // Check password
    if(checkOutside || input == "password_confirm") {
      if(password_confirm != password) {
        confirmPasswordError = 'Las contraseñas no coinciden.';
      }
      else {
        confirmPasswordError = '';
      }
    }

    setErrorMessages({ usernameError, emailError, passwordError, confirmPasswordError });
  };

  const canRegister = () => {
    return username.length > 0 &&
      email.length > 0 && validateEmail(email) &&
      password.length >= 6 &&
      password_confirm == password; 
  };

  useEffect( () => {
    if(picture) {
      const url = URL.createObjectURL(picture);
      setAvatarSRC(url);
    }
  }, [picture]);

  return (

    <form onSubmit = { Submit }>

      <Grid
        display = "flex"
        sx = {{
          width: { xs: "93vw", sm: "70vw", md: "55vw" },
          height: { xs: "100vh", sm: "750px", md: "80vh" },
          minHeight: { xs: "920px", sm: "900px", md: "0px" },
          mt: { xs: 25, sm: 25, md: 30 },
          mb: { xs: 10, sm: 10, md: 15 },
        }}
      >

        {/* Login side */}

        <Grid
          display = {{ xs: "none", sm: "none", md: "inline" }}
          sx = {{
            backgroundColor: "#01668d",
            height: "100%",
            width: "50%",
            borderTopLeftRadius: 35,
            borderBottomLeftRadius: 35,
          }}
        >

          <Grid
            container
            justifyContent = "center"
            alignItems = "center"
            direction = "column"
            sx = {{ 
              height: "100%" 
            }}
          >

            <Grid
              sx = {{
                textAlign: "center",
              }}
            >

              <Typography
                component = {'span'} 
                variant = {'body2'}
                sx = {{ 
                  color: "white",
                  fontWeight: "bold", 
                  fontSize: { lg: 25, xl: 40 },
                }}
              >

                ¡Bienvenido!

              </Typography>

            </Grid>

            <Grid
              display = {{ md: "none", lg: "flex" }}
              sx = {{
                textAlign: "center",
                mt: 3,
                px: 4,
              }}
            >

              <Typography
                component = {'span'} 
                variant = {'body2'}
                textAlign = "center"
                sx = {{ 
                  color: "white",
                  fontWeight: "bold", 
                  fontSize: { lg: 20, xl: 28 },
                }}
              >

                Si ya tienes una cuenta con nosotros, inicia sesión acá para ver los regalos de los demás

              </Typography>

            </Grid>

            <Grid
              sx = {{
                mt: 5,
              }}
            >

              <Link 
                to="/auth/login"
                style = {{ 
                  textDecoration: "none",
                }}
              >

                <Grid
                  container
                  justifyContent = "center"
                >

                  <Button
                    sx = {{
                      backgroundColor: "white",
                      pt: 1.5,
                      pb: 1.5,
                      pl: 3,
                      pr: 3,
                      ":hover": { backgroundColor: "#95bbcf" }
                    }}
                  >

                    <Typography
                      component = {'span'} 
                      variant = {'body2'}
                      textAlign = "start"
                      sx = {{ 
                        color: "#01668d",
                        fontWeight: "bold", 
                        fontSize: { lg: 18, xl: 25 },
                      }}
                    >

                      Ingresar

                    </Typography>

                  </Button>

                </Grid>  
                    
              </Link>

            </Grid>

          </Grid>

        </Grid>

        {/* Register side */}

        <Grid
          sx = {{
            backgroundColor: "white",
            height: "100%",
            width: { xs: "100%", sm: "100%", md: "80%", lg: "80%", xl: "50%" },
            borderTopRightRadius: 35,
            borderBottomRightRadius: 35,
            borderTopLeftRadius: { xs: 35, sm: 35, md: 0 },
            borderBottomLeftRadius: { xs: 35, sm: 35, md: 0 },
          }}
        >

          <Grid
            container
            justifyContent = "center"
            sx = {{
              pt: { xs: 5, sm: 3, md: 3 },
            }}
          >

            <Typography
              component = {'span'} 
              variant = {'body2'}
              textAlign = "center"
              sx = {{ 
                color: "#01668d",
                fontWeight: "bold", 
                fontSize: { xs: 30, sm: 30, md: 40 },
                pt: { xs: 0, sm: 1 }
              }}
            >

              Crea tu cuenta

            </Typography>

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              pt: 1,
              width: "100%",
            }}
          >

            <Grid
              container
              justifyContent = "center"
              sx = {{
                width: { xs: "85%", sm: "85%", md: "40%" },
                mt: 3,
              }}
            >

              <TextField 
                className = "inputs"
                type = "name"
                placeholder = "Nombre..."
                name = "displayName"
                value = { displayName }
                onChange = { onInputChange }
                variant = "standard"
                InputProps = {{
                  disableUnderline: true,
                  sx: {
                    color: "white",
                    fontWeight: "bold",
                    fontSize: 20,
                    pt: 1.5,
                    pb: 1.5,
                    pr: 3,
                    pl: 3.5,
                  },
                }}
                style = {{
                  underline: "none",
                  borderRadius: 20,
                  backgroundColor: "#6b98ae",
                }}
              />

              <ErrorIndicator message={ errorMessages.usernameError } />

            </Grid>

            <Grid
              display = {{ xs: "none", sm: "none", md: "initial" }}
              sx = {{
                px: 1.5
              }}
            />

            <Grid
              container
              justifyContent = "center"
              sx = {{
                width: { xs: "85%", sm: "85%", md: "40%" },
                mt: 3,
              }}
            >

              <TextField 
                className = "inputs"
                type = "username"
                onBlur={checkElementErrors}
                placeholder = "Usuario..."
                name = "username"
                value = { username }
                onChange = { onInputChange }
                variant = "standard"
                InputProps = {{
                  disableUnderline: true,
                  sx: {
                    color: "white",
                    fontWeight: "bold",
                    fontSize: 20,
                    pt: 1.5,
                    pb: 1.5,
                    pr: 3,
                    pl: 3.5,
                  },
                }}
                style = {{
                  underline: "none",
                  borderRadius: 20,
                  backgroundColor: "#6b98ae",
                }}
              />

              <Grid
                display = { !errorMessages ? "none": "flex" }
              >

                <Typography
                  sx = {{
                    color: "white",
                  }}
                >

                  .

                </Typography>

              </Grid>

            </Grid>

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              mt: 3,
              width: "100%",
            }}
          >

            <Grid
              container
              justifyContent = "center"
              sx = {{
                width: { xs: "85%", sm: "85%", md: "84%" },
              }}
            >

              <TextField 
                className = "inputs"
                type = "email"
                onBlur={checkElementErrors}
                placeholder = "Email..."
                name = 'email'
                value = { email }
                onChange = { onInputChange }
                variant = "standard"
                InputProps = {{
                  disableUnderline: true,
                  sx: {
                    color: "white",
                    fontWeight: "bold",
                    fontSize: 20,
                    pt: 1.5,
                    pb: 1.5,
                    pr: 3,
                    pl: 3.5,
                  },
                }}
                style = {{
                  underline: "none",
                  borderRadius: 20,
                  backgroundColor: "#6b98ae",
                }}
              />

              <ErrorIndicator message={ errorMessages.emailError } />

            </Grid>

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              mt: 3,
              width: "100%",
            }}
          >

            <Grid
              container
              justifyContent = "center"
              sx = {{
                width: { xs: "85%", sm: "85%", md: "84%" },
              }}
            >

              <TextField 
                className = "inputs"
                type = "password"
                onBlur={checkElementErrors}
                placeholder = "Contraseña..."
                name = "password"
                value = { password }
                onChange={ onInputChange }
                variant = "standard"
                InputProps = {{
                  disableUnderline: true,
                  sx: {
                    color: "white",
                    fontWeight: "bold",
                    fontSize: 20,
                    pt: 1.5,
                    pb: 1.5,
                    pr: 3,
                    pl: 3.5,
                  },
                }}
                style = {{
                  underline: "none",
                  borderRadius: 20,
                  backgroundColor: "#6b98ae",
                }}
              />

              <Grid
                display = { "flex" }
                sx = {{
                  width: "100%",
                  mt: 1,
                }}
              >

                <Typography
                  sx = {{
                    color: "red",
                  }}
                >

                  <ErrorIndicator message={ errorMessages.passwordError } />

                </Typography>

              </Grid>

            </Grid>

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              mt: 3,
              width: "100%",
            }}
          >

            <Grid
              container
              justifyContent = "center"
              sx = {{
                width: { xs: "85%", sm: "85%", md: "84%" },
              }}
            >

              <TextField 
                className = "inputs"
                type = "password"
                placeholder = "Contraseña..."
                name = 'password_confirm'
                onBlur={checkElementErrors}
                value = { password_confirm }
                onChange = { onInputChange }
                variant = "standard"
                InputProps = {{
                  disableUnderline: true,
                  sx: {
                    color: "white",
                    width: { xs: "335px", sm: "450px" },
                    fontWeight: "bold",
                    fontSize: 20,
                    pt: 1.5,
                    pb: 1.5,
                    pr: 3,
                    pl: 3.5,
                  },
                }}
                style = {{
                  underline: "none",
                  borderRadius: 20,
                  backgroundColor: "#6b98ae",
                }}
              />

              <Grid
                display = { "flex" }
                sx = {{
                  width: "90%",
                  mt: 1,
                }}
              >

                <Typography
                  sx = {{
                    color: "red"
                  }}
                >

                  <ErrorIndicator message={ errorMessages.confirmPasswordError } />

                </Typography>

              </Grid>

            </Grid>

          </Grid>

          <Grid
            container
            sx = {{
              pt: 3,
              pl: { xs: 3, sm: 8, md: 5 }
            }}
          >

            <input 
              type = "file"
              multiple
              ref = { fileInputRef }
              name = 'picture'
              onChange = { onFileChange }
              style = {{ display: 'none' }}
            />

            <Avatar
              src={avatarSRC}
              onClick = { () => fileInputRef.current.click() }
              sx = {{
                backgroundColor: "orange",
                width: "65px",
                height: "65px",
                cursor: "pointer"
              }}
            />

            <Button
              sx = {{
                ml: { xs: 1, sm: 2 },
              }}
              onClick = { () => fileInputRef.current.click() }
            >

              <Typography
                sx = {{
                  color: "#01668d",
                }}
              >

                Sube una foto de perfil

              </Typography>

            </Button>

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              pt: { xs: 3, sm: 2},
            }}
          >

            <Button
              variant = 'contained' 
              value = 'Register'
              disabled={registeringLoad}
              type="submit"
              sx = {{
                backgroundColor: "#01668d",
                pt: 1.5,
                pb: 1.5,
                pl: 3,
                pr: 3,
                ":hover": { backgroundColor: "#197cae" }
              }}
            >

              <Typography
                component = {'span'} 
                variant = {'body2'}
                textAlign = "start"
                sx = {{ 
                    color: "white",
                    fontWeight: "bold", 
                    fontSize: 25,
                }}
              >

                { registeringLoad ? <CircularProgress /> : 'Registrarse' }

              </Typography>

            </Button>

          </Grid>

          <Grid
            display = {{ md: "none"}}
            sx = {{
              mt: 2.5
            }}
          >

            <Link
              to = '/auth/login'
              style = {{ 
                textDecoration: "none",
                color: "#00417d",
              }}
            >
            
              <Typography
                sx = {{
                  textAlign: "center"
                }}
              >

                ¿Ya tienes una cuenta? 

              </Typography>

            </Link>

          </Grid>

        </Grid>

      </Grid>
      
      { newAlert() }
      { newToast() }

    </form>

  );

}