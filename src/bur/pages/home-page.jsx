import { Grid, Typography } from "@mui/material"
import { useState } from "react"
import { useEffect } from "react"
import { useDispatch } from "react-redux"
import { useNavigate } from "react-router-dom"
import { useForm } from "../../hooks/useForm"
import { useUserData } from "../../hooks/useUserData"
import { useGetFiveMemebersQuery, usePostSearchMutation } from "../../store/api/giftsApi"
import { getGiftData } from "../../store/gifts/giftsSlice"
import { ErrorIndicator } from "../components/global/error-indicator"
import { HomeSearchResult } from "../components/home-page/home-search-result"
import { HomePageInput } from "../components/home-page/index"

const communityData = {
  searchbox: '',
  topic: 0
}

export const HomePage = () => {

  const { communities, communitiesLoading } = useUserData();
  const { searchbox, topic, onInputChange } = useForm( communityData );
  const [ search ] = usePostSearchMutation();

  const [errorMessage, setErrorMessage] = useState('');
  const [searchRes, setSearchRes] = useState([]);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { data: dataRandom } = useGetFiveMemebersQuery();

  const searchInputChange = (e) => {
    onInputChange(e);
  }

  const onSubmitInput = async (e) => {
    e.preventDefault();

    setErrorMessage('');
  
    const communityID = topic <= 1 ? 0 : topic - 2;
    
    const { data: dataSearch, error } = await search({ communityID, searchText: searchbox, searchType: topic });

    console.log(dataSearch);
    console.log(error);

    if(dataSearch && dataSearch.code === 200) {
      setSearchRes(dataSearch.rows);
    }
    else if(dataSearch && dataSearch.code != 200) {
      setErrorMessage(`No hubo resultados con: ${searchbox}`);
    }
    else
      setSearchRes([]);
  }

  const selectSearchItem = (data) => {
    if(topic >= 1) {
      dispatch( getGiftData(data) );
      navigate('/gifts/view');
    }
    else {
      navigate(`/gifts/user/${data.id}`);
    }
  }
    
  useEffect(() => {
    setSearchRes([]);
  }, [topic]);

  useEffect(() => {
    if(dataRandom && dataRandom.code == 200)
      setSearchRes(dataRandom.rows);
  }, [dataRandom]);

  if(communitiesLoading) {
    return (
      <h2 style={{ color: 'white' }}>Loading...</h2>
    )
  }

  return (
    <Grid
      container
      alignContent = 'start'
      justifyContent = 'center'
      sx={{
        minHeight: '100vh',
        color: 'white',
      }}
    >

      <HomePageInput 
        communities={communities} 
        onSubmitInput={onSubmitInput}
        searchInputChange={searchInputChange} 
        searchValue={searchbox}
        topicValue={topic}
      />

      <Grid
        container
        sx = {{
          width: { xs: "80%", sm: "70%", md: "60%" },
        }}
      >

        <ErrorIndicator message={errorMessage} color={"white"} size={30} />

        {
          searchRes.map(res => (
            <HomeSearchResult 
              {...res}
              clickable={ selectSearchItem }
              key={res.id} 
              communityID={topic <= 1 ? 0 : topic - 2}
              topic={topic} 
            />
          ))
        }

      </Grid>

    </Grid>
  )
}
