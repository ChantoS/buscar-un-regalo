import { Grid, Typography } from "@mui/material";
import { useGetUserReservedGiftsQuery } from "../../store/api/giftsApi";
import { ReservedGiftsLayout } from "../components/reserved-gifts/reserved-gift-layout";

export const ReserveGifts = () => {

    const { data } = useGetUserReservedGiftsQuery();

    return (
        
        <Grid
            sx = {{
                width: "100vw",
                height: "70vh",
                mt: { xs: 20, sm: 25 },
            }}
        >

            <Grid
                container
                justifyContent = "center"
            >

                <Typography
                    component = {'span'}
                    variant = {'body2'}
                    sx = {{ 
                        color: "white",
                        fontWeight: "bold",
                        fontSize: { xs: 30, sm: 40, md: 50 },
                    }}
                >

                    Regalos que apartaste

                </Typography>

            </Grid>

            <Grid
                container
                justifyContent = "center"
                sx = {{
                    width: "100%",
                }}
            >

                <Grid 
                    className = "roundedRectangle"
                    sx = {{
                        backgroundColor: "#5494b3",
                        height: "4px",
                        width: "90%",
                        borderTopLeftRadius: 100,
                        borderTopRightRadius: 100,
                        borderBottomRightRadius: 100,
                        borderBottomLeftRadius: 100,
                        mt: 3,
                    }}
                />

            </Grid>

            <Grid
                container
                justifyContent = "center"
                sx = {{
                    mt: { xs: 5, sm: 10 },
                    width: "100vw",
                }}
            >

                <Grid
                    container
                    justifyContent = "center"
                    sx = {{
                        width: { xs: "80vw", sm: "100vw", md: "90vw"},
                    }}
                >
                    
                    {
                        data && data.rows && data.rows.map((row) => (
                            <ReservedGiftsLayout key={row.id} {...row} />
                        ))
                    }

                    {
                        (!data || (data.code && data.code != 200)) && (

                            <Typography
                                sx={{
                                    color: 'white',
                                    fontSize: 25,
                                    textAlign: "center"
                                }}
                            >

                                No has apartado ningún regalo todavía.

                            </Typography>

                        )
                    }

                </Grid>

            </Grid>

        </Grid>

    );

}
