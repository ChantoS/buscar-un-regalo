import { Button, Grid, TextField, Typography } from "@mui/material";
import { useState } from "react";
import { apiURL } from "../../helpers";
import { useCustomAlerts } from "../../hooks/useCustomAlerts";
import { useForm } from "../../hooks/useForm";
import { useUserData } from "../../hooks/useUserData";
import { usePostCommunitiesMutation, usePostLeaveCommunityMutation } from "../../store/api/giftsApi";
import { PictureHeader } from "../components/global/picture-header";
import { GroupLayout } from "../components/groups-page/groups-layout";

const createGroupState = {
  groupName: ''
};

export const GroupsPage = () => {

  const { groupName, onInputChange, onResetForm } = useForm(createGroupState);
  const { communities, communitiesLoading, profilePicture } = useUserData();
  const [ createGroup ] = usePostCommunitiesMutation();
  const [ leaveGroup ] = usePostLeaveCommunityMutation();
  const [creatingGroup, setCreatingGroup] = useState(false);

  const {
    openToast,
    newToast
  } = useCustomAlerts();

  const postCreateGroup = async (e) => {
    e.preventDefault();

    if(creatingGroup) {
      return;
    }
    
    if(groupName <= 0) {
      openToast("Nombre obligatorio para crear el grupo", 'warning');
      return;
    }
    
    setCreatingGroup(true);

    // Save info and clear form
    const groupNameValue = groupName;
    onResetForm();

    const { data: createData, error: createDataError } = await createGroup({ name: groupNameValue });

    setCreatingGroup(false);

    if(createData && createData.code == 200) {
      openToast("Grupo creado!", 'success');
    }
    else if(createDataError) {
      openToast("Error al crear el grupo", 'error');
    }
  };

  const postLeaveGroup = async (communityID, callback) => {
    const { data: leaveData, error: leaveError } = await leaveGroup({ communityID });
    if(leaveData && leaveData.code == 200) {
      openToast("Salió exitosamente del grupo", 'success');
      if(callback)
        callback();
    }
    else if (leaveError) {
      openToast("Hubo un error al querer salir del grupo", 'success');
      console.log(leaveError);
    }
  };

  return (
    <form onSubmit={ postCreateGroup }>

      <Grid
        sx = {{
          width: "100vw",
          height: "600px",
        }}
      >

        <Grid 
          sx = {{
            py: 1.5
          }}
        />

        <PictureHeader profilePicture={ profilePicture.length > 1 ? `${apiURL}uploads/users/${profilePicture}` : ''} title = "Mis grupos" />

        <Grid
          container
          direction = "column"
          justifyContent = "center"
          alignContent = "center"
          sx = {{
            mt: 5,
          }}
        >

          <Grid
            container
            justifyContent = "center"
            sx = {{
              width: "100%",
            }}
          >

            <Grid
              container
              sx = {{
                width: "300px",
              }}
            >

              <TextField 
                className = "inputs"
                placeholder = "Aa..."
                variant = "standard"
                name = "groupName"
                value = { groupName }
                onChange = { onInputChange }
                InputProps = {{
                  disableUnderline: true,
                  sx: {
                    color: "white",
                    fontWeight: "bold",
                    fontSize: 15,
                    pt: 1.5,
                    pb: 1.5,
                    pr: 3,
                    pl: 3.5,
                  },
                }}
                style = {{
                  underline: "none",
                  borderRadius: 20,
                  backgroundColor: "#6b99b0",
                }}
              />

            </Grid>

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              width: "100%",
              mt: 3,
              mb: 5,
            }}
          >

            <Button
              variant = "contained" 
              type="submit"
              sx = {{
                backgroundColor: "white",
                pt: 1.5,
                pb: 1.5,
                pl: 3,
                pr: 3,
                ":hover": { backgroundColor: "#95bbcf" },
                borderRadius: 20
              }}
            >

              <Typography
                component = {'span'} 
                variant = {'body2'}
                textAlign = "start"
                sx = {{ 
                    color: "#01668d",
                    fontWeight: "bold", 
                    fontSize: { xs: 15, sm: 25 },
                }}
              >

                Crear grupo

              </Typography>

            </Button>

          </Grid>

        </Grid>

        { 
          !communitiesLoading && communities &&
          communities.map((row) => (
            <GroupLayout key={row.id} leaveAction={postLeaveGroup} {...row} />
          ))
        }

      </Grid>
      { newToast() }
    </form>

  );

}
