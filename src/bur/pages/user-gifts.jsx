import { Grid, MenuItem, Select, Typography } from "@mui/material";
import { useState } from "react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { apiURL } from "../../helpers";
import { useForm } from "../../hooks/useForm";
import { useUserData } from "../../hooks/useUserData";
import { useGetCommonCommunitiesQuery, useGetUserGiftsQuery, useGetUserQuery } from "../../store/api/giftsApi";
import { getGiftData } from "../../store/gifts/giftsSlice";
import { PictureHeader } from "../components/global/picture-header";
import { GiftComponent } from "../components/user-gifts/gift-component";

const initialCommunity = {
    community: 0
}
    
let profileImage = '';
let display_name = '';

export const UserGifts = ({ id }) => {

    // Initial Data
    const { community, onInputChange } = useForm(initialCommunity);
    const { myID } = useUserData();
    const { giftUserID = myID } = useParams();
    
    const { data: userInfo, error: userError } = useGetUserQuery(giftUserID);
    const { data: giftData } = useGetUserGiftsQuery({id: giftUserID, community});
    const { data: communityData, error: communityError } = useGetCommonCommunitiesQuery(giftUserID);
    
    const [giftList, setGiftList] = useState([]);
    const [communityList, setCommunityList] = useState([]);

    const navigate = useNavigate();
    const dispatch = useDispatch();

    useEffect( () => {
        if(userInfo && userInfo.code && userInfo.code == 200) {
            profileImage = userInfo.result[0].img;
            display_name = userInfo.result[0].display_name;
        }
    }, [userInfo]);

    useEffect( () => {
        if(communityData && communityData.code && communityData.code == 200) {
            setCommunityList(communityData.rows);
        }
        else {
            setCommunityList([]);
        }
    }, [communityData]);

    useEffect( () => {
        if(giftData && giftData.code && giftData.code == 200) {
            setGiftList(giftData.rows);
        }
        else {
            setGiftList([]);
        }
    }, [giftData]);

    const showGift = ( gift ) => {
        dispatch( getGiftData({...gift, communityID: community}) );
        navigate('/gifts/view');
    }

    if(userError) {
        return <h1 style={{ color: 'white'}}>Este usuario no existe o ha sido bloqueado.</h1>
    }

    if(display_name.length > 1)
    return (

        <Grid
            sx = {{
                width: "100%",
                height: "600px",
            }}
        >

            <Grid 
                sx = {{
                    py: 2,
                }}
            />

            <PictureHeader img={ profileImage.length > 1 ? `${apiURL}uploads/users/${profileImage}` : ''} title={display_name} />
            
            <Grid
                container
                direction = "column"
                alignContent = "center"
                justifyContent = "center"
                sx = {{
                    width: "100%",
                }}
            >

                <Grid
                    container
                    alignContent = "center"
                    justifyContent = "center"
                    sx = {{
                        mt: 2,
                        width: "100%",
                    }}
                >

                    <Select
                        defaultValue = { 0 }
                        name='community'
                        value={community}
                        onChange={onInputChange}
                        sx = {{
                            width: "200px",
                            fontWeight: "bold",
                            backgroundColor: "#6b99b0",
                            borderRadius: 5,
                            textAlign: { xs: "center", sm: "center" },
                        }}
                    >

                        <MenuItem value = { 0 }>

                            <Typography
                                component = {'span'} 
                                variant = {'body2'}
                                sx = {{ 
                                    color: "#003e65",
                                    fontWeight: "bold", 
                                    fontSize: 25,
                                    mr: 5,
                                }}
                            >
                                
                                Grupo: Público
                                
                            </Typography>

                        </MenuItem>

                        {communityList.map( row => (
                            <MenuItem key={row.id} value = { row.id }>

                                <Typography
                                    component = {'span'} 
                                    variant = {'body2'}
                                    sx = {{ 
                                        color: "#003e65",
                                        fontWeight: "bold", 
                                        fontSize: 25,
                                        mr: 5,
                                    }}
                                >
                                    
                                    Grupo: {row.name}
                                    
                                </Typography>

                            </MenuItem>
                        ))}

                    </Select>

                </Grid>

            </Grid>

            <Grid
                container
                justifyContent = "center"
                sx = {{
                    mt: 3.5,
                }}
            >

                <Grid 
                    className = "roundedRectangle"
                    sx = {{
                        backgroundColor: "#5494b3",
                        height: "3px",
                        width: "40%",
                    }}
                />

            </Grid>

            <Grid
                container
                justifyContent = "center"
                sx = {{
                    width: "100%",
                    mt: 5,
                    pb: 12,
                    pr: { xs: 25, sm: 0, md: 25 },
                    pl: { xs: 25, sm: 0, md: 25 },
                }}
            >               
                
                { giftList.map ( res => (

                    <GiftComponent key={res.id} viewGift={showGift} {...res} />
                    
                ))}

            </Grid>

        </Grid>

    );

}
