import { Button, Grid, TextField, Typography } from "@mui/material";
import { useState } from "react";
import { useRef } from "react";
import { Navigate, useNavigate, useParams } from "react-router-dom";
import { apiURL } from "../../helpers";
import { useForm } from "../../hooks/useForm";
import { useGetCommunityByIDQuery, usePostJoinCommunityMutation } from "../../store/api/giftsApi";
import { ErrorMessage } from "../components/global/error-message";
import { PictureHeader } from "../components/global/picture-header";
import { useCustomAlerts } from "../../hooks/useCustomAlerts";

export const GroupsPageJoin = () => {

  const { groupID } = useParams();
  const { data, isLoading, error } = useGetCommunityByIDQuery(groupID);
  const [ joinGroup ] = usePostJoinCommunityMutation(); 
  const navigate = useNavigate();
  const [joinGroupError, setJoinGroupError] = useState({code: 0, message: ""}); 
  const { password, onInputChange } = useForm({ password: '' });

  const {
    openToast, newToast
  } = useCustomAlerts();

  const onJoin = async () => {

    if(data && data.rows.length > 0 && data.rows[0].isprotected && password.length <= 0) {
      openToast("La contraseña no puede estar vacía", 'warning');
      return;
    }

    const { data:res, error: joinError } = await joinGroup({ communityID: data.rows[0].id, password });
    if(res && res.code === 200) {
      navigate('/groups/mygroup', {
        replace: true
      });
    }
    if(joinError) {
      if(joinError.data.errCode == 1)
        openToast("La contraseña es incorrecta", 'error');
      else if(joinError.data.errCode == 2)
          openToast("Ya perteneces a este grupo");
    }
  };

  if(isLoading) {
    return <h1>Loading...</h1>
  }

  if((data && data.code && data.code != 200)) {
    return <Navigate to='/groups/mygroup' />
  }

  if(data && data.code && data.code == 200) {

    return (

      <Grid
        sx = {{
          width: "100%",
          height: "600px",
        }}
      >
        { newToast() }
        <PictureHeader 
          img = {data.rows[0].img ? `${apiURL}uploads/communities/${data.rows[0].img}` : ''} 
          title = {data.rows[0].name} 
        />

        <Grid
          container
          alignItems = "center"
          justifyContent = "center"
          sx = {{
            mt: 2,
          }}
        >

          <Typography 
            sx = {{
              color: 'white',
              fontSize: 20,
            }}
          > 
          
            { data.rows[0].description } 
                
          </Typography>

        </Grid>

        <Grid
          container
          alignItems = "center"
          justifyContent = "center"
          sx = {{
            mt: 3,
          }}
        > 

          <Grid 
            className = "roundedRectangle"
            sx = {{
              backgroundColor: "#5494b3",
              height: "3px",
              width: "40%",
              borderTopLeftRadius: 100,
              borderTopRightRadius: 25,
              borderBottomRightRadius: 25,
            }}
          />

        </Grid>

        { data.rows[0].isprotected == 1 && (
          <>
            <Grid
              container
              alignItems = "center"
              justifyContent = "center"
              sx = {{
                mt: 5
              }}
            >

              <Typography
                sx = {{
                  color: "white",
                  fontSize: 20
                }}
              >

                Contraseña del grupo

              </Typography>

            </Grid>

            <Grid
              container
              alignItems = "center"
              justifyContent = "center"
              sx = {{
                mt: 2,
              }}
            >

              <TextField
                variant = "standard"
                type="password"
                name='password'
                value={password}
                onChange={onInputChange}
                InputProps = {{
                  disableUnderline: true,
                  sx: {
                    color: "white",
                    width: "100%",
                    fontWeight: "bold",
                    fontSize: 20,
                    pt: 1.5,
                    pb: 1.5,
                    pr: 3,
                    pl: 3.5,
                  },
                }}
                style = {{
                  underline: "none",
                  borderRadius: 20,
                  backgroundColor: "#6b98ae",
                }}
              />

            </Grid>
          </>
        )}
        
        <Grid
          container
          display='column'
          alignItems = "center"
          justifyContent = "center"
          sx = {{
            mt: 5,
          }}
        >
          <Grid>

            <Button
              variant = "contained" 
              onClick={onJoin}
              sx = {{
                backgroundColor: "white",
                pt: 1.5,
                pb: 1.5,
                pl: 3,
                pr: 3,
                ":hover": { backgroundColor: "#95bbcf" },
                borderRadius: 20,
              }}
            >
    
              <Typography
                component = {'span'} 
                variant = {'body2'}
                textAlign = "start"
                sx = {{ 
                    color: "#01668d",
                    fontWeight: "bold", 
                    fontSize: 20,
                }}
              >

                Unirse al grupo

              </Typography>
    
            </Button>

            <Grid 
              sx = {{
                display: joinGroupError.code > 0 ? "block" : "none",
                mt: 3,
                width: "100%",
                textAlign: "center"
              }}
            >

            <ErrorMessage message={joinGroupError.message} color="white" />

          </Grid>

          </Grid>

        </Grid>
        
      </Grid>
  
    );

  }

}
