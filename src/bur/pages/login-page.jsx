import { Link } from 'react-router-dom';

import { useAuthStore } from "../../hooks/useAuthStore";
import { useForm } from "../../hooks/useForm";
import { usePostLoginMutation, usePostRegisterCodeMutation } from "../../store/api/giftsApi";

import { Button, CircularProgress, Grid, TextField, Typography } from "@mui/material";
import { useState } from "react";
import { RecoverPasswordComponent } from "../components/register/recover-password-component";
import { useCustomAlerts } from "../../hooks/useCustomAlerts";
import { ErrorIndicator } from '../components/global/error-indicator';
import { onGetCodeResponse } from '../../store/register/registerSlice';
import { useDispatch } from 'react-redux';

const logingForm = {
  username: '',
  password: ''
}

export const Login = () => {

  const { startLogin } = useAuthStore();

  const { username, password, onInputChange } = useForm( logingForm );
  const [ login ] = usePostLoginMutation();
  const dispatch = useDispatch();

  const [ sendCode ] = usePostRegisterCodeMutation();
  const [errorMessages, setErrorMessages] = useState({
    userError: '',
    passError: '',
  })

  const [loginLoad, setLoginLoad] = useState(false);

  const resetLoading = () => {
    setLoginLoad(false);
  }

  const {
    openAlertDialog,
    openToast,
    closeDialogAlert,
    newAlert,
    newToast
  } = useCustomAlerts();

  const Submit = (event) => {
    event.preventDefault();

    checkElementErrors(undefined, true);
    
    if(!canLogin())
      return;

    setLoginLoad(true);
    
    startLogin(login, 
      username, 
      password, 
      submitCode, 
      openToast, 
      openAlertDialog, 
      closeDialogAlert,
      resetLoading);
  }

  const submitCode = async(code, user) => {

    const formData = new FormData();
    formData.append('code', code);
    formData.append('user', user);

    try {  

      const  { data, error }  = await sendCode( formData );

      if(data && data.code == 200) {
        closeDialogAlert();
        startLogin(login, 
          username, 
          password, 
          submitCode, 
          openToast, 
          openAlertDialog, 
          closeDialogAlert,
          resetLoading);
      }
      else if( error || (data && data.code != 200) ) {
        dispatch( onGetCodeResponse({ codeRegister: 'Código incorrecto.' }) );
      }

    } catch (error) {
      console.log(error);
    }
  }

  const checkElementErrors = (event, checkOutside = false) => {
    var input = event && event.target ? event.target.name : '';
    
    let userError = '', passError = ''; 

    // Check username 
    if(checkOutside || input == "username") {
      if(username.length <= 0 ) {
        userError = 'Usuario o correo obligatorio.';
      }
      else {
        userError = '';
      }
    }

    // Check password
    if(checkOutside || input == "password") {
      if(password.length < 6) {
        passError = 'La contraseña necesita mínimo 6 carácteres.';
      }
      else {
        passError = '';
      }
    }

    setErrorMessages({ userError, passError });
  };

  const canLogin = () => {
    return username.length > 0 && password.length >= 6; 
  };

  return (

    <form
      onSubmit={ Submit }
    >

      <Grid
        display = "flex"
        sx = {{
          width: { xs: "92vw", sm: "80vw", md: "55vw" },
          height: { xs: "75vh", sm: "600px", md: "65vh" },
          mt: { xs: 25, sm: 25, md: 15 },
          mb: { xs: 10, sm: 10, md: 0 }
        }}
      >

        {/* Login side */}

        <Grid
          sx = {{
            backgroundColor: "white",
            height: "100%",
            minHeight: "580px",
            width: { xs: "100%", sm: "100%", md: "80%", lg: "80%", xl: "50%" },
            borderTopLeftRadius: 35,
            borderBottomLeftRadius: 35,
            borderTopRightRadius: { xs: 35, sm: 35, md: 0 },
            borderBottomRightRadius: { xs: 35, sm: 35, md: 0 },
          }}
        >

          <Grid
            container
            alignItems = "center"
            justifyContent = "center"
            sx = {{
              pt: { xs: 5, sm: 3, md: 7 },
            }}
          >

            <Typography
              component = {'span'} 
              variant = {'body2'}
              textAlign = "center"
              sx = {{ 
                color: "#01668d",
                fontWeight: "bold", 
                fontSize: { xs: 30, sm: 40, md: 25, lg: 40, xl: 40 },
              }}
            >

              Ingresa a tu cuenta

            </Typography>

          </Grid>

          <Grid
            sx = {{
              pt: { xs: 4.5, sm: 4, md: 6 },
            }}
          >

            <Typography
              component = {'span'} 
              variant = {'body2'}
              textAlign = "start"
              sx = {{ 
                  color: "#01668d",
                  fontWeight: "bold", 
                  fontSize: 20,
                  mt: 5,
                  ml: { xs: 6, sm: 7, md: 10 },
              }}
            >

              Usuario

            </Typography>

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              pt: 1,
              width: "100%",
            }}
          >

            <Grid
              container
              justifyContent = "center"
              sx = {{
                width: { xs: "75%", sm: "85%", md: "75%" },
              }}
            >

              <TextField 
                className = "inputs"
                placeholder = "Aa..."
                type = "username"
                name = "username"
                onBlur={checkElementErrors}
                value = { username }
                onChange = { onInputChange }
                variant = "standard"
                InputProps = {{
                  disableUnderline: true,
                  sx: {
                    color: "white",
                    fontWeight: "bold",
                    fontSize: 20,
                    pt: 1.5,
                    pb: 1.5,
                    pr: 3,
                    pl: 3.5,
                  },
                }}
                style = {{
                  underline: "none",
                  borderRadius: 20,
                  backgroundColor: "#6b98ae",
                }}
              />

              <ErrorIndicator message={ errorMessages.userError } />

            </Grid>

          </Grid>

          <Grid
            sx = {{
              pt: 3,
            }}
          >

            <Typography
              component = {'span'} 
              variant = {'body2'}
              textAlign = "start"
              sx = {{ 
                  color: "#01668d",
                  fontWeight: "bold", 
                  fontSize: 20,
                  mt: 5,
                  ml: { xs: 6, sm: 7, md: 10 },
              }}
            >

              Contraseña

            </Typography>

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              pt: 1,
            }}
          >

            <Grid
              container
              justifyContent = "center"
              sx = {{
                width: { xs: "75%", sm: "85%", md: "75%" },
              }}
            >

              <TextField 
                className = "inputs"
                placeholder = "Aa..."
                type = "password"
                onBlur={checkElementErrors}
                name = 'password'
                value = { password }
                onChange = { onInputChange }
                variant = "standard"
                InputProps = {{
                  disableUnderline: true,
                  sx: {
                    color: "white",
                    fontWeight: "bold",
                    fontSize: 20,
                    pt: 1.5,
                    pb: 1.5,
                    pr: 3,
                    pl: 3.5,
                  },
                }}
                style = {{
                  underline: "none",
                  borderRadius: 20,
                  backgroundColor: "#6b98ae",
                }}
              />

              <ErrorIndicator message={ errorMessages.passError } />

            </Grid>

          </Grid>

          <Grid
            sx = {{
              mt: 1,
              textAlign: "end",
              mr: { xs: 5, sm: 7 },
            }}
          >

            <Button
              onClick={() => openAlertDialog("Recupera tu contraseña", <RecoverPasswordComponent {...{closeDialogAlert}} /> )}
              variant = "contained" 
              sx = {{
                backgroundColor: "white",
                ":hover": { backgroundColor: "#95bbcf" },
                boxShadow: "none",
              }}
            >

              <Typography
                component = {'span'} 
                variant = {'body2'}
                textAlign = "start"
                sx = {{ 
                    color: "#01668d",
                    fontWeight: "bold", 
                    fontSize: { xs: 12, sm: 15, md: 10, lg: 15, xl: 15 },
                }}
              >

                ¿Olvidaste tu contraseña?

              </Typography>

            </Button>

          </Grid>

          <Grid
            container
            alignItems = "center"
            justifyContent = "center"
            sx = {{
              width: "100%",
              mt: { xs: 1, sm: 2.3, md: 2.3 }, 
            }}
          >

          </Grid>

          <Grid
            sx = {{
              pt: 1,
            }}
          >

            <Grid
              container
              justifyContent = "center"
              sx = {{
                pt: 2,
              }}
            >
            
              <Button
                type = "submit"
                variant = "contained" 
                value = "Login"
                disabled={loginLoad}
                sx = {{
                  backgroundColor: "#01668d",
                  pt: 1.5,
                  pb: 1.5,
                  pl: 3,
                  pr: 3,
                  ":hover": { backgroundColor: "#197cae" }
                }}
              >
              
                <Typography
                  component = {'span'} 
                  variant = {'body2'}
                  textAlign = "start"
                  sx = {{ 
                      color: "white",
                      fontWeight: "bold", 
                      fontSize: { xs: 25, sm: 20, md: 25 },
                  }}
                >
                
                  { loginLoad ? <CircularProgress /> : 'Ingresar' }
                
                </Typography>
                
              </Button>
                
            </Grid>

          </Grid>

          <Grid
            container
            display = {{ md: "none" }}
            justifyContent = "center"
            sx = {{
              mt: 1,
            }}
          >

            <Grid
              sx = {{
                mr: 2,
              }}
            >

              <Typography
                sx = {{
                  color: "#6b98ae"
                }}
              >

                ¿Eres nuevo por aquí? 

              </Typography>

            </Grid>

            <Grid>

              <Link
                to = "/auth/register"
              >
              
                <Typography
                  sx = {{
                    color: "#01668d"
                  }}
                >

                  Registrate

                </Typography>

              </Link>

            </Grid>

          </Grid>

        </Grid>

        {/* Register side */}

        <Grid
          container
          justifyContent = "center"
          alignItems = "center"
          display = {{ xs: "none", sm: "none", md: "inline" }}
          sx = {{
            backgroundColor: "#01668d",
            height: "100%",
            width: "50%",
            borderTopRightRadius: 35,
            borderBottomRightRadius: 35,
          }}
        >

          <Grid 
            container
            justifyContent = "center"
            alignItems = "center"
            direction = "column"
            sx = {{ 
              height: "100%" 
            }}
          >

            <Grid
              sx = {{
                textAlign: "center",
                px: 2,
              }}
            >

              <Typography
                component = {'span'} 
                variant = {'body2'}
                textAlign = "center"
                sx = {{ 
                  color: "white",
                  fontWeight: "bold", 
                  fontSize: { lg: 25, xl: 40 },
                }}
              >

                Hey, ¿eres nuevo por aquí?

              </Typography>

            </Grid>

            <Grid
              sx = {{
                textAlign: "center",
                mt: 5,
                px: 3,
              }}
            >

              <Typography
                component = {'span'} 
                variant = {'body2'}
                textAlign = "center"
                sx = {{ 
                  color: "white",
                  fontWeight: "bold", 
                  fontSize: { lg: 20, xl: 28 },
                }}
              >

                Ingresa tus datos personales para crear tu cuenta

              </Typography>

            </Grid>

            <Grid
              sx = {{
                mt: 5,
              }}
            >

              <Link 
                to = "/auth/register"
                style = {{ 
                  textDecoration: "none",
                }}
              >

                <Button
                  variant = "contained" 
                  sx = {{
                    backgroundColor: "white",
                    pt: 1.5,
                    pb: 1.5,
                    pl: 3,
                    pr: 3,
                    ":hover": { backgroundColor: "#95bbcf" }
                  }}
                >

                  <Typography
                    component = {'span'} 
                    variant = {'body2'}
                    textAlign = "start"
                    sx = {{ 
                      color: "#01668d",
                      fontWeight: "bold", 
                      fontSize: { lg: 18, xl: 25 },
                    }}
                  >

                    REGISTRARSE

                  </Typography>

                </Button>

              </Link>

            </Grid>
            
          </Grid>

        </Grid>

      </Grid>

      { newAlert() }
      { newToast() }

    </form>

  );
}