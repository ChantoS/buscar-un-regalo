import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import { Avatar, Button, Grid, IconButton, Typography } from '@mui/material';
import { usePostRecoverCodeMutation, usePutEditUserMutation } from '../../store/api/giftsApi';
import BorderColorIcon from '@mui/icons-material/BorderColor';
import { useDispatch } from 'react-redux';
import { editUser } from '../../store/auth/authSlice';
import { useForm } from '../../hooks/useForm';
import { useEffect } from 'react';
import { useRef } from 'react';
import { useUserData } from '../../hooks/useUserData';
import { apiURL } from '../../helpers';
import { useCustomAlerts } from '../../hooks/useCustomAlerts';
import { EditInfoComponent } from '../components/profile/edit-info-component';

export const ProfilePage = () => {

  const fileInputRef = useRef();
  const dispatch = useDispatch();

  const { 
    myName, 
    myEmail, 
    profilePicture 
  } = useUserData();

  const { 
    displayName, 
    email, 
    picture, 
    fileChanged,
    onInputChange, 
    onFileChange 
  } = useForm( { displayName: myName, email: myEmail, picture: '' } );

  const {
    enableLoadAction,
    openAlertDialog,
    openToast,
    closeDialogAlert,
    newAlert,
    newToast
  } = useCustomAlerts();

  const [ edit ] = usePutEditUserMutation();
  const [ sendCode ] = usePostRecoverCodeMutation();

  const handleEditName = async() => {

    try {

      const { data } = await edit({ displayName, myEmail, profilePicture });
      
      if(data.code === 200) { 
        dispatch(editUser({ name: displayName, email: myEmail, img: profilePicture}));
        closeDialogAlert();
        openToast('Se ha cambiado el nombre correctamente.', 'success');
      }

    } catch (error) {
      console.log({error});
    }
  }

  const handleEditEmail = async() => {  

    try {

      const { data } = await edit({ myName, email, profilePicture });
      
      if(data.code === 200) {
        dispatch(editUser({ name: myName, email, img: profilePicture }));
        closeDialogAlert();
        openToast('Se ha cambiado el correo correctamente.', 'success');
      }

    } catch (error) {
      console.log({error});
    }
  }

  const handleEditImg = async() => {

    const formData = new FormData();
    formData.append('picture', picture);

    try {

      const { data }  = await edit( formData );
      
      if(data.code === 200) {
        dispatch(editUser({ name: myName, email: myEmail, img: data.newImg }));
        closeDialogAlert();
        openToast('Se ha cambiado tu foto correctamente.', 'success');
      }

    } catch (error) {
      console.log(error);
    }

  }

  const sendRecoverCode = async () => {
      const { data, error } = await sendCode({ email: myEmail });

      console.log(data);
      console.log(error);

      if(data && data.code == 200) {
          closeDialogAlert();
          openToast("Se te ha enviado un correo, ve a revisarlo.", 'success');
      }
  }

  useEffect( () => {

    if(fileChanged) {

      // Image picked
      if(picture !== undefined && picture !== null)
        handleEditImg();
      // Action cancelled
      else 
        console.log("User cancelled");

    }
  }, [picture]);
  
  return (

    <>

      {/* Desktop UI */}
    
      <Grid
        display = {{ xs: "none", sm: "none", md: "flex" }}
        sx = {{
          height: "65vh",
          width: { md: "70vw", lg: "55vw", xl: "55vw" },
          mt: 15,
        }}
      >

        <Grid
          sx = {{
            height: "65vh",
            width: { md: "70%", lg: "65%" , xl: "60%" },
          }}
        >

          <Grid>

            <Typography
              component = {'span'} 
              variant = {'body2'}
              sx = {{ 
                color: "white",
                fontWeight: "bold", 
                fontSize: 30,
                mt: 5,
              }}
            >

              Nombre

            </Typography>

          </Grid>

          <Grid 
            className = "roundedRectangle"
            sx = {{
                backgroundColor: "#5494b3",
                height: "4px",
                width: "55%",
                borderTopLeftRadius: 100,
                borderTopRightRadius: 100,
                borderBottomRightRadius: 100,
                borderBottomLeftRadius: 100,
                mt: 1.5,
            }}
          />

          <Grid
            container
            sx = {{
              mt: 3
            }}
          >

            <Grid
              sx = {{
                width: "70%"
              }}
            >

              <input
                className = "profileInputsLarge"
                type = "text"
                placeholder = { myName }
                name = "displayName"
                value = { displayName }
                onChange = { onInputChange }
              />

            </Grid>

            <Grid>
            
              <IconButton
                variant = 'contained' 
                value = 'editName'
                onClick = { () => openAlertDialog('¿Seguro que quieres cambiar tu nombre público?', <EditInfoComponent {... {editAction: handleEditName, closeDialogAlert } } /> ) }
                sx = {{
                  padding: 2,
                }}
              >

                <BorderColorIcon
                  sx = {{
                    color: "white",
                    fontSize: 35,
                  }}
                />

              </IconButton>

            </Grid>

          </Grid>

          <Grid
            sx = {{
              mt: 7,
            }}
          >

            <Typography
              component = {'span'} 
              variant = {'body2'}
              sx = {{ 
                color: "white",
                fontWeight: "bold", 
                fontSize: 30,
                mt: 5,
              }}
            >

              Correo

            </Typography>

          </Grid>

          <Grid 
            className = "roundedRectangle"
            sx = {{
                backgroundColor: "#5494b3",
                height: "4px",
                width: "55%",
                borderTopLeftRadius: 100,
                borderTopRightRadius: 100,
                borderBottomRightRadius: 100,
                borderBottomLeftRadius: 100,
                mt: 1.5,
            }}
          />

          <Grid
            container
            sx = {{
              mt: 3,
            }}
          >

            <Grid
              sx = {{
                width: "70%"
              }}
            >

              <input
                className = "profileInputsLarge"
                type = "text"
                placeholder = { myEmail }
                name= "email"
                value = { email }
                onChange = { onInputChange }
              />

            </Grid>

            <Grid>

              <IconButton
                variant = "contained" 
                value = "editEmail"
                onClick = { () => openAlertDialog('¿Seguro que quieres cambiar el correo de tu cuenta?', <EditInfoComponent {... { editAction: handleEditEmail, closeDialogAlert}} /> ) }
                sx = {{
                  padding: 2,
                }}
              >
                <BorderColorIcon 
                  sx = {{
                    color: "white",
                    fontSize: 35,
                  }}
                />

              </IconButton>

            </Grid>

          </Grid>

          <Grid
            sx = {{
              mt: 7,
            }}
          >

            <Typography
              component = {'span'} 
              variant = {'body2'}
              sx = {{ 
                color: "white",
                fontWeight: "bold", 
                fontSize: 30,
                mt: 5,
              }}
            >

              Contraseña

            </Typography>

          </Grid>

          <Grid 
            className = "roundedRectangle"
            sx = {{
                backgroundColor: "#5494b3",
                height: "4px",
                width: "55%",
                borderTopLeftRadius: 100,
                borderTopRightRadius: 100,
                borderBottomRightRadius: 100,
                borderBottomLeftRadius: 100,
                mt: 1.5,
            }}
          />

          <Grid
            sx = {{
              mt: 3
            }}
          >

            <Button
              onClick={() => openAlertDialog("¿Deseas cambiar la contraseña?", <EditInfoComponent {... { editAction: sendRecoverCode, closeDialogAlert }} /> )}
              sx = {{
                backgroundColor: "#6b98ae",
                py: 2.5,
                width: "70%"
              }}
            >

              <Typography
                sx = {{
                  color: "white",
                }}
              >

                Cambiar contraseña

              </Typography>

            </Button>

          </Grid>

        </Grid>

        <Grid
          sx = {{
            height: "65vh",
            width: "50%",
            mt: 6.2,
          }}
        >

          <Grid
            container
            justifyContent = "center"
          >

            <input
              type = "file"
              ref = { fileInputRef }
              name = 'picture'
              onChange = { onFileChange }
              style = {{ 
                display: 'none',
              }}
            />

            <Avatar 
              alt = "avatar"
              src = { profilePicture.length > 1 ? `${apiURL}uploads/users/${profilePicture}` : ''}
              sx = {{ 
                width: 320, 
                height: 320 
              }}
            />

            <Grid
              container
              justifyContent = "end"
              sx = {{
                width: "100%",
                height: "10px",
                position: "relative",
                top: "-90px",
                left: { lg: "-20px", xl: "-60px" }
              }}
            >

              <Grid
                container
                alignItems = "center"
                justifyContent = "center"
                sx = {{
                  backgroundColor: "#6b98ae",
                  height: "90px",
                  width: "90px",
                  borderRadius: "50px"
                }}
              >

                <AddPhotoAlternateIcon 
                  onClick = { () => fileInputRef.current.click() }
                  sx = {{
                    fontSize: "55px",
                    color: "#003e65"
                  }}
                />

              </Grid>

            </Grid>

          </Grid>

          <Grid
            container
            justifyContent = "center"
          >

            <Typography
              component = {'span'} 
              variant = {'body2'}
              sx = {{ 
                color: "white",
                fontWeight: "bold", 
                fontSize: 30,
                mt: 4.5,
              }}
            >

              Tipo de subscripción

            </Typography>

          </Grid>

          <Grid
            container
            justifyContent = "center"
          >

            <Grid 
              className = "roundedRectangle"
              sx = {{
                  backgroundColor: "#5494b3",
                  height: "4px",
                  width: "65%",
                  borderTopLeftRadius: 100,
                  borderTopRightRadius: 100,
                  borderBottomRightRadius: 100,
                  borderBottomLeftRadius: 100,
                  mt: 1.5,
                  pl: 5,
                  pr: 5,
              }}
            />

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              pt: 3,
              width: "100%",
            }}
          >

            <Grid>

              <input
                className = "profileInputsShort"
                placeholder = "Gratuita"
                disabled
              />

            </Grid>

          </Grid>

        </Grid>

      </Grid>

      {/* Portrait mobile UI */}

      <Grid
        display = {{ sm: "none" }}
        sx = {{
          height: "85vh",
          width: "80vw",
          mt: 25,
          mb: 10,
        }}
      >

        <Grid
          container
          justifyContent = "center"
        >

          <input
            type = "file"
            ref = { fileInputRef }
            name = 'picture'
            onChange = { onFileChange }
            style = {{ 
              display: 'none',
            }}
          />

          <Avatar 
            alt = "avatar"
            src = { profilePicture.length > 1 ? `${apiURL}uploads/users/${profilePicture}` : ''}
            sx = {{ 
              width: 200, 
              height: 200 
            }}
          />

            <Grid
              container
              justifyContent = "end"
              sx = {{
                width: "100%",
                height: "10px",
                position: "relative",
                top: "-80px",
                left: "-65px"
              }}
            >

              <Grid
                container
                alignItems = "center"
                justifyContent = "center"
                sx = {{
                  backgroundColor: "#6b98ae",
                  height: "90px",
                  width: "90px",
                  borderRadius: "50px"
                }}
              >

                <AddPhotoAlternateIcon 
                  onClick = { () => fileInputRef.current.click() }
                  sx = {{
                    fontSize: "55px",
                    color: "#003e65"
                  }}
                />

              </Grid>

            </Grid>

          </Grid>

        <Grid
          sx = {{
            mt: 5,
          }}
        >

          <Typography
            component = {'span'} 
            variant = {'body2'}
            sx = {{ 
              color: "white",
              fontWeight: "bold", 
              fontSize: 25,
            }}
          >

            Nombre

          </Typography>

        </Grid>

        <Grid
          container
          sx = {{
            mt: 1,
          }}
        >

          <Grid
            sx = {{
              width: "100%",
            }}
          >

            <Grid 
              className = "roundedRectangle"
              sx = {{
                  backgroundColor: "#5494b3",
                  height: "4px",
                  width: "100%",
                  borderTopLeftRadius: 100,
                  borderTopRightRadius: 100,
                  borderBottomRightRadius: 100,
                  borderBottomLeftRadius: 100,
                  mt: 1.5,
                  pl: 5,
                  pr: 5,
              }}
            />

          </Grid>

          <Grid
            display = "flex"
            sx = {{
              mt: 3,
            }}
          >

            <Grid>

              <input
                className = "profileInputResponsive"
                type = "text"
                placeholder = { myName }
                name = "displayName"
                value = { displayName }
                onChange = { onInputChange }
              />

            </Grid>

            <Grid>
            
              <IconButton
                variant = 'contained' 
                value = 'editName'
                onClick = { () => openAlertDialog('¿Seguro que quieres cambiar tu nombre público?', <EditInfoComponent {... {editAction: handleEditName, closeDialogAlert } } /> ) }
                sx = {{
                  padding: 2,
                }}
              >

                <BorderColorIcon
                  sx = {{
                    color: "white",
                    fontSize: 35,
                  }}
                />

              </IconButton>
            
            </Grid>

          </Grid>

        </Grid>

        <Grid
          sx = {{
            mt: 5,
          }}
        >

          <Typography
            component = {'span'} 
            variant = {'body2'}
            sx = {{ 
              color: "white",
              fontWeight: "bold", 
              fontSize: 25,
            }}
          >

            Correo

          </Typography>

        </Grid>

        <Grid
          container
          sx = {{
            mt: 1,
          }}
        >

          <Grid
            sx = {{
              width: "100%",
            }}
          >

            <Grid 
              className = "roundedRectangle"
              sx = {{
                  backgroundColor: "#5494b3",
                  height: "4px",
                  width: "100%",
                  borderTopLeftRadius: 100,
                  borderTopRightRadius: 100,
                  borderBottomRightRadius: 100,
                  borderBottomLeftRadius: 100,
                  mt: 1.5,
                  pl: 5,
                  pr: 5,
              }}
            />

          </Grid>

          <Grid
            display = "flex"
            sx = {{
              mt: 3,
            }}
          >

            <Grid>

              <input
                className = "profileInputResponsive"
                type = "text"
                placeholder = { myEmail }
                name= "email"
                value = { email }
                onChange = { onInputChange }
              />

            </Grid>

            <Grid>

              <IconButton
                variant = "contained" 
                value = "editEmail"
                onClick = { () => openAlertDialog('¿Seguro que quieres cambiar el correo de tu cuenta?', <EditInfoComponent {... { editAction: handleEditEmail, closeDialogAlert }} /> ) }
                sx = {{
                  padding: 2,
                }}
              >
                <BorderColorIcon 
                  sx = {{
                    color: "white",
                    fontSize: 35,
                  }}
                />

              </IconButton>

            </Grid>

          </Grid>

        </Grid>

        <Grid
          sx = {{
            mt: 5,
          }}
        >

          <Typography
            component = {'span'} 
            variant = {'body2'}
            sx = {{ 
              color: "white",
              fontWeight: "bold", 
              fontSize: 25,
            }}
          >

            Contraseña

          </Typography>

        </Grid>

        <Grid
          container
          sx = {{
            mt: 1,
          }}
        >

          <Grid
            sx = {{
              width: "100%",
            }}
          >

            <Grid 
              className = "roundedRectangle"
              sx = {{
                  backgroundColor: "#5494b3",
                  height: "4px",
                  width: "100%",
                  borderTopLeftRadius: 100,
                  borderTopRightRadius: 100,
                  borderBottomRightRadius: 100,
                  borderBottomLeftRadius: 100,
                  mt: 1.5,
                  pl: 5,
                  pr: 5,
              }}
            />

          </Grid>

          <Grid
            sx = {{
              mt: 3,
              width: "80%"
            }}
          >

            <Button
              sx = {{
                backgroundColor: "#6b98ae",
                py: 2.5,
                width: "100%"
              }}
            >

              <Typography
                sx = {{
                  color: "white",
                }}
              >

                Cambiar contraseña

              </Typography>

            </Button>

          </Grid>

        </Grid>

        <Grid
          sx = {{
            mt: 5,
          }}
        >

          <Typography
            component = {'span'} 
            variant = {'body2'}
            sx = {{ 
              color: "white",
              fontWeight: "bold", 
              fontSize: 25,
            }}
          >

            Tipo de subscripción

          </Typography>

        </Grid>

        <Grid
          container
          sx = {{
            mt: 1,
          }}
        >

          <Grid
            sx = {{
              width: "100%",
            }}
          >

            <Grid 
              className = "roundedRectangle"
              sx = {{
                  backgroundColor: "#5494b3",
                  height: "4px",
                  width: "100%",
                  borderTopLeftRadius: 100,
                  borderTopRightRadius: 100,
                  borderBottomRightRadius: 100,
                  borderBottomLeftRadius: 100,
                  mt: 1.5,
                  pl: 5,
                  pr: 5,
              }}
            />

          </Grid>

          <Grid
            sx = {{
              mt: 3,
            }}
          >

            <input
              className = "profileInputResponsive"
              placeholder = "Gratuita"
              disabled
            />

          </Grid>

        </Grid>

        <Grid
          sx = {{
            pb: 10,
          }}
        />

      </Grid>

      {/* Landscape mobile UI  */}

      <Grid
        display = {{ xs: "none", sm: "flex", md: "none" }}
        sx = {{
          width: "80%",
          height: "465px",
          mt: 25,
        }}
      >

        <Grid
          sx = {{
            width: "50%",
            mt: 3,
          }}
        >

          <Grid>

            <Typography
              component = {'span'} 
              variant = {'body2'}
              sx = {{ 
                color: "white",
                fontWeight: "bold", 
                fontSize: 20,
                mt: 5,
              }}
            >

              Nombre

            </Typography>

          </Grid>

          <Grid>

            <Grid 
              className = "roundedRectangle"
              sx = {{
                backgroundColor: "#5494b3",
                height: "4px",
                width: "80%",
                borderTopLeftRadius: 100,
                borderTopRightRadius: 100,
                borderBottomRightRadius: 100,
                borderBottomLeftRadius: 100,
                mt: 1.5,
              }}
            />

          </Grid>

          <Grid
            container
            sx = {{
              mt: 2
            }}
          >

            <input
              className = "profileInputResponsiveShortLandscape"
              type = "text"
              placeholder = { myName }
              name = "displayName"
              value = { displayName }
              onChange = { onInputChange }
            />

            <Grid>
              
              <IconButton
                variant = 'contained' 
                value = 'editName'
                onClick = { () => openAlertDialog('¿Seguro que quieres cambiar tu nombre público?', <EditInfoComponent {... {editAction: handleEditName, closeDialogAlert } } /> ) }
                sx = {{
                  padding: 2,
                }}
              >

                <BorderColorIcon
                  sx = {{
                    color: "white",
                    fontSize: 35,
                  }}
                />

              </IconButton>

              { newAlert() }
            
            </Grid>

          </Grid>

          <Grid
            sx = {{
              mt: 3,
            }}
          >

            <Typography
              component = {'span'} 
              variant = {'body2'}
              sx = {{ 
                color: "white",
                fontWeight: "bold", 
                fontSize: 20,
                mt: 5,
              }}
            >

              Correo

            </Typography>

          </Grid>

          <Grid
            sx = {{
              mt: 1,
            }}
          >

            <Grid 
              className = "roundedRectangle"
              sx = {{
                  backgroundColor: "#5494b3",
                  height: "4px",
                  width: "80%",
                  borderTopLeftRadius: 100,
                  borderTopRightRadius: 100,
                  borderBottomRightRadius: 100,
                  borderBottomLeftRadius: 100,
                  mt: 1.5,
              }}
            />

          </Grid>

          <Grid
            container
            sx = {{
              mt: 2
            }}
          >

            <input
              className = "profileInputResponsiveShortLandscape"
              type = "text"
              placeholder = { myEmail }
              name= "email"
              value = { email }
              onChange = { onInputChange }
            />

            <Grid>

              <IconButton
                variant = "contained" 
                value = "editEmail"
                onClick = { () => openAlertDialog('¿Seguro que quieres cambiar el correo de tu cuenta?', <EditInfoComponent {... { editAction: handleEditEmail, closeDialogAlert }} /> ) }
                sx = {{
                  padding: 2,
                }}
              >
                <BorderColorIcon 
                  sx = {{
                    color: "white",
                    fontSize: 35,
                  }}
                />

              </IconButton>

            </Grid>

          </Grid>

          <Grid
            sx = {{
              mt: 3,
            }}
          >

            <Typography
              component = {'span'} 
              variant = {'body2'}
              sx = {{ 
                color: "white",
                fontWeight: "bold", 
                fontSize: 20,
                mt: 5,
              }}
            >

              Contraseña

            </Typography>

          </Grid>

          <Grid
            sx = {{
              mt: 1,
            }}
          >

            <Grid 
              className = "roundedRectangle"
              sx = {{
                  backgroundColor: "#5494b3",
                  height: "4px",
                  width: "80%",
                  borderTopLeftRadius: 100,
                  borderTopRightRadius: 100,
                  borderBottomRightRadius: 100,
                  borderBottomLeftRadius: 100,
                  mt: 1.5,
              }}
            />

          </Grid>

          <Grid
            sx = {{
              mt: 3,
              width: "80%"
            }}
          >

            <Button
              sx = {{
                backgroundColor: "#6b98ae",
                py: 2.5,
                width: "100%"
              }}
            >

              <Typography
                sx = {{
                  color: "white",
                }}
              >

                Cambiar contraseña

              </Typography>

            </Button>

          </Grid>

        </Grid>

        <Grid
          container
          alignItems = "center"
          justifyContent = "center"
          direction = "column"
          sx = {{
            width: "50%",
          }}
        >

          <Grid>

            <input
              type = "file"
              ref = { fileInputRef }
              name = 'picture'
              onChange = { onFileChange }
              style = {{ 
                display: 'none',
              }}
            />

            <Avatar 
              alt = "avatar"
              src = { profilePicture.length > 1 ? `${apiURL}uploads/users/${profilePicture}` : ''}
              sx = {{ 
                width: 250, 
                height: 250 
              }}
            />

            <Grid
              container
              justifyContent = "end"
              sx = {{
                width: "100%",
                height: "10px",
                position: "relative",
                top: "-80px",
              }}
            >

              <Grid
                container
                alignItems = "center"
                justifyContent = "center"
                sx = {{
                  backgroundColor: "#6b98ae",
                  height: "90px",
                  width: "90px",
                  borderRadius: "50px"
                }}
              >

                <AddPhotoAlternateIcon 
                  onClick = { () => fileInputRef.current.click() }
                  sx = {{
                    fontSize: "55px",
                    color: "#003e65"
                  }}
                />

              </Grid>

            </Grid>

          </Grid>

          <Grid
            container
            justifyContent = "center"
            sx = {{
              mt: 6
            }}
          >

            <Grid>

              <Typography
                component = {'span'} 
                variant = {'body2'}
                sx = {{ 
                  color: "white",
                  fontWeight: "bold", 
                  fontSize: 20,
                }}
              >

                Tipo de subscripción

              </Typography>

            </Grid>

            <Grid
              container
              justifyContent = "center"
              sx = {{
                mt: 6,
              }}
            >

              <input
                className = "profileInputResponsiveShortLandscape"
                placeholder = "Gratuita"
                disabled
              />

            </Grid>

          </Grid>

        </Grid>

      </Grid>
      
      { newToast() }

    </>

  );

}