import { Avatar, Button, Grid, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { apiURL } from "../../helpers";
import { useCustomAlerts } from "../../hooks/useCustomAlerts";
import { useUserData } from "../../hooks/useUserData";
import { useDeleteGiftMutation, usePostReserveGiftMutation, usePostUnreserveGiftMutation } from "../../store/api/giftsApi";
import { reserveGiftReducer, resetGiftData, unreserveGiftReducer } from "../../store/gifts/giftsSlice";
import { DeleteGiftPopup } from "../components/user-gifts/delete-gift-popup";
import { ReserveGiftPopup } from "../components/user-gifts/reserve-gift-popup";
import { UnreserveGiftPopup } from "../components/user-gifts/unreserve-gift-popup";

export const ViewGift = () => {

    const {id: giftID, name, description, img, reserved, owner, communityID } = useSelector( state => state.gifts );
    const { myID } = useUserData();
    const [ deleteGift ] = useDeleteGiftMutation ();
    const [ reserveGift ] = usePostReserveGiftMutation ();
    const [ unreserveGift ] = usePostUnreserveGiftMutation ();
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const {
        openAlertDialog,
        closeDialogAlert,
        newAlert,
    } = useCustomAlerts();

    
    const deleteGiftByID = async() => {
        try {
            const { data } = await deleteGift({ giftID });
            if(data.code === 200) { 
                dispatch( resetGiftData() );
                navigate('/gifts/mygifts');
            }
        } catch (error) {
            // console.log(error);
        }
    }
    
    const reserveGiftByID = async () => {
        try {
            const { data, error } = await reserveGift({ giftID,  communityID });
            if(data.code === 200) { 
                dispatch( reserveGiftReducer({userID: 2}) );
                closeDialogAlert();
            }
        } catch (error) {
            // console.log(error);
        }
    }
    
    const unreserveGiftByID = async() => {
        try {
            const { data, error } = await unreserveGift({ giftID });
            if(data.code === 200) { 
                dispatch( unreserveGiftReducer() );
                closeDialogAlert();
            }
        } catch (error) {
            // console.log(error);
        }
    }

    useEffect(() => {
        if(name == '')
            navigate('/gifts/mygifts');
    }, [])

    return (

        <>
        
            <Grid
                display = {{ xs: "none", sm: "none", md: "initial" }}
                sx = {{
                    height: "65vh",
                    width: { md: "90vw", lg: "70vw", xl: "65vw" },
                    mt: 20,
                }}
            >

                <Grid>

                    <Grid 
                        className = "roundedRectangle"
                        sx = {{
                            backgroundColor: "#5494b3",
                            width: "100%",
                            height: "20px",
                            borderTopLeftRadius: 100,
                            borderTopRightRadius: 100,
                        }}
                    />

                </Grid>


                <Grid
                    container
                    sx = {{
                        mt: 15,
                        mb: 10,
                    }}
                >

                    <Grid
                        container
                        justifyContent = "center"
                        sx = {{
                            width: "50%"
                        }}
                    >

                        <Avatar
                            onClick={() => img.length > 1 && openAlertDialog("", <>
                                <img
                                    src={`${apiURL}uploads/gifts/${img}`}
                                />
                            </>)}
                            sx = {{
                                height: "300px",
                                width: "300px",
                                borderRadius: "200px",
                                cursor: img.length > 1 ? "pointer" : "default"  
                            }}
                            src = { img.length > 1 ? `${apiURL}uploads/gifts/${img}` : "/imgs/BUR-gift-logo.png"}
                        />

                    </Grid>

                    <Grid
                        sx = {{
                            width: "50%",
                        }}
                    >

                        <Grid
                            sx = {{
                                mb: 2,
                            }}
                        >

                            <Typography
                                component = {'span'} 
                                variant = {'body2'}
                                sx = {{ 
                                    color: "white",
                                    fontWeight: "bold", 
                                    fontSize: 35,
                                }}
                            >   

                                {name}
                                
                            </Typography>

                        </Grid>

                        <Grid>

                            <Grid 
                                className = "roundedRectangle"
                                sx = {{
                                    backgroundColor: "#5494b3",
                                    width: "70%",
                                    height: "5px",
                                    borderBottomLeftRadius: 100,
                                    borderBottomRightRadius: 100,
                                    mb: 3,
                                }}
                            />

                        </Grid>

                        <Grid
                            sx = {{
                                mb: 5,
                            }}
                        >

                            <Typography
                                component = {'span'} 
                                variant = {'body2'}
                                sx = {{ 
                                    color: "white",
                                    fontWeight: "bold", 
                                    fontSize: 25,
                                }}
                            >

                                {description} 
                                
                            </Typography>

                        </Grid>
                        
                        { owner !== "yes" && myID > 0 && reserved != 1 && (
                            <Grid
                                sx = {{
                                    mb: 2,
                                }}
                            >

                                <Button
                                    onClick = { () => (reserved == 2) ? openAlertDialog("¿Seguro ya no quieres tener apartado este regalo?", <UnreserveGiftPopup {...{closeDialogAlert, unreserveGiftByID}} />) : openAlertDialog("¿Seguro que quieres apartar este regalo?", <ReserveGiftPopup {...{closeDialogAlert, reserveGiftByID}} />)}
                                    sx = {{
                                        backgroundColor: reserved == 2 ? "#ba3232" : "#5494b3",
                                        width: "230px",
                                        height: "60px",
                                        ":hover": { backgroundColor: reserved == 2 ? "#ba3232" : "#5494b3" }
                                    }}
                                >
                                    <Typography
                                        component = {'span'} 
                                        variant = {'body2'}
                                        sx = {{ 
                                            color: reserved == 2 ? "#ffffff" : "#004f77",
                                            fontWeight: "bold", 
                                            fontSize: 20,
                                        }}
                                    >

                                        { reserved == 2 ? "Quitar apartado" : "Apartar regalo" }

                                    </Typography>

                                </Button>

                            </Grid>
                        )}

                        { owner === "yes" && (
                            <>
                                <Grid
                                    sx = {{
                                        mb: 2,
                                    }}
                                >

                                <NavLink 
                                    to='/gifts/editgift' 
                                    style = {{ 
                                        textDecoration: "none",
                                        color: "white",
                                    }}
                                >

                                    <Button
                                        sx = {{
                                            backgroundColor: "#6b96ad", 
                                            width: "230px",
                                            height: "60px",
                                            ":hover": { backgroundColor: "#95bbcf" }
                                        }}
                                    >                                        

                                            <Typography
                                                component = {'span'} 
                                                variant = {'body2'}
                                                sx = {{ 
                                                    color: "#004f77",
                                                    fontWeight: "bold", 
                                                    fontSize: 20,
                                                }}
                                            >
                                                                                    
                                                Editar Regalo   

                                            </Typography>

                                    </Button>

                                </NavLink>

                                </Grid>

                                <Grid>

                                    <Button
                                        sx = {{
                                            backgroundColor: "#6b96ad",
                                            width: "230px",
                                            height: "60px",
                                            ":hover": { backgroundColor: "#95bbcf" }
                                        }}
                                        // onClick={ deleteGiftByID }
                                        onClick = { () => openAlertDialog("¿Seguro que quieres eliminar este regalo?", <DeleteGiftPopup {...{closeDialogAlert, deleteGiftByID}} />)}
                                    >

                                        <Typography
                                            component = {'span'} 
                                            variant = {'body2'}
                                            sx = {{ 
                                                color: "#004f77",
                                                fontWeight: "bold", 
                                                fontSize: 20,
                                            }}
                                        >

                                            Eliminar Regalo

                                        </Typography>

                                    </Button>

                                </Grid>
                            </>
                        )}

                        <Grid display={reserved == 1 && owner != 'yes' ? "flex" : "none"}>

                            <Typography
                                sx = {{
                                    color: "orange",
                                    fontSize: 20,
                                }}
                            >
                                
                                Este regalo ya ha sido apartado

                            </Typography>

                        </Grid>

                    </Grid>

                </Grid>


                <Grid>

                    <Grid 
                        className = "roundedRectangle"
                        sx = {{
                            backgroundColor: "#5494b3",
                            width: "100%",
                            height: "20px",
                            borderBottomLeftRadius: 100,
                            borderBottomRightRadius: 100,
                            mt: 10,
                        }}
                    />

                </Grid>

                { newAlert() }

            </Grid>

            {/* Portrait mobile UI */}

            <Grid
                display = {{ xs: "initial", sm: "none" }}
                sx = {{
                    width: "100vw",
                }}
            >

                <Grid
                    sx = {{
                        height: "80vh",
                        mt: 25,
                        mb: 10,
                    }}
                >

                    <Grid
                        container
                        justifyContent = "center"
                        alignItems = "center"
                    >

                        <Grid 
                            className = "roundedRectangle"
                            sx = {{
                                backgroundColor: "#5494b3",
                                width: "90%",
                                height: "10px",
                                borderTopLeftRadius: 100,
                                borderTopRightRadius: 100,
                            }}
                        />

                    </Grid>

                    <Grid
                        container
                        justifyContent = "center"
                        sx = {{
                            mt: 5,
                        }}
                    >

                        <Avatar
                            onClick={ () => img.length > 1 && openAlertDialog("", <>
                                <img
                                    src={`${apiURL}uploads/gifts/${img}`}
                                />
                            </>)}
                            sx = {{
                                height: "250px",
                                width: "250px",
                                borderRadius: "200px",
                                cursor: img.length > 1 ? "pointer" : "default"     
                            }}
                            src = { img.length > 1 ? `${apiURL}uploads/gifts/${img}` : "/imgs/BUR-gift-logo.png"}
                        />

                    </Grid>

                    <Grid
                        container
                        justifyContent = "center"
                        sx = {{
                            mt: 5,
                            pr: 5,
                            pl: 5,
                        }}
                    >

                        <Typography
                            component = {'span'} 
                            variant = {'body2'}
                            sx = {{ 
                                color: "white",
                                fontWeight: "bold", 
                                fontSize: 35,
                                textAlign: "center",
                            }}
                        >   

                            {name}
                            
                        </Typography>

                    </Grid>

                    <Grid
                        container
                        justifyContent = "center"
                        sx = {{
                            mt: 1,
                        }}
                    >

                        <Grid 
                            className = "roundedRectangle"
                            sx = {{
                                backgroundColor: "#5494b3",
                                width: "85%",
                                height: "5px",
                                borderBottomLeftRadius: 100,
                                borderBottomRightRadius: 100,
                                mb: 3,
                            }}
                        />

                    </Grid>

                    <Grid
                        container
                        justifyContent = "center"
                        sx = {{
                            pr: 5,
                            pl: 5,
                        }}
                    >

                        <Typography
                            component = {'span'} 
                            variant = {'body2'}
                            sx = {{ 
                                color: "white",
                                fontWeight: "bold", 
                                fontSize: 25,
                                textAlign: "center",
                            }}
                        >

                            {description} 
                            
                        </Typography>

                    </Grid>

                    <Grid
                        container
                        alignItems = "center"
                        justifyContent = "center"
                        sx = {{
                            mt: 3,
                        }}
                    >

                        { owner !== "yes" && myID > 0 && reserved != 1 && (
                            <Grid
                                sx = {{
                                    mb: 2,
                                }}
                            >

                                <Button
                                    onClick = { () => (reserved == 2) ? openAlertDialog("¿Seguro ya no quieres tener apartado este regalo?", <UnreserveGiftPopup {...{closeDialogAlert, unreserveGiftByID}} />) : openAlertDialog("¿Seguro que quieres apartar este regalo?", <ReserveGiftPopup {...{closeDialogAlert, reserveGiftByID}} />)}
                                    sx = {{
                                        backgroundColor: reserved == 2 ? "#ba3232" : "#5494b3",
                                        width: "230px",
                                        height: "60px",
                                        ":hover": { backgroundColor: reserved == 2 ? "#ba3232" : "#5494b3" }
                                    }}
                                >
                                    <Typography
                                        component = {'span'} 
                                        variant = {'body2'}
                                        sx = {{ 
                                            color: reserved == 2 ? "#ffffff" : "#004f77",
                                            fontWeight: "bold", 
                                            fontSize: 20,
                                        }}
                                    >

                                        { reserved == 2 ? "Quitar apartado" : "Apartar regalo" }

                                    </Typography>

                                </Button>

                            </Grid>
                        )}

                    </Grid>

                    <Grid
                        container
                        justifyContent = "center"
                        direction = "column"
                        alignItems = "center"
                    >

                        { owner === "yes" && (
                            <>
                                <Grid
                                    sx = {{
                                        mb: 2,
                                    }}
                                >

                                    <Button
                                        sx = {{
                                            backgroundColor: "#6b96ad", 
                                            width: "230px",
                                            height: "60px",
                                            ":hover": { backgroundColor: "#95bbcf" }
                                        }}
                                    >
                                        <NavLink 
                                            to='/gifts/editgift' 
                                            style = {{ 
                                                textDecoration: "none",
                                                color: "white",
                                            }}
                                        >

                                            <Typography
                                                component = {'span'} 
                                                variant = {'body2'}
                                                sx = {{ 
                                                    color: "#004f77",
                                                    fontWeight: "bold", 
                                                    fontSize: 20,
                                                }}
                                            >
                                                                                    
                                                Editar Regalo   

                                            </Typography>

                                        </NavLink>

                                    </Button>

                                </Grid>

                                <Grid>

                                    <Button
                                        sx = {{
                                            backgroundColor: "#6b96ad",
                                            width: "230px",
                                            height: "60px",
                                            ":hover": { backgroundColor: "#95bbcf" }
                                        }}
                                        // onClick={ deleteGiftByID }
                                        onClick = { () => openAlertDialog("¿Seguro que quieres eliminar este regalo?", <DeleteGiftPopup {...{closeDialogAlert, deleteGiftByID}} />)}
                                    >

                                        <Typography
                                            component = {'span'} 
                                            variant = {'body2'}
                                            sx = {{ 
                                                color: "#004f77",
                                                fontWeight: "bold", 
                                                fontSize: 20,
                                            }}
                                        >

                                            Eliminar Regalo

                                        </Typography>

                                    </Button>

                                </Grid>
                            </>
                        )}

                    </Grid>

                    <Grid
                        container
                        justifyContent = "center"
                        sx = {{
                            mt: 5,
                            pb: 10
                        }}
                    >

                        <Grid 
                            className = "roundedRectangle"
                            sx = {{
                                backgroundColor: "#5494b3",
                                width: "90%",
                                height: "10px",
                                borderBottomLeftRadius: 100,
                                borderBottomRightRadius: 100,
                            }}
                        />

                    </Grid>

                </Grid>

            </Grid>

            {/* Landscape mobile UI */}

            <Grid
                display = {{ xs: "none", sm: "initial", md: "none" }}
                sx = {{
                    width: "100vw",
                }}
            >

                <Grid
                    sx = {{
                        mt: 25,
                        mb: 10,
                    }}
                >

                    <Grid
                        container
                        justifyContent = "center"
                        sx = {{
                            mb: 8
                        }}
                    >

                        <Grid 
                            className = "roundedRectangle"
                            sx = {{
                                backgroundColor: "#5494b3",
                                width: "90%",
                                height: "10px",
                                borderTopLeftRadius: 100,
                                borderTopRightRadius: 100,
                            }}
                        />

                    </Grid>

                    <Grid
                        container
                    >

                        <Grid
                            container
                            justifyContent = "center"
                            alignItems = "center"
                            sx = {{
                                width: "50%",
                            }}
                        >

                            <Avatar
                                onClick={() => img.length > 1 && openAlertDialog("", <>
                                    <img
                                        src={`${apiURL}uploads/gifts/${img}`}
                                    />
                                </>)}
                                sx = {{
                                    height: "270px",
                                    width: "270px",
                                    borderRadius: "200px",
                                    cursor: img.length > 1 ? "pointer" : "default"  
                                }}
                                src = { img.length > 1 ? `${apiURL}uploads/gifts/${img}` : "/imgs/BUR-gift-logo.png"}
                            />

                        </Grid>

                        <Grid
                            container
                            direction = "column"
                            justifyContent = "center"
                            sx = {{
                                width: "50%",
                               
                                pr: 5,
                            }}
                        >

                            <Grid>

                                <Typography
                                    component = {'span'} 
                                    variant = {'body2'}
                                    sx = {{ 
                                        color: "white",
                                        fontWeight: "bold", 
                                        fontSize: 35,
                                        textAlign: "center",
                                    }}
                                >   

                                    { name }
                                
                                </Typography>

                            </Grid>

                            <Grid
                                sx = {{
                                    mt: 2,
                                }}
                            >

                                <Grid 
                                    className = "roundedRectangle"
                                    sx = {{
                                        backgroundColor: "#5494b3",
                                        width: "85%",
                                        height: "5px",
                                        borderBottomLeftRadius: 100,
                                        borderBottomRightRadius: 100,
                                        mb: 3,
                                    }}
                                />

                            </Grid>

                            <Grid>

                                <Typography
                                    component = {'span'} 
                                    variant = {'body2'}
                                    sx = {{ 
                                        color: "white",
                                        fontWeight: "bold", 
                                        fontSize: 25,
                                        textAlign: "center",
                                    }}
                                >

                                    { description } 
                                
                                </Typography>

                            </Grid>

                            <Grid
                                container
                                sx = {{
                                    mt: 3,
                                }}
                            >

                                { owner !== "yes" && myID > 0 && reserved != 1 && (
                                    <Grid
                                        sx = {{
                                            mb: 2,
                                        }}
                                    >

                                        <Button
                                            onClick = { () => (reserved == 2) ? openAlertDialog("¿Seguro ya no quieres tener apartado este regalo?", <UnreserveGiftPopup {...{closeDialogAlert, unreserveGiftByID}} />) : openAlertDialog("¿Seguro que quieres apartar este regalo?", <ReserveGiftPopup {...{closeDialogAlert, reserveGiftByID}} />)}
                                            sx = {{
                                                backgroundColor: reserved == 2 ? "#ba3232" : "#5494b3",
                                                width: "230px",
                                                height: "60px",
                                                ":hover": { backgroundColor: reserved == 2 ? "#ba3232" : "#5494b3" }
                                            }}
                                        >
                                            <Typography
                                                component = {'span'} 
                                                variant = {'body2'}
                                                sx = {{ 
                                                    color: reserved == 2 ? "#ffffff" : "#004f77",
                                                    fontWeight: "bold", 
                                                    fontSize: 20,
                                                }}
                                            >

                                                { reserved == 2 ? "Quitar apartado" : "Apartar regalo" }

                                            </Typography>

                                        </Button>

                                    </Grid>
                                )}

                                { owner === "yes" && (
                                    <>
                                        <Grid
                                            sx = {{
                                                mb: 2,
                                            }}
                                        >

                                            <Button
                                                sx = {{
                                                    backgroundColor: "#6b96ad", 
                                                    width: "230px",
                                                    height: "60px",
                                                    ":hover": { backgroundColor: "#95bbcf" }
                                                }}
                                            >
                                                <NavLink 
                                                    to='/gifts/editgift' 
                                                    style = {{ 
                                                        textDecoration: "none",
                                                        color: "white",
                                                    }}
                                                >

                                                    <Typography
                                                        component = {'span'} 
                                                        variant = {'body2'}
                                                        sx = {{ 
                                                            color: "#004f77",
                                                            fontWeight: "bold", 
                                                            fontSize: 20,
                                                        }}
                                                    >
                                                                                            
                                                        Editar Regalo   

                                                    </Typography>

                                                </NavLink>

                                            </Button>

                                        </Grid>

                                        <Grid
                                            sx = {{
                                                mb: 2,
                                            }}
                                        >

                                            <Button
                                                sx = {{
                                                    backgroundColor: "#6b96ad",
                                                    width: "230px",
                                                    height: "60px",
                                                    ":hover": { backgroundColor: "#95bbcf" }
                                                }}
                                                // onClick={ deleteGiftByID }
                                                onClick = { () => openAlertDialog("¿Seguro que quieres eliminar este regalo?", <DeleteGiftPopup {...{closeDialogAlert, deleteGiftByID}} />)}
                                            >

                                                <Typography
                                                    component = {'span'} 
                                                    variant = {'body2'}
                                                    sx = {{ 
                                                        color: "#004f77",
                                                        fontWeight: "bold", 
                                                        fontSize: 20,
                                                    }}
                                                >

                                                    Eliminar Regalo

                                                </Typography>

                                            </Button>

                                        </Grid>
                                    </>
                                )}

                            </Grid>

                        </Grid>

                    </Grid>

                    <Grid
                        container
                        justifyContent = "center"
                        sx = {{
                            pb: 5,
                            mt: 8
                        }}
                    >

                        <Grid 
                            className = "roundedRectangle"
                            sx = {{
                                backgroundColor: "#5494b3",
                                width: "90%",
                                height: "10px",
                                borderBottomLeftRadius: 100,
                                borderBottomRightRadius: 100,
                            }}
                        />

                    </Grid>

                </Grid>

            </Grid>
        
        </>
        
    );
}