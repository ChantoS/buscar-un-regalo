import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate, Route, Routes, useLocation, useNavigate, useParams } from "react-router-dom";
import { AddGift, EditGift, GroupsPage, HomePage, LoadingPage, Login, ProfilePage, Register, ReserveGifts, ResetPasswordPage, ViewGift } from "../bur/pages/";
import { GroupsPageJoin } from "../bur/pages/groups-page-join";
import { UserGifts } from "../bur/pages/user-gifts";
import { useAuthStore } from "../hooks/useAuthStore";
import { useGetTokenQuery } from "../store/api/giftsApi";
import { onLogin, onLogout } from "../store/auth/authSlice";

export const AppRouter = () => {
  
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { pathname, search } = useLocation();
    const { status } = useAuthStore();
    const { data, isLoading } =  useGetTokenQuery();
    
    const { id:myID } = useSelector( state => state.auth );
    const setLastPath = pathname + search;  
    const storePath = localStorage.getItem('lastPath');  
    const { giftUserID } = useParams();

    useEffect(() => { 
        
        if(storePath === null) {
            localStorage.setItem('lastPath', setLastPath);
        }

        // See if we are already logged in
        if ( data && data.code ) {
            if (data.code === 200) {
                try {
                    const token = localStorage.getItem('token');  
                    if ( !token ) 
                    return dispatch( onLogout() );

                    const { display_name, email, id, img, sub, user } = data.result;

                    dispatch( onLogin({ display_name, email, id, img, sub, user }));

                    const navTo = localStorage.getItem('lastPath');
                    localStorage.removeItem('lastPath');
                    navigate( navTo, {
                        replace:true
                    });
                } catch (error) {
                }
            } else {
                localStorage.clear();
                dispatch(onLogout());
            }
        }
        
    }, [data]);

    // Auth user
    if(status === 'authenticated') {
        return (
            <Routes>
                <Route path="/" element={ <HomePage /> } />
                <Route path="/profile" element={ <ProfilePage /> } />
                <Route path="/gifts/add" element={ <AddGift /> } />
                <Route path="/gifts/mygifts" element={ <UserGifts id={myID} /> } />
                <Route path="/gifts/user/:giftUserID" element={ <UserGifts id={-1} /> } />
                <Route path="/gifts/view/" element={ <ViewGift /> } />
                <Route path="/gifts/editgift" element={ <EditGift /> } />
                <Route path="/groups/mygroup" element={ <GroupsPage /> } />
                <Route path="/groups/join/:groupID" element={ <GroupsPageJoin /> } />
                <Route path="/groups/reservegift" element={ <ReserveGifts /> } />
                <Route path="/resetPassword/:resetCode" element={ <ResetPasswordPage /> } />
                <Route path="/*" element={ <Navigate to='/' /> } />
            </Routes>
        )
    }

    // Not auth user
    else {
        return (
            <Routes>
                { isLoading 
                    ? <Route path="/" element={ <LoadingPage /> }/>
                    : <Route path="/" element={ <HomePage /> }/>
                }      
                <Route path="/gifts/user/:giftUserID" element={ <UserGifts id={-1} /> } />
                <Route path="/gifts/view/" element={ <ViewGift /> } />
                <Route path="/auth/login" element={ <Login /> }/>
                <Route path="/auth/register" element={ <Register /> }/>
                <Route path="/resetPassword/:resetCode" element={ <ResetPasswordPage /> } />
                <Route path="/*" element={ <Navigate to='/' /> }/>
            </Routes>
        )
    }
    
}
