import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Navigate, Route, Routes, useLocation, useNavigate } from 'react-router-dom'
import { HomePage, Register, Login } from '../bur/pages'
import { LoadingPage } from '../bur/pages/loading-page'
import { useAuthStore } from '../hooks/useAuthStore'
import { useGetTokenQuery } from '../store/api/giftsApi'

export const NotAuthRoutes = () => {
  
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { pathname, search } = useLocation();
  const { status } = useAuthStore();
  const { data, isLoading } =  useGetTokenQuery();

  const setLastPath = pathname + search;  
  const storePath = localStorage.getItem('lastPath');  

  useEffect(() => { 
        
    if(storePath === null) {
        localStorage.setItem('lastPath', setLastPath);
        console.log(setLastPath);
    }

    // See if we are already logged in
    if ( data && data.code ) {
        if (data.code === 200) {
            try {
                const token = localStorage.getItem('token');  
                if ( !token ) 
                  return dispatch( onLogout() );

                const { display_name, email, id, img, sub, user } = data.result;

                dispatch( onLogin({ display_name, email, id, img, sub, user }));

                const navTo = localStorage.getItem('lastPath');
                localStorage.removeItem('lastPath');
                navigate( navTo, {
                    replace:true
                });
            } catch (error) {
                console.log(error)
            }
        } else {
            localStorage.clear();
            console.log('Problema con el token');
            dispatch(onLogout());
        }
    }
    
}, [data]);

  return (

    <Routes>
      { isLoading 
        ? <Route path="/" element={ <LoadingPage /> }/>
        : <Route path="/" element={ <HomePage /> }/>
      }      
      <Route path="/auth/login" element={ <Login /> }/>
      <Route path="/auth/register" element={ <Register /> }/>z
      <Route path="/*" element={ <Navigate to='/' /> }/>
    </Routes>

  );

}
