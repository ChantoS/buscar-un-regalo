import { Provider } from "react-redux"
import { AppTheme } from "./theme"
import { AuthLayout } from "./auth/components/AuthLayout";

import { store } from "./store/store"
import { GlobalNavbar } from "./bur/components/navbar/global-navbar-ui";
import { HomePageFloatingButton } from "./bur/components";
import { AppRouter } from "./router/AppRouter";

export const BURApp = () => {

  return (
    
    <Provider store = { store }>
      <AppTheme>
        <AuthLayout>
          <GlobalNavbar />
          <HomePageFloatingButton />
          <AppRouter />
        </AuthLayout>
      </AppTheme>
    </Provider>

  )
}
