import { createTheme } from "@mui/material";
import { red } from "@mui/material/colors";

export const darkTheme = createTheme({
    palette: {
        primary: {
            main: '#342628'
        },
        secondary: {
            main: '#3d5a5b'
        },
        error: {
            main: red.A400
        }
    },
    typography: {
        fontFamily: [
          'Fredoka One',
          'cursive',
        ].join(','),
    },
})