import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import { useGetCommunitiesQuery } from "../store/api/giftsApi";
import { setCommunities } from "../store/auth/authSlice";

export const useUserData = () => {

    const { communities, displayName, id, img, email } = useSelector( state => state.auth );
    const { data, isLoading, error } = useGetCommunitiesQuery();
    const dispatch = useDispatch();
        
    useEffect(() => {
        if(data && data.code && data.code === 200) {
            dispatch(setCommunities({ communities: data.rows }));
        }
    }, [data]);

  return {
    //* Props
    communities,
    myName: displayName,
    myID: id,
    profilePicture: img,
    myEmail: email,
    communitiesLoading: isLoading
  }
}
