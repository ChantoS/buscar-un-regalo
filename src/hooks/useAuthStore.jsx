import { useDispatch, useSelector } from "react-redux"
import { EmailVerificationUI } from "../bur/components/register/email-verification-ui";
import { clearErrorMessage, onChecking, onLogin, onLogout } from "../store/auth/authSlice";
import { useCustomAlerts } from "./useCustomAlerts";

export const useAuthStore = () => {

    const {status, username, password, errorMessage} = useSelector( state => state.auth );
    const dispatch = useDispatch();

    const startLogin = async(login, username, password, submitCode, openToast, openAlertDialog, closeDialogAlert, resetLoading ) => {
      dispatch( onChecking());
    
      try {
        const { data, error } = await login({ username, password });

        resetLoading();

        if(data && data.code == 200) {
          const [{ display_name, email, id, img, sub, username: user }] = data.result;
          localStorage.setItem( 'token', data.token );
          dispatch( onLogin({ display_name, email, id, img, sub, user }));
        }
        if(error) {
          console.log(error);
          dispatch( onLogout(error.data));
          
          if(error.data && error.data.returnCode == 1) {
            openAlertDialog("Confirme su correo", <EmailVerificationUI {...{user: username, submitCode, closeDialogAlert }} />);
          }
          else {
            openToast("El usuario y/o contraseña son incorrectos.", "error");
            // openToast(error.status, "error");
          }
        }
    
      } catch (error) { 
        console.log(error);
        openToast(error, 'error');
        dispatch( onLogout('Datos incorrectos'));
        setTimeout(() => {
          dispatch( clearErrorMessage() );
        }, 10);
      }
    }


  return {

    //* Props
    status,
    username,
    password,
    errorMessage,

    //* Methods
    startLogin,
    //checkAuthToken,

  }
}
