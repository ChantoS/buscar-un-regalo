import { useRef } from "react";
import { useState } from "react";
import { AlertDialog } from "../bur/components/global/alert-dialog";
import { ToastAlert } from "../bur/components/global/toast-alert";

export const useCustomAlerts = () => {

    // const [ loadingBtn, setLoadingBtn ] = useState(false);
    const loadingBtn = useRef(false);

    const [ toastInfo, setToastInfo ] = useState({ 
        message: '',
        toastType: 'info',
        timeToHide: 6000
    })

    const [alertInfo, setAlertInfo] = useState({ 
        title: '',
        content: ''
    })

    const [ toastOpened, setToastOpened] = useState(false);
    const [ alertDialog, setAlertDialog] = useState(false);

    const enableLoadAction = (isEnable) => {
        console.log("Is false? ", isEnable);
        // setLoadingBtn(isEnable);
        loadingBtn.current = isEnable;
    }

    const openAlertDialog = (title, content) => {
        setAlertInfo({
            title,
            content
        });

        setAlertDialog(true);
    }
    
    const openToast = (message, toastType = 'info', timeToHide = 6000 ) => {
        setToastInfo({
            message,
            toastType,
            timeToHide
        });

        setToastOpened(true);
    }

    const closeToast = () => {
        setToastInfo({
            message: '',
            toastType: 'info',
            timeToHide: 6000
        });

        setToastOpened(false);
    }

    const closeDialogAlert = () => {
        setAlertInfo({
            title: '',
            content: ''
        });

        setAlertDialog(false);
    }

    const newAlert = () => {
        return (
            <AlertDialog
                title = { alertInfo.title }
                open = { alertDialog }
                content = { alertInfo.content }
                closeAction = { closeDialogAlert }
            />
        );
    }

    const newToast = () => {
        return (
            <ToastAlert 
                message = { toastInfo.message }
                open = { toastOpened }
                timeToHide = { toastInfo.imeToHide }
                closeToast = { closeToast }
                toastType = { toastInfo.toastType }
            />
        );
    }

    return {
        loadingBtn, 
        
        openAlertDialog,
        openToast, 
        closeDialogAlert,
        newAlert,
        newToast,
        enableLoadAction
    }
}
