-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 104.248.15.205    Database: burdb
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `community`
--

DROP TABLE IF EXISTS `community`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `community` (
  `id` int NOT NULL AUTO_INCREMENT,
  `owner_id` int DEFAULT '0',
  `name` varchar(70) NOT NULL,
  `password` varchar(70) DEFAULT NULL,
  `img` varchar(60) DEFAULT NULL,
  `description` varchar(140) DEFAULT NULL,
  `isprotected` tinyint(1) DEFAULT '0',
  `visible` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community`
--

LOCK TABLES `community` WRITE;
/*!40000 ALTER TABLE `community` DISABLE KEYS */;
INSERT INTO `community` VALUES (1,1,'Navidad Soto Cambronero 2022','$2a$10$nadoOqXIwJMKF3JN.urnyu/9aOCGbfEbojGQLVFigafkv8QtUQvHS','8974299d-376e-47c7-89c1-84d5e359c273.jpeg','Bienvenidos a la legendaria navidad de mana y abuelo',1,1,'2022-10-27 04:23:27'),(2,1,'Prueba1',NULL,'','',0,0,'2022-10-27 05:16:14'),(3,9,'Grupo d',NULL,'','',0,1,'2022-10-27 05:21:30'),(4,9,'Grupo pz','$2a$10$j7XvRje5k6Q4Y1BseRT5eeC0rPOGMnEZtRFnx6req9hHcEtzDeerW','','Grupo de la familia perez zeledon',1,1,'2022-10-27 05:26:36');
/*!40000 ALTER TABLE `community` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `community_users`
--

DROP TABLE IF EXISTS `community_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `community_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `community_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `unique_id` int DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `joined` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`unique_id`),
  KEY `community_id` (`community_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_users`
--

LOCK TABLES `community_users` WRITE;
/*!40000 ALTER TABLE `community_users` DISABLE KEYS */;
INSERT INTO `community_users` VALUES (1,1,1,11,1,'2022-10-27 04:23:27'),(2,2,1,21,0,'2022-10-27 05:16:14'),(3,3,9,39,1,'2022-10-27 05:21:30'),(4,4,9,49,1,'2022-10-27 05:26:36'),(5,1,3,13,1,'2022-10-27 07:13:56'),(6,1,2,12,1,'2022-10-27 07:14:42'),(7,1,5,15,1,'2022-10-27 07:37:30'),(8,1,6,16,1,'2022-10-27 07:39:14');
/*!40000 ALTER TABLE `community_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gifts`
--

DROP TABLE IF EXISTS `gifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gifts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `description` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `img` varchar(60) DEFAULT NULL,
  `reserved` int DEFAULT '0',
  `user_id` int DEFAULT NULL,
  `community_id` int DEFAULT '0',
  `visible` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `community_id` (`community_id`),
  KEY `reserved` (`reserved`),
  FULLTEXT KEY `name` (`name`,`description`)
) ENGINE=MyISAM AUTO_INCREMENT=98 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gifts`
--

LOCK TABLES `gifts` WRITE;
/*!40000 ALTER TABLE `gifts` DISABLE KEYS */;
INSERT INTO `gifts` VALUES (1,'Chocolates Akatsuki','Akatsuki, nubesita','4d17751a-cf93-4904-9ec8-01f055ede1b7.png',1,2,0,1,'2022-10-27 03:56:04'),(2,'Regalos 2','Regalos que no son para allan porque no va a navidad','912a7402-497b-418d-a350-ef0b4d598679.png',0,2,0,1,'2022-10-27 03:57:12'),(3,'Regalo 3','asdasda','',0,2,0,1,'2022-10-27 03:57:33'),(4,'Queso','Amarillo','',2,1,0,1,'2022-10-27 04:07:13'),(5,'Regalo 4 ','asdasdas','',3,2,0,1,'2022-10-27 04:09:56'),(6,'CREMA DE AFECITAR','CREMA DE AFEITAR NIVEA','b37ea8b4-5324-4c99-8ab2-f0313d872888.jpg',0,4,0,0,'2022-10-27 04:25:54'),(7,'CREMA DE AFECITAR','CREMA DE AFEITAR NIVEA','a30f850a-86cd-4cb2-ac45-276e001234ba.jpg',0,4,0,1,'2022-10-27 04:25:55'),(8,'Headset Bluetooth ','','',9,5,0,1,'2022-10-27 04:27:30'),(9,'REPUESTOS DE HOJA DE AFEITAR','PUEDE SER GILLETTE MACK 3 O MACK 3 TURBO O SENSITIVE','ada4dcff-6060-4cce-9c8c-007ce1a7adde.jpg',0,4,0,1,'2022-10-27 04:33:18'),(10,'Ropa deportiva','busos y pantalonetas','059d2c9a-1950-4869-8141-4c4985723944.jpg',0,8,0,1,'2022-10-27 04:33:39'),(11,'Radio con pantalla android ','Un radio con pantalla androoid para el carro ','d7cc59a1-55f3-4463-904e-b8d23c315904.jpg',0,9,0,1,'2022-10-27 05:02:34'),(12,'Olla de carne ','Una rica olla de carne ','15ae2012-1a08-468f-9fd7-ad82fddc94f8.jpg',0,9,0,1,'2022-10-27 05:21:02'),(13,'Camisetas negras o blancas','Pero que sean de el estilo de la foto','f578a6a0-a160-4f19-be2f-e89f361b0099.jpg',0,1,1,1,'2022-10-27 07:23:51'),(14,'Bebe','Coche','',0,9,4,1,'2022-10-27 07:49:04'),(15,'Silla','Para bebe','7a971c40-8b66-47d9-a816-e5e3b03a8856.jpg',0,9,4,1,'2022-10-27 07:51:57'),(16,'Control','Bluetooth ','',0,9,4,1,'2022-10-27 07:52:35'),(17,'Silla','Para bebe','7300074f-041c-4416-974d-a5e7d17b7fff.jpg',0,9,4,0,'2022-10-27 07:52:52'),(18,'Silla','Para bebe','5578f066-4d22-48e8-96f0-662194b3f9c4.jpg',0,9,4,0,'2022-10-27 07:52:55'),(19,'Silla','Para bebe','e7ad31fd-efdd-495f-853e-41d654616855.jpg',0,9,4,0,'2022-10-27 07:52:58'),(20,'Silla','Para bebe','687cd42c-0f4b-421d-a0f2-7070be4d4b56.jpg',0,9,4,0,'2022-10-27 07:52:59'),(21,'Silla','Para bebe','81635156-d087-49d6-8ca2-851316c682f4.jpg',0,9,4,0,'2022-10-27 07:52:59'),(22,'Silla','Para bebe','a8a71272-0b3b-49bb-81b7-1cb3412f90fe.jpg',0,9,4,0,'2022-10-27 07:52:59'),(23,'Silla','Para bebe','244c920b-e294-4498-beb9-0cd207b0712d.jpg',0,9,4,0,'2022-10-27 07:53:00'),(24,'Silla','Para bebe','edb0a7f6-3c33-40e5-8c7c-8e136b1434fa.jpg',0,9,4,0,'2022-10-27 07:53:00'),(25,'Silla','Para bebe','c6a0ef44-6381-4f0d-bbb8-06078715bf6e.jpg',0,9,4,0,'2022-10-27 07:53:00'),(26,'Silla','Para bebe','b7e64dff-fa5f-4c99-942e-0acce2ccd551.jpg',0,9,4,0,'2022-10-27 07:53:00'),(27,'Silla','Para bebe','a671bc7d-7fb9-4033-ad78-0a3968f4f386.jpg',0,9,4,0,'2022-10-27 07:53:02'),(28,'Silla','Para bebe','668277e2-b1b7-4688-b68f-856953d7ae7a.jpg',0,9,4,1,'2022-10-27 07:53:03'),(29,'Silla','Para bebe','7d6a9b37-b667-405e-af63-2f6abacc0a11.jpg',0,9,4,1,'2022-10-27 07:53:04'),(30,'Silla','Para bebe','bc1f5bae-941a-41c1-96ed-54618555be85.jpg',0,9,4,1,'2022-10-27 07:53:04'),(31,'Silla','Para bebe','8b719343-a8bd-4235-a2f6-7b15c6749d0d.jpg',0,9,4,1,'2022-10-27 07:53:05'),(32,'Silla','Para bebe','19f00792-58bc-4a47-be8a-d87f11a440e6.jpg',0,9,4,1,'2022-10-27 07:53:05'),(33,'Silla','Para bebe','3be5a506-f251-4003-97c4-62d578eee9b0.jpg',0,9,4,1,'2022-10-27 07:53:05'),(34,'Silla','Para bebe','25613dad-453a-4454-be1c-4a1384175007.jpg',0,9,4,1,'2022-10-27 07:53:06'),(35,'Silla','Para bebe','a1f6db5c-43ef-4cae-b168-ead9a67d7b25.jpg',0,9,4,1,'2022-10-27 07:53:07'),(36,'Silla','Para bebe','505c261f-2eb8-4cbd-b72b-7fc65b2a25f3.jpg',0,9,4,1,'2022-10-27 07:53:07'),(37,'Silla','Para bebe','b4282916-b5be-4992-8e02-1288ee8b8750.jpg',0,9,4,1,'2022-10-27 07:53:07'),(38,'Silla','Para bebe','732091a4-2844-42c3-b4d8-fcc752d82e5b.jpg',0,9,4,1,'2022-10-27 07:53:09'),(39,'Silla','Para bebe','cbb72429-c356-4a7f-99a7-2acbb985acee.jpg',0,9,4,0,'2022-10-27 07:53:11'),(40,'Silla','Para bebe','5f0a1df5-1060-484b-9d77-97f864b59be9.jpg',0,9,4,0,'2022-10-27 07:53:11'),(41,'Control','Bluetooth ','20abea6e-c4e2-427a-99e6-76295f175f80.jpg',0,9,4,1,'2022-10-27 07:54:02'),(42,'Control','Bluetooth ','73c5d89c-d32e-4b01-9396-7bf7d163ccfd.jpg',0,9,4,1,'2022-10-27 07:54:03'),(43,'Control','Bluetooth ','f7e8576b-5c32-48a5-b357-b7847c46f71b.jpg',0,9,4,1,'2022-10-27 07:54:06'),(44,'Control','Bluetooth ','45158ff2-03f9-4fed-8e5c-bdc5cd908e45.jpg',0,9,4,1,'2022-10-27 07:54:06'),(45,'Control','Bluetooth ','9d81895f-ad30-49d6-b468-45ada1be81fd.jpg',0,9,4,1,'2022-10-27 07:54:06'),(46,'Control','Bluetooth ','2f7e989b-81b2-4ae1-8b42-c8776ec283eb.jpg',0,9,4,1,'2022-10-27 07:54:07'),(47,'Control','Bluetooth ','5ad09f8b-0ab6-4743-8872-c4f297f6b573.jpg',0,9,4,1,'2022-10-27 07:54:07'),(48,'Control','Bluetooth ','277144b3-1468-4901-b375-072446b26887.jpg',0,9,4,1,'2022-10-27 07:54:07'),(49,'Control','Bluetooth ','31d1b7c5-f331-4638-8b6e-20db3cc2e1b6.jpg',0,9,4,1,'2022-10-27 07:54:08'),(50,'Control','Bluetooth ','acc1c29a-cf4e-4af9-bd86-92058934215d.jpg',0,9,4,1,'2022-10-27 07:54:08'),(51,'Control','Bluetooth ','14d8b84e-ba96-4f57-88c5-285a379fa8c8.jpg',0,9,4,1,'2022-10-27 07:54:08'),(52,'Control','Bluetooth ','985851b6-3ea4-4a9d-ad56-0bb78e107fbd.jpg',0,9,4,1,'2022-10-27 07:54:09'),(53,'Control','Bluetooth ','78407aa6-518d-448e-8558-46a5bb96dfca.jpg',0,9,4,1,'2022-10-27 07:54:11'),(54,'Control','Bluetooth ','de5d5406-36ee-4b67-82b2-c3c225e009c3.jpg',0,9,4,1,'2022-10-27 07:54:11'),(55,'Control','Bluetooth ','fcc7061e-9604-4109-a6cb-ccfc3d594f2d.jpg',0,9,4,1,'2022-10-27 07:54:11'),(56,'Control','Bluetooth ','d05992f3-4eb2-41d7-80cb-5d38b023f43f.jpg',0,9,4,1,'2022-10-27 07:54:11'),(57,'Control','Bluetooth ','655fa48f-5970-43b8-a80f-721947e2bd5e.jpg',0,9,4,1,'2022-10-27 07:54:11'),(58,'Control','Bluetooth ','97d4e3fe-59d6-477f-b684-5a8c972663fb.jpg',0,9,4,1,'2022-10-27 07:54:12'),(59,'Control','Bluetooth ','fe9ec188-6978-42a7-afd2-147347cf3d6e.jpg',0,9,4,1,'2022-10-27 07:54:12'),(60,'Control','Bluetooth ','794ec195-5741-4d74-994c-129112773200.jpg',0,9,4,1,'2022-10-27 07:54:12'),(61,'Control','Bluetooth ','d4ae3309-a743-468a-b651-0093786365f5.jpg',0,9,4,1,'2022-10-27 07:54:12'),(62,'Control','Bluetooth ','3b946bf0-0c9a-4527-8589-22f19b2f28ab.jpg',0,9,4,1,'2022-10-27 07:54:12'),(63,'Control','Bluetooth ','fc00973a-e2c2-4cd9-a006-503240f1add8.jpg',0,9,4,1,'2022-10-27 07:54:12'),(64,'Control','Bluetooth ','8bbcec08-0914-4cad-84be-4a607b7c3cb8.jpg',0,9,4,1,'2022-10-27 07:54:12'),(65,'Control','Bluetooth ','b9a5fc1b-ad58-48ba-a20a-a563c076b757.jpg',0,9,4,1,'2022-10-27 07:54:12'),(66,'Control','Bluetooth ','dabe9720-c2da-4075-822c-18d8e3ab16cc.jpg',0,9,4,1,'2022-10-27 07:54:13'),(67,'Control','Bluetooth ','608ab297-dc19-4a6e-80c6-51570c9f57b4.jpg',0,9,4,1,'2022-10-27 07:54:13'),(68,'Control','Bluetooth ','dba3f7be-99d6-40a6-93e1-dcb292179be6.jpg',0,9,4,1,'2022-10-27 07:54:13'),(69,'Control','Bluetooth ','3d30da59-0b0f-435a-8594-90c502773489.jpg',0,9,4,1,'2022-10-27 07:54:13'),(70,'Control','Bluetooth ','fd911938-2b93-41eb-93ed-fcfe0378f1e0.jpg',0,9,4,1,'2022-10-27 07:54:13'),(71,'Control','Bluetooth ','9a702371-af17-4da9-a1d5-366a5faef91b.jpg',0,9,4,1,'2022-10-27 07:54:13'),(72,'Control','Bluetooth ','9d91dc43-e723-4938-9617-8943ffa135d5.jpg',0,9,4,1,'2022-10-27 07:54:13'),(73,'Control','Bluetooth ','b4037ee5-42ef-4a13-b5b2-d454fd7b47eb.jpg',0,9,4,1,'2022-10-27 07:54:13'),(74,'Control','Bluetooth ','8ea315e4-a895-4dfd-88f3-1ff020f04aa0.jpg',0,9,4,1,'2022-10-27 07:54:14'),(75,'Control','Bluetooth ','1c4fedb2-3e9b-477b-854e-6dd3a827baf6.jpg',0,9,4,1,'2022-10-27 07:54:14'),(76,'Control','Bluetooth ','ffdc1efe-3db6-4024-9a3a-4857452ae3f0.jpg',0,9,4,1,'2022-10-27 07:54:14'),(77,'Control','Bluetooth ','7bf9a572-86f7-498f-8b17-43dc2129ba27.jpg',0,9,4,1,'2022-10-27 07:54:14'),(78,'Control','Bluetooth ','48559f27-ba3b-4df4-ad99-241d2bf33ccc.jpg',0,9,4,1,'2022-10-27 07:54:14'),(79,'Control','Bluetooth ','a08fbd06-6641-428f-b484-9fe76ac5dbd7.jpg',0,9,4,1,'2022-10-27 07:54:14'),(80,'Control','Bluetooth ','6f1f0f1f-acb9-49b4-924d-fc3a39a0c2d7.jpg',0,9,4,1,'2022-10-27 07:54:14'),(81,'Control','Bluetooth ','d1b21234-782e-4060-99cf-dfb7d4fa2424.jpg',0,9,4,1,'2022-10-27 07:54:14'),(82,'Control','Bluetooth ','77f7cbaa-cbb6-4611-b3ac-e2650c2f1e2f.jpg',0,9,4,0,'2022-10-27 07:54:15'),(83,'Control','Bluetooth ','b37a9b6c-b2f9-4593-b6a4-0bfff964c2e4.jpg',0,9,4,0,'2022-10-27 07:54:15'),(84,'Control','Bluetooth ','3a595f36-7077-4f15-a4c6-01f5047e00ab.jpg',0,9,4,0,'2022-10-27 07:54:15'),(85,'Control','Bluetooth ','694bac52-e6c7-4754-9b2d-3435e3d08305.jpg',0,9,0,1,'2022-10-27 07:54:15'),(86,'Control','Bluetooth ','48a09c1f-0d18-4c6a-816b-3862305bb1d2.jpg',0,9,0,1,'2022-10-27 07:54:15'),(87,'Control','Bluetooth ','f1dbf821-255b-4cc8-bc10-12f358638079.jpg',0,9,0,1,'2022-10-27 07:54:15'),(88,'Control','Bluetooth ','ee2a6945-daf7-450c-b2ea-9c5bdb72b5ab.jpg',0,9,0,1,'2022-10-27 07:54:15'),(89,'Control','Bluetooth ','f3d17b1f-d9a6-45be-b7bf-a55de36a9c32.jpg',0,9,0,1,'2022-10-27 07:54:15'),(90,'Control','Bluetooth ','1a748867-946f-480d-9ca3-f610902e0d08.jpg',0,9,0,1,'2022-10-27 07:54:15'),(91,'Control','Bluetooth ','4d47de3a-5eb1-4691-9b33-d10af1b1d380.jpg',0,9,0,1,'2022-10-27 07:54:16'),(92,'Control','Bluetooth ','85922d8d-8a66-4982-9fd2-e08cc4ec4dfa.jpg',0,9,0,1,'2022-10-27 07:54:16'),(93,'Pijama','D navidad','',0,9,4,1,'2022-10-27 07:57:27'),(94,'Carro','Azul para mi','021dfaff-955e-4769-8a53-00dc3e65f88f.jpg',0,9,4,1,'2022-10-27 08:01:11'),(95,'Ropa','De bebé','bd4ffffd-5b49-4c7a-a1bc-38a1b903c9ad.jpg',0,9,3,1,'2022-10-27 08:02:36'),(96,'Transportador','Pal recién nacido','e61129dd-d969-46b4-beb4-c13bef4e9c83.jpg',0,9,3,1,'2022-10-27 08:04:12'),(97,'Coche','De bebe','fa1342c8-15a7-427d-9b61-cf04cb55f105.jpg',0,9,3,1,'2022-10-27 08:06:31');
/*!40000 ALTER TABLE `gifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_reset`
--

DROP TABLE IF EXISTS `password_reset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_reset` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `code` varchar(8) NOT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_reset`
--

LOCK TABLES `password_reset` WRITE;
/*!40000 ALTER TABLE `password_reset` DISABLE KEYS */;
INSERT INTO `password_reset` VALUES (1,1,'8y2sm5',0,'2022-10-27 03:51:26'),(2,1,'5qfois',0,'2022-10-27 07:20:46');
/*!40000 ALTER TABLE `password_reset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subscriptions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `amount` smallint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriptions`
--

LOCK TABLES `subscriptions` WRITE;
/*!40000 ALTER TABLE `subscriptions` DISABLE KEYS */;
INSERT INTO `subscriptions` VALUES (1,'Free',0);
/*!40000 ALTER TABLE `subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `display_name` varchar(80) DEFAULT NULL,
  `username` varchar(70) NOT NULL,
  `password` varchar(70) NOT NULL,
  `email` varchar(70) NOT NULL,
  `img` varchar(60) DEFAULT NULL,
  `sub` int NOT NULL DEFAULT '1',
  `register_code` varchar(8) DEFAULT '',
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `username_2` (`username`),
  UNIQUE KEY `email_2` (`email`),
  KEY `sub` (`sub`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Josue Soto','Sycmaster','$2a$10$b.PgxhpBlqXTQc5bSMtNhuReBxfkccfX6cwSmHiRTRglbnI/dKpLO','sycmasters@gmail.com','d7e9657c-1c51-404f-baa3-fc24c248ba86.jpg',1,'',1,'2022-10-27 03:49:52'),(2,'Gabriel Soto','Seitrax','$2a$10$UJMTci0w2.VW1zmNa1y3KOdYrArmzPGaPnATGQbC5e3w9F/YshLke','seitraxmlg@hotmail.com','43a97442-30d6-4db0-9210-0d9361aae5d8.jpg',1,'',1,'2022-10-27 03:52:43'),(3,'sebas','sebas','$2a$10$1qeB7m0CBf0vShbNcsGvVOM1Pb7ByAeluWQQhO3AGIv4X1MgLk0b.','sebas.soto175@gmail.com','',1,'',1,'2022-10-27 04:03:46'),(4,'Diego','JD','$2a$10$WXXojw6NKCq48UHERTmJweySqiw2mOEpab24XQxjbIcazJra30mI2','juandiegocambronero@gmail.com','605eb862-2d85-4ffe-a565-4c1125adc464.jpg',1,'',1,'2022-10-27 04:15:37'),(5,'Abby ','Snicker','$2a$10$E47zwglhX1gYUhYTs4AF3.oHur0Kfz5e/E0KOZV3GtAFlf.x6K1yS','abbysc07@hotmail.com','',1,'',1,'2022-10-27 04:19:35'),(6,'Mary','Maricela','$2a$10$lNgTRXs3sqyA368YOkGEFeqr42VzUKXzmacMhm3h9fKzYMrzCb0/W','marycp17@hotmail.com','',1,'',1,'2022-10-27 04:21:29'),(7,'Santiago','SAN','$2a$10$K4TbJtcXWCkPQBXV8cAjGOBP3pB8W7sT5cwivqIrQVkRZUzDZ14te','smjaajms@hotmail.com','bf6e09ec-e958-48f7-b024-6dd600e4b760.jpg',1,'',1,'2022-10-27 04:22:25'),(8,'Marvin','Marzolo777','$2a$10$QJ3fblSnDOl.XzV0qLcL/efp/2ih3ap6uUFaWAPHD3IbbyCpUzrT2','marvin3d@gmail.com','',1,'',1,'2022-10-27 04:26:32'),(9,'Oscar perez granados ','oscpg89','$2a$10$KUYMj6Jb1bRfosk2kmA3uehlKXlv0bhJ62L4S1w/XlE1Rr8V3xcIu','oapg1989@gmail.com','fa7a3f16-811b-4aba-8879-cc1c004df72b.jpg',1,'',1,'2022-10-27 04:47:22'),(10,'Eduardo','Selkon','$2a$10$esJwOQ7VhNKjlQ6FzVZ1EOp3B/D4525Mo90qeAj0OKfH5L8DnM1.C','eduardoselkon@hotmail.com','4b608f58-dbc6-487c-969e-2cca1778ab8b.jpg',1,'',1,'2022-10-27 15:20:04'),(11,'Jose','Selk','$2a$10$ir5MgQED/KBqgr5SEdRaXeYzhGqOXgXwbFXSYCzPVfWFkRTzoP2Ny','eduardoselkon@gmail.com','',1,'xpu2c0',0,'2022-10-27 16:01:59');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-27 19:41:16
